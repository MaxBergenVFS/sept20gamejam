// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "SheeshPlant"
{
	Properties
	{
		_TextureSample2("Texture Sample 2", 2D) = "white" {}
		_Cutoff( "Mask Clip Value", Float ) = 0.62
		_WaterScale("Water Scale", Range( 0 , 15)) = 15
		_AlphaScale("Alpha Scale", Range( 0 , 1)) = 0
		_Brightness("Brightness", Range( 0 , 2)) = 0.4699083
		_DistortionScale("Distortion Scale", Range( 0 , 5)) = 1
		_DistortionIntensity("Distortion Intensity", Range( 0 , 1)) = 5
		_NoiseGenScale("Noise Gen Scale", Range( 0 , 5)) = 5
		_DistortionScroll("Distortion Scroll", Vector) = (0.2,0,0,0)
		_WaterScroll("Water Scroll", Vector) = (0.2,0,0,0)
		_TextureSample1("Texture Sample 1", 2D) = "white" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout"  "Queue" = "AlphaTest+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Off
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 4.6
		#pragma surface surf Unlit keepalpha noshadow noambient novertexlights nolightmap  nodynlightmap nodirlightmap nometa noforwardadd 
		struct Input
		{
			float4 vertexColor : COLOR;
			float4 screenPos;
			float2 uv_texcoord;
		};

		uniform sampler2D _TextureSample2;
		uniform float2 _DistortionScroll;
		uniform float _DistortionScale;
		uniform float _NoiseGenScale;
		uniform float _DistortionIntensity;
		uniform float2 _WaterScroll;
		uniform float _WaterScale;
		uniform float _Brightness;
		uniform sampler2D _TextureSample1;
		uniform float _AlphaScale;
		uniform float _Cutoff = 0.62;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		inline half4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return half4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float2 temp_cast_0 = (_DistortionScale).xx;
			float2 uv_TexCoord59 = i.uv_texcoord * temp_cast_0;
			float2 panner57 = ( 1.0 * _Time.y * _DistortionScroll + uv_TexCoord59);
			float simplePerlin2D101 = snoise( panner57*_NoiseGenScale );
			simplePerlin2D101 = simplePerlin2D101*0.5 + 0.5;
			float2 temp_cast_1 = (_WaterScale).xx;
			float2 uv_TexCoord9 = i.uv_texcoord * temp_cast_1;
			float2 panner119 = ( 1.0 * _Time.y * _WaterScroll + uv_TexCoord9);
			float2 temp_output_112_0 = ( ( ase_screenPosNorm.w + ( simplePerlin2D101 * _DistortionIntensity ) ) + panner119 );
			o.Emission = ( ( i.vertexColor * tex2D( _TextureSample2, temp_output_112_0 ) ) * _Brightness ).rgb;
			o.Alpha = 1;
			float4 temp_output_116_0 = ( tex2D( _TextureSample1, temp_output_112_0 ) * _AlphaScale );
			clip( temp_output_116_0.r - _Cutoff );
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18900
-1556;23;1121;1036;454.1324;690.2025;1.74098;True;False
Node;AmplifyShaderEditor.RangedFloatNode;72;-1613.575,-435.9302;Inherit;False;Property;_DistortionScale;Distortion Scale;5;0;Create;True;0;0;0;False;0;False;1;0;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;58;-1206.231,-282.312;Inherit;False;Property;_DistortionScroll;Distortion Scroll;8;0;Create;True;0;0;0;False;0;False;0.2,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;59;-1272.846,-452.0202;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;102;-1038.177,-120.6689;Inherit;False;Property;_NoiseGenScale;Noise Gen Scale;7;0;Create;True;0;0;0;False;0;False;5;0;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;57;-959.1612,-280.4212;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;101;-707.3755,-236.4288;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;1;-1079.12,163.1719;Inherit;False;Property;_WaterScale;Water Scale;2;0;Create;True;0;0;0;False;0;False;15;1;0;15;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;53;-753.0923,15.53063;Inherit;False;Property;_DistortionIntensity;Distortion Intensity;6;0;Create;True;0;0;0;False;0;False;5;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;9;-690.6243,155.3161;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;120;-801.8056,353.0712;Inherit;False;Property;_WaterScroll;Water Scroll;9;0;Create;True;0;0;0;False;0;False;0.2,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.ScreenPosInputsNode;52;-719.9128,-497.5712;Float;True;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;54;-421.1634,-106.4796;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;67;-264.8204,-366.4718;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;119;-392.9697,228.9106;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;112;29.81012,-27.0719;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;94;233.0711,-107.181;Inherit;True;Property;_TextureSample2;Texture Sample 2;0;0;Create;True;0;0;0;False;0;False;-1;31337d6ffa6084a7b99fffc8b828e47c;7b5432fedb49f41dea2cbade31c02439;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.VertexColorNode;137;554.5507,-343.6764;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;117;746.6461,-281.626;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;141;613.499,72.12029;Inherit;False;Property;_Brightness;Brightness;4;0;Create;True;0;0;0;False;0;False;0.4699083;1;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;14;220.6679,173.2935;Inherit;True;Property;_TextureSample1;Texture Sample 1;10;0;Create;True;0;0;0;False;0;False;-1;535962df7bf314066ada0e72130d9dfd;77b82b046a79f442c963261ec66c2d39;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;115;442.8242,494.7526;Inherit;False;Property;_AlphaScale;Alpha Scale;3;0;Create;True;0;0;0;False;0;False;0;0.79;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;142;940.8909,-79.04996;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;116;827.7883,349.6426;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;140;1123.452,-156.3062;Float;False;True;-1;6;ASEMaterialInspector;0;0;Unlit;SheeshPlant;False;False;False;False;True;True;True;True;True;False;True;True;False;False;True;False;False;False;False;False;False;Off;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Masked;0.62;True;False;0;False;TransparentCutout;;AlphaTest;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;True;0.5;False;0;5;False;-1;10;False;-1;0;5;False;-1;10;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;1;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;59;0;72;0
WireConnection;57;0;59;0
WireConnection;57;2;58;0
WireConnection;101;0;57;0
WireConnection;101;1;102;0
WireConnection;9;0;1;0
WireConnection;54;0;101;0
WireConnection;54;1;53;0
WireConnection;67;0;52;4
WireConnection;67;1;54;0
WireConnection;119;0;9;0
WireConnection;119;2;120;0
WireConnection;112;0;67;0
WireConnection;112;1;119;0
WireConnection;94;1;112;0
WireConnection;117;0;137;0
WireConnection;117;1;94;0
WireConnection;14;1;112;0
WireConnection;142;0;117;0
WireConnection;142;1;141;0
WireConnection;116;0;14;0
WireConnection;116;1;115;0
WireConnection;140;2;142;0
WireConnection;140;10;116;0
ASEEND*/
//CHKSM=E5B61993F87278B4C1EC7672A46722953D496580