﻿using UnityEngine;

public class DayNightCycle : MonoBehaviour
{
    [SerializeField] private GameObject _dayMesh;
    [SerializeField] private GameObject _nightMesh;
    [SerializeField] private Material _daySkybox;
    [SerializeField] private Material _nightSkybox;

    private void EnableDay()
    {
        _dayMesh.SetActive(true);
        _nightMesh.SetActive(false);
        RenderSettings.skybox = _daySkybox;
    }

    private void EnableNight()
    {
        _dayMesh.SetActive(false);
        _nightMesh.SetActive(true);
        RenderSettings.skybox = _nightSkybox;
    }

    public void ChangeTimeOfDay()
    {
        if (_dayMesh.activeSelf) { EnableNight(); }
        else { EnableDay(); }
    }
}
