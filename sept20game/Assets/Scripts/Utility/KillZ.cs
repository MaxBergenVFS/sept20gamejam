﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class KillZ : MonoBehaviour
{
    [SerializeField]
    private GameObject _gameOverScreen;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _gameOverScreen.SetActive(true);
            GameManager.instance.GameOver(true);
        }
        else
        {
            Destroy(other.gameObject);
        }
    }
}
