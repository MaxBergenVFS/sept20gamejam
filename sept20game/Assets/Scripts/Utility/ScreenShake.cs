﻿using UnityEngine;

public class ScreenShake : MonoBehaviour
{
    [SerializeField]
    private float _shakeDuration = 0.5f;
    [SerializeField]
    private float _shakeAmount = 0.02f;
    [SerializeField]
    private float _decreaseFactor = 1.0f;

    private Transform _camTransform;
    private Vector3 _originalPos;
    private float _currentShakeDuration;
    private HeadBob _headBob;

    private void Awake()
    {
        _camTransform = GetComponent<Transform>();
        _headBob = GetComponent<HeadBob>();
        _currentShakeDuration = 0;
    }

    private void OnEnable()
    {
        _originalPos = _camTransform.localPosition;
        _currentShakeDuration = _shakeDuration;
        _headBob.enabled = false;
    }

    private void OnDisable()
    {
        _headBob.enabled = true;
    }

    private void Update()
    {
        if (_currentShakeDuration > 0)
        {
            _camTransform.localPosition = _originalPos + Random.insideUnitSphere * _shakeAmount;

            _currentShakeDuration -= Time.deltaTime * _decreaseFactor;
        }
        else
        {
            _currentShakeDuration = 0f;
            _camTransform.localPosition = _originalPos;
            this.enabled = false;
        }
    }
}
