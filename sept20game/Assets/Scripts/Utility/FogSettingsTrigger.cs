﻿using System.Collections;
using UnityEngine;

public class FogSettingsTrigger : TriggerCheck
{
    [SerializeField]
    private bool _resetFogToDefault;
    [SerializeField]
    private Color _colorToChangeTo;
    [SerializeField]
    private float _densityToChangeTo;
    [SerializeField]
    private float _timeBeforeValueChange;
    [SerializeField]
    private int _fogZoneNum = 0;

    private Color _defaultColor;
    private float _defaultDensity;
    private PlayerZoneManager _zoneManager;

    private void Awake()
    {
        _defaultColor = RenderSettings.fogColor;
        _defaultDensity = RenderSettings.fogDensity;
        if (GetComponentInParent<PlayerZoneManager>() != null)
        {
            _zoneManager = GetComponentInParent<PlayerZoneManager>();
        }
        else { Debug.LogError($"Missing PlayerZoneManager component in parent on {gameObject.name}"); }
    }

    protected override void TriggerAction()
    {
        if (_fogZoneNum != _zoneManager.GetZoneNum())
        {
            base.TriggerAction();
            StopAllCoroutines();
            if (_resetFogToDefault) { StartCoroutine(LerpParameters(_defaultColor, _defaultDensity)); }
            else { StartCoroutine(LerpParameters(_colorToChangeTo, _densityToChangeTo)); }
            ResetTrigger();
        }
        _zoneManager.SetZoneNum(_fogZoneNum);
    }

    IEnumerator LerpParameters(Color colorValue, float densityValue)
    {
        float timeElapsed = 0;
        float density;
        float densityStartValue = RenderSettings.fogDensity;
        Color color;
        Color colorStartValue = RenderSettings.fogColor;

        while (timeElapsed < _timeBeforeValueChange)
        {
            color = Color.Lerp(colorStartValue, colorValue, timeElapsed / _timeBeforeValueChange);
            density = Mathf.Lerp(densityStartValue, densityValue, timeElapsed / _timeBeforeValueChange);
            timeElapsed += Time.deltaTime;
            RenderSettings.fogColor = color;
            RenderSettings.fogDensity = density;
            yield return null;
        }
        RenderSettings.fogColor = colorValue;
        RenderSettings.fogDensity = densityValue;
    }
}
