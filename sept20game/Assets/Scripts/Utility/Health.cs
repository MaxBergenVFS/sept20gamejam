﻿using UnityEngine;

public abstract class Health : MonoBehaviour
{
    [SerializeField]
    protected int _defenseLevel = 1;
    [SerializeField]
    protected int _healthAmount = 20;

    protected virtual void SpawnObjOnDeath(GameObject itemDrop, GameObject[] prefabsToDrop, Vector3 spawnPos, float dropChance)
    {
        if (spawnPos == null) return;
        
        if (itemDrop != null)
        {
            itemDrop.transform.position = spawnPos;
        }
        else if (prefabsToDrop.Length > 0)
        {
            SpawnPrefab(prefabsToDrop, dropChance, spawnPos);
        }
    }

    private void SpawnPrefab(GameObject[] prefabs, float dropChance, Vector3 spawnPos)
    {
        float dropChanceRoll;
        dropChanceRoll = Random.Range(0f, 1f);
        if (dropChance > dropChanceRoll)
        {
            int index;
            index = Random.Range(0, prefabs.Length);
            GameObject obj = Instantiate(prefabs[index], spawnPos, Quaternion.identity);
        }
    }

    public bool CompareDefenseLevel(int atkLvl)
    {
        if (atkLvl >= _defenseLevel) return true;
        return false;
    }

    public virtual void TakeDamage(int amt, bool player) => _healthAmount -= amt;

    public bool CheckIfDead() { return UtilityHelper.CheckIfDepleted(_healthAmount); }

}
