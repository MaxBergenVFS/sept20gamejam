﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelChangeFade : MonoBehaviour
{
    [SerializeField]
    private int _levelToChangeTo;

    private Animator _anim;
    private InputManager _playerInput;

    private void Awake()
    {
        _anim = GetComponent<Animator>();
    }

    private void Start()
    {
        if (PlayerController.instance) _playerInput = PlayerController.instance.GetComponent<InputManager>();
    }

    public void ChangeLevel()
    {
        if (_playerInput && !_playerInput.enabled) _playerInput.enabled = true;
        if (GameManager.instance != null) GameManager.instance.UpdateStats();
        SceneManager.LoadScene(_levelToChangeTo);
    }
    public void OverrideLevelIndex(int index) => _levelToChangeTo = index;
    public void FadeIn() => _anim.SetTrigger("FadeIn");
    public void FadeOut() => _anim.SetTrigger("FadeOut");

}
