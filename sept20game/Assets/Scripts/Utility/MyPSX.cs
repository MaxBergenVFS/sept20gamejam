﻿using UnityEngine;

[RequireComponent(typeof(Camera))]
[ExecuteInEditMode]
public class MyPSX : MonoBehaviour
{
    public Vector2Int customRes = new Vector2Int(620, 480);
    public float resolutionFactor = 3f;
    private RenderTexture rt;
    private Vector2 prevCustomRes;

    private void Awake()
    {
        // if (Application.isPlaying) {
        // 	QualitySettings.vSyncCount = 0;
        // }

        QualitySettings.antiAliasing = 0;

        Camera cam = GetComponent<Camera>();
        cam.depthTextureMode = cam.depthTextureMode | DepthTextureMode.Depth;

        customRes = new Vector2Int(((int)((float)Screen.width / resolutionFactor)), ((int)((float)Screen.height / resolutionFactor)));

        rt = new RenderTexture(customRes.x, customRes.y, 16, RenderTextureFormat.ARGB32);
        rt.filterMode = FilterMode.Point;
    }

    private void Update()
    {
        customRes = new Vector2Int(((int)((float)Screen.width / resolutionFactor)), ((int)((float)Screen.height / resolutionFactor)));

        if (prevCustomRes != customRes)
        {
            rt = new RenderTexture(customRes.x, customRes.y, 16, RenderTextureFormat.ARGB32);
            rt.filterMode = FilterMode.Point;
        }

        prevCustomRes = customRes;
    }

    private void OnRenderImage(RenderTexture src, RenderTexture dst)
    {
        if (src != null)
            src.filterMode = FilterMode.Point;

        Graphics.Blit(src, rt);
        Graphics.Blit(rt, dst);
    }
}
