﻿using UnityEngine;

public class PauseAnimation : MonoBehaviour
{
    private Animator _anim;

    private void Awake()
    {
        _anim = GetComponent<Animator>();
    }

    public void PauseAnim() { if (_anim.enabled == true) _anim.enabled = false; }
    public void UnPauseAnim() { if (_anim.enabled == false) _anim.enabled = true; }

}
