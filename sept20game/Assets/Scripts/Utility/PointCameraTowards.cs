﻿using UnityEngine;

public class PointCameraTowards : MonoBehaviour
{
    [SerializeField]
    private Transform _target;
    [SerializeField]
    private float _speed = 1f;
    [SerializeField]
    private float _timeUntilCameraControl = 5f;
    [SerializeField]
    private Animator _anim;
    [SerializeField]
    private TriggerCutscene _cutsceneTrigger;
    [SerializeField]
    private GameObject[] _objsToDisable;
    [SerializeField]
    private int _cutsceneNum = 0;

    private int _defaultTime;
    private bool _isFadingOut = false;
    private CameraController _cameraController;
    private PlayerController _playerController;
    private InputManager _inputManager;
    private bool _dontFadeOut = false;

    private void Awake()
    {
        _cameraController = GetComponent<CameraController>();
        _playerController = GetComponent<PlayerController>();
        _inputManager = GetComponent<InputManager>();
    }

    private void Start()
    {
        if (GameManager.instance.CheckIfHasPlayedCutscene(_cutsceneNum))
        {
            foreach (GameObject obj in _objsToDisable)
            {
                obj.SetActive(false);
            }
            Disable();
        }
    }

    public void SetNewTarget(Transform target, float timeUntilCamControl, bool dontFadeOut)
    {
        _target = target;
        _timeUntilCameraControl = timeUntilCamControl;
        _dontFadeOut = dontFadeOut;
    }

    private void OnDisable()
    {
        _cameraController.enabled = true;
        _playerController.enabled = true;
        _inputManager.enabled = true;
    }

    private void OnEnable()
    {
        //this is throwing an error on reload
        //_playerController.ResetSprintAndStam();

        _playerController.enabled = false;
        _cameraController.enabled = false;
        _inputManager.enabled = false;
    }

    private void Update()
    {
        _timeUntilCameraControl -= Time.deltaTime;
        Vector3 lTargetDir = _target.position - transform.position;
        transform.rotation = Quaternion.RotateTowards(transform.rotation,
        Quaternion.LookRotation(lTargetDir), Time.deltaTime * _speed);

        if (_timeUntilCameraControl < 0f && (!_isFadingOut || _dontFadeOut))
        {
            if (!_dontFadeOut)
            {
                _anim.SetTrigger("FadeOutDontChangeLevel");
                _isFadingOut = true;
            }
            else
            {
                _cutsceneTrigger.SetFinishLookTowards(true);
                Disable();
            }
        }
    }

    public void Disable()
    {
        this.enabled = false;
    }
}
