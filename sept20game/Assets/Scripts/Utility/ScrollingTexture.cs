﻿using UnityEngine;

public class ScrollingTexture : MonoBehaviour
{
    [SerializeField]
    private float _scrollSpeed;

    private Renderer _rend;

    private void Awake()
    {
        _rend = GetComponent<Renderer>();
    }

    private void Update()
    {
        float vOffset = Time.time * _scrollSpeed;
        _rend.material.SetTextureOffset("_MainTex", new Vector2(0f, vOffset));
    }
}
