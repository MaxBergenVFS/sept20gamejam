﻿public static class UtilityHelper
{
    public enum musicTriggerEnum
    {
        StartLoop,
        StopAudio,
        PlayOneShot
    };

    public static bool CheckIfDepleted(int num)
    {
        if (num <= 0) return true;
        return false;
    }

    public static bool CheckIfDepleted(float num)
    {
        if (num <= 0f) return true;
        return false;
    }
}
