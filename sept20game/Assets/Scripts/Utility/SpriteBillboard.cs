﻿using UnityEngine;

public class SpriteBillboard : MonoBehaviour
{
    private Transform _mainCamTransform;

    private void Awake()
    {
        _mainCamTransform = Camera.main.transform;
    }

    void Update()
    {
        transform.LookAt(_mainCamTransform.position, Vector3.up);
    }
}
