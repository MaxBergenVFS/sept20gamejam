﻿using UnityEngine;

public class SceneSettings : MonoBehaviour
{
    private MusicManager _musicManager;
    private CursorManager _cursorManager;
    private PlayerController _playerController;
    private CharacterController _characterController;
    private PlayerFootstepSFX _footstepSFX;
    private GameManager _gameManager;
    private InputManager _input;
    private PauseMenu _pauseMenu;

    [SerializeField] private string _levelName;
    [SerializeField]
    private bool _startSceneWithCursor, _startScenePaused, _thisSceneIsSaveRoom,
        _changeMusicOnSceneEnter, _returnPlayerToAltSpawnPos;
    [SerializeField] private Transform _alternatePlayerSpawnPos, _inFrontOfSaveRoomSpawnPos;
    [SerializeField] private SaveGameInteract _saveGameInteract;
    [SerializeField] private AudioParamaterHandler _audioParamHandler;
    [SerializeField] private int _musicIndexToChangeTo;

    private void Awake()
    {
        _musicManager = GameObject.Find("MusicManager").GetComponent<MusicManager>();
        _cursorManager = GameObject.Find("CursorManager").GetComponent<CursorManager>();
    }

    private void Start()
    {
        if (GameManager.instance != null) _gameManager = GameManager.instance;
        if (PlayerController.instance)
        {
            _playerController = PlayerController.instance;
            _characterController = _playerController.GetComponent<CharacterController>();
            _footstepSFX = _playerController.GetComponent<PlayerFootstepSFX>();
            _input = _playerController.GetComponent<InputManager>();
            _input.isPaused = _startScenePaused;
            _pauseMenu = _playerController.GetComponent<PauseMenu>();
            _pauseMenu.SetLevelName(_levelName);
            _playerController.GetComponent<PlayerHealth>().SetCurrentHealthAmt(_gameManager.GetCurrentHealthAmt());
        }
        if (_thisSceneIsSaveRoom && _gameManager.GetLoadData())
        {
            _saveGameInteract.LoadGame();
            MovePlayerToAlternateSpawnPos(_alternatePlayerSpawnPos);
        }
        _cursorManager.StartSceneWithCursor(_startSceneWithCursor);
        if (_gameManager != null)
        {
            if (_gameManager.currentlyInSaveRoom)
            {
                SpawnPlayerInAlternatePos(_inFrontOfSaveRoomSpawnPos);
            }
            else if (_alternatePlayerSpawnPos != null && !GameManager.instance.playerHasReadDiary)
            {
                SpawnPlayerInAlternatePos(_alternatePlayerSpawnPos);
            }
            _gameManager.currentlyInSaveRoom = _thisSceneIsSaveRoom;
        }
    }

    public void ChangeMusicLoop()
    {
        if (!_changeMusicOnSceneEnter) return;
        _musicManager.SetMusicLoop(_musicIndexToChangeTo);
        _audioParamHandler?.SetNextParameter(_musicManager.GetCurrentEventInstance(), _musicManager.GetCurrentParamIndex());
    }

    private void SpawnPlayerInAlternatePos(Transform spawnPos)
    {
        if (_playerController == null) return;
        if (_alternatePlayerSpawnPos == null) { Debug.LogError("missing _alternatePlayerSpawnPos Transform reference"); }
        else if (!_thisSceneIsSaveRoom) { MovePlayerToAlternateSpawnPos(spawnPos); }
    }

    private void MovePlayerToAlternateSpawnPos(Transform spawnPos)
    {
        _characterController.enabled = false;
        _playerController.transform.position = spawnPos.position;
        _footstepSFX.DontPlaySFXOnEnable();
        _characterController.enabled = true;
    }
}
