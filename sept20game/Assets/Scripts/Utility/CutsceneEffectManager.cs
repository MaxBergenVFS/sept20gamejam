﻿using UnityEngine;

public class CutsceneEffectManager : MonoBehaviour
{
    [SerializeField]
    private Animator _anim;
    [SerializeField]
    private float _maxTimeBetweenEffects = 5f;
    [SerializeField]
    private float _minTimeBetweenEffects = 1f;

    private float _timeBeforeNextEffect;
    private string _path = "event:/SFX/Cutscene/Zap";

    private void Awake()
    {
        SetTimeBeforeNextEffect();
    }

    private void Update()
    {
        _timeBeforeNextEffect -= Time.deltaTime;
        if (_timeBeforeNextEffect < 0) { TriggerEffect(); }
    }

    private void TriggerEffect()
    {
        _anim.SetTrigger("TriggerEffect");
        FMODUnity.RuntimeManager.PlayOneShot(_path);
        SetTimeBeforeNextEffect();
    }

    private void SetTimeBeforeNextEffect()
    {
        _timeBeforeNextEffect = Random.Range(_minTimeBetweenEffects, _maxTimeBetweenEffects);
    }
}
