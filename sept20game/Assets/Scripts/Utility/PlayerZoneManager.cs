﻿using UnityEngine;

public class PlayerZoneManager : MonoBehaviour
{
    private int _currentZoneNum = 0;

    public void SetZoneNum(int num) { _currentZoneNum = num; }
    public int GetZoneNum() { return _currentZoneNum; }
}
