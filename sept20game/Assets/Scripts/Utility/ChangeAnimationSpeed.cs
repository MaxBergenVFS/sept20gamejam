﻿using UnityEngine;

public class ChangeAnimationSpeed : MonoBehaviour
{
    private Animator _anim;

    [SerializeField]
    private float _speedModifier;

    private void Awake()
    {
        _anim = GetComponent<Animator>();
    }

    private void Start()
    {
        _anim.speed *= _speedModifier;
    }
}
