﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StateHandler : MonoBehaviour
{

    [SerializeField] private SceneSettings _sceneSettings;
    [SerializeField] private Door[] _doors;
    [SerializeField] private Key[] _keys;
    [SerializeField] private Gold[] _goldCoins;
    [SerializeField] private Secret[] _secrets;
    [SerializeField] private Soul[] _souls;
    [SerializeField] private TeleportPlant[] _teleportPlants;
    [SerializeField] private OrbSlot[] _orbSlots;
    [SerializeField] private Backpack[] _backpack;

    private int _healthAmtToResetTo = 100;
    private int _currentSceneIndex;
    private StateManager _stateManager;
    private PlayerInventory _playerInventory;
    private PlayerHealth _playerHealth;
    private Collectables _collectables;
    private GameObject _pauseMenu;
    private PlayerController _playerController;
    private Dictionary<int, bool> _doorDict = new Dictionary<int, bool>();
    private Dictionary<int, bool> _keyHasBeenPickedUpDict = new Dictionary<int, bool>();
    private Dictionary<int, bool> _keyHasBeenUsedDict = new Dictionary<int, bool>();
    private Dictionary<int, bool> _goldHasBeenPickedUpDict = new Dictionary<int, bool>();
    private Dictionary<int, bool> _secretHasBeenFoundDict = new Dictionary<int, bool>();
    private Dictionary<int, bool> _soulHasBeenFreedDict = new Dictionary<int, bool>();
    private Dictionary<int, bool> _plantHasBeenPicked = new Dictionary<int, bool>();
    private Dictionary<int, bool> _orbSlotHasBeenUnlocked = new Dictionary<int, bool>();
    private Dictionary<int, bool> _backpackhasBeenPickedUp = new Dictionary<int, bool>();

    private void Awake() => _currentSceneIndex = SceneManager.GetActiveScene().buildIndex;

    private void Start()
    {
        if (PlayerController.instance)
        {
            _playerController = PlayerController.instance;
            _pauseMenu = _playerController.GetComponent<PauseMenu>().pauseMenu;
            _pauseMenu.SetActive(true);
            _playerInventory = _playerController.GetComponent<PlayerInventory>();
            _collectables = _playerController.GetComponent<Collectables>();
            _playerHealth = _playerController.GetComponent<PlayerHealth>();
            _stateManager = StateManager.instance;
            InitCollectables();
            AddObjsToStateManager();
            SetSceneObjStates();
            CheckIfKeysHaveBeenPickedUp();
            UpdateCollectablesUI();
            _pauseMenu.SetActive(false);
        }
        else { _sceneSettings.ChangeMusicLoop(); }
    }

    private void AddObjsToStateManager()
    {
        AddObjToStateManager(_doors, _doorDict, _stateManager.door_isUnlocked, false);
        AddObjToStateManager(_keys, _keyHasBeenPickedUpDict, _stateManager.key_hasBeenPickedUp, false);
        AddObjToStateManager(_keys, _keyHasBeenUsedDict, _stateManager.key_hasBeenUsed, true);
        AddObjToStateManager(_goldCoins, _goldHasBeenPickedUpDict, _stateManager.gold_hasBeenPickedUp, false);
        AddObjToStateManager(_secrets, _secretHasBeenFoundDict, _stateManager.secret_hasBeenFound, false);
        AddObjToStateManager(_souls, _soulHasBeenFreedDict, _stateManager.soul_hasBeenFreed, false);
        AddObjToStateManager(_teleportPlants, _plantHasBeenPicked, _stateManager.plant_hasBeenPicked, false);
        AddObjToStateManager(_orbSlots, _orbSlotHasBeenUnlocked, _stateManager.orbSlot_hasBeenUnlocked, false);
        AddObjToStateManager(_backpack, _backpackhasBeenPickedUp, _stateManager.backpack_hasBeenPickedUp, false);
    }

    private void SetSceneObjStates()
    {
        GameManager.instance.CheckIfHasChangedScene();

        if (GameManager.instance.hasChangedScene)
        {
            UpdateStateForObjsInScene(_doors, _stateManager.door_isUnlocked);
            UpdateStateForObjsInScene(_keys, _stateManager.key_hasBeenPickedUp);
            UpdateStateForObjsInScene(_goldCoins, _stateManager.gold_hasBeenPickedUp);
            UpdateStateForObjsInScene(_secrets, _stateManager.secret_hasBeenFound);
            UpdateStateForObjsInScene(_souls, _stateManager.soul_hasBeenFreed);
            UpdateStateForObjsInScene(_teleportPlants, _stateManager.plant_hasBeenPicked);
            UpdateStateForObjsInScene(_orbSlots, _stateManager.orbSlot_hasBeenUnlocked);
            UpdateStateForObjsInScene(_backpack, _stateManager.backpack_hasBeenPickedUp);
            _sceneSettings.ChangeMusicLoop();
        }
        else //player has died and scene has reset
        {
            //reset playerHealth Amt to what is was on scene enter
            _playerHealth.SetCurrentHealthAmt(_healthAmtToResetTo);
            ForceDefaultStateForObjsInScene(_doors, _stateManager.door_isUnlocked);
            ForceDefaultStateForObjsInScene(_keys, _stateManager.key_hasBeenPickedUp);
            ForceDefaultStateForObjsInScene(_goldCoins, _stateManager.gold_hasBeenPickedUp);
            ForceDefaultStateForObjsInScene(_secrets, _stateManager.secret_hasBeenFound);
            ForceDefaultStateForObjsInScene(_souls, _stateManager.soul_hasBeenFreed);
            ForceDefaultStateForObjsInScene(_teleportPlants, _stateManager.plant_hasBeenPicked);
            ForceDefaultStateForObjsInScene(_orbSlots, _stateManager.orbSlot_hasBeenUnlocked);
            ForceDefaultStateForObjsInScene(_backpack, _stateManager.backpack_hasBeenPickedUp);
        }
    }

    public int GetGoldAmtByIndex(int index) { return _goldCoins[index].GetAmtOfCoins(); }

    private void CheckIfKeysHaveBeenPickedUp()
    {
        if (_keys.Length > 0)
        {
            _playerInventory.InitKeys();
            foreach (Key key in _keys)
            {
                key.CheckIfKeyHasBeenPickedUp();
            }
        }
    }

    private void InitCollectables()
    {
        if (_goldCoins.Length > 0)
        {
            int amtOfCoins = 0;
            for (int i = 0; i < _goldCoins.Length; i++)
            {
                _goldCoins[i].SetIDNum(i);
                amtOfCoins += _goldCoins[i].GetAmtOfCoins();
            }
            _collectables.InitGoldInScene(amtOfCoins);
        }
        if (_secrets.Length > 0)
        {
            for (int i = 0; i < _secrets.Length; i++) { _secrets[i].SetIDNum(i); }
            _collectables.InitSecretsInScene(_secrets.Length);
        }
        if (_souls.Length > 0)
        {
            for (int i = 0; i < _souls.Length; i++) { _souls[i].SetIDNum(i); }
            _collectables.InitSoulsInScene(_souls.Length);
        }
    }

    private void UpdateCollectablesUI()
    {
        if (_goldCoins.Length > 0)
        {
            List<int> goldFoundInScene = _stateManager.GetNumOfGoldFoundInLevel(_currentSceneIndex);
            _collectables.UpdateGoldFoundInScene(goldFoundInScene);
        }
        if (_secrets.Length > 0)
        {
            int secretsFoundInScene = _stateManager.GetNumOfSecretsFoundInLevel(_currentSceneIndex);
            _collectables.UpdateSecretsFoundInScene(secretsFoundInScene);
        }
        if (_souls.Length > 0)
        {
            int soulsFreedInScene = _stateManager.GetNumOfSoulsFreedInLevel(_currentSceneIndex);
            _collectables.UpdateSoulsFreedInScene(soulsFreedInScene);
        }
    }

    private void AddObjToStateManager(ISaveable[] saveables, Dictionary<int, bool> handlerDict, Dictionary<int, Dictionary<int, bool>> managerDict, bool forceStartFalse)
    {
        if (saveables.Length < 0 || managerDict.ContainsKey(_currentSceneIndex)) return;

        foreach (var saveable in saveables)
        {
            if (forceStartFalse) { handlerDict.Add(saveable.GetIDNum(), false); }
            else { handlerDict.Add(saveable.GetIDNum(), saveable.IsInDefaultState()); }
        }
        managerDict.Add(_currentSceneIndex, handlerDict);
    }

    private void UpdateStateForObjsInScene(ISaveable[] saveables, Dictionary<int, Dictionary<int, bool>> managerDict)
    {
        if (saveables.Length > 0)
        {
            foreach (var saveable in saveables)
            {
                saveable.UpdateState(!managerDict[_currentSceneIndex][saveable.GetIDNum()]);
            }
        }
    }

    private void ForceDefaultStateForObjsInScene(ISaveable[] saveables, Dictionary<int, Dictionary<int, bool>> managerDict)
    {
        if (saveables.Length > 0)
        {
            List<int> nums = new List<int>();
            foreach (var saveable in saveables) { nums.Add(saveable.GetIDNum()); }
            foreach (var num in nums) { managerDict[_currentSceneIndex][num] = false; }
        }
    }
}