﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChangeTrigger : MonoBehaviour
{
    [SerializeField]
    private int _sceneToChangeTo = 0;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GameManager.instance.UpdateStats();
            SceneManager.LoadScene(_sceneToChangeTo);
        }
    }
}
