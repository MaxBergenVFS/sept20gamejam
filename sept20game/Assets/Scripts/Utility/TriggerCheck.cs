﻿using UnityEngine;

public class TriggerCheck : MonoBehaviour
{
    private bool _hasBeenTriggered = false;

    private void OnTriggerEnter(Collider other)
    {
        if (!_hasBeenTriggered && other.CompareTag("Player"))
        {
            TriggerAction();
        }
    }

    protected virtual void TriggerAction() { _hasBeenTriggered = true; }
    protected void ResetTrigger() { _hasBeenTriggered = false; }
    public bool CheckIfTriggered() { return _hasBeenTriggered; }
}
