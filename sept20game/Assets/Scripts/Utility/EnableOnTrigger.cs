﻿using UnityEngine;

public class EnableOnTrigger : TriggerCheck
{
    [SerializeField]
    private GameObject[] _objsToEnable;
    [SerializeField]
    private GameObject[] _objsToDisable;
    [SerializeField]
    private bool _disableInputManager = false;
    [SerializeField]
    private bool _canBeRetriggered = true;

    private InputManager _inputManager;

    private void Start()
    {
        _inputManager = PlayerController.instance.GetComponent<InputManager>();
    }

    protected override void TriggerAction()
    {
        base.TriggerAction();
        if (_disableInputManager) _inputManager.enabled = false;
        foreach (var obj in _objsToEnable)
        {
            obj.SetActive(true);
        }
        foreach (var obj in _objsToDisable)
        {
            obj.SetActive(false);
        }
        if (_canBeRetriggered) ResetTrigger();
    }
}
