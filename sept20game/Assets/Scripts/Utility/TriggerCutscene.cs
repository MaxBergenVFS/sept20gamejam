﻿using UnityEngine;

public class TriggerCutscene : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _cameras;
    [SerializeField]
    private float _timeBeforeCameraChange = 0.5f;
    [SerializeField]
    private float _timeBeforeLastCameraChange = 9f;
    [SerializeField]
    private Animator[] _anims;
    [SerializeField]
    private string[] _animTriggers;
    [SerializeField]
    private Transform _target;
    [SerializeField]
    private float _timeUntilTargetFocus = 1f;

    private PointCameraTowards _cameraControl;
    private GameObject _player;
    private EnableOrDisableObjs _disableObjs;
    private float _currentTime;
    private bool _finishedLookTowards = false;
    private int _cameraIndex = 0;

    private void Awake()
    {
        _currentTime = _timeBeforeCameraChange;
        _disableObjs = GetComponent<EnableOrDisableObjs>();
    }

    private void Start()
    {
        _player = PlayerController.instance.gameObject;
        _cameraControl = _player.GetComponent<PointCameraTowards>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _cameraControl.enabled = true;
            _cameraControl.SetNewTarget(_target, _timeUntilTargetFocus, true);
        }
    }

    public void SetFinishLookTowards(bool cond)
    {
        _finishedLookTowards = cond;
    }

    private void Update()
    {
        if (!_finishedLookTowards) return;

        _player.SetActive(false);
        _cameras[_cameraIndex].SetActive(true);
        _cameras[_cameraIndex].GetComponent<EnableOrDisableObjs>().SwapObjs();
        _currentTime -= Time.deltaTime;
        if (_currentTime < 0f)
        {
            _cameras[_cameraIndex].SetActive(false);
            if (_cameraIndex < _cameras.Length - 1)
            {
                _cameraIndex++;
                _cameras[_cameraIndex].SetActive(true);
                _cameras[_cameraIndex].GetComponent<EnableOrDisableObjs>().SwapObjs();
                _currentTime = _timeBeforeCameraChange;
                if (_cameraIndex == _cameras.Length - 1)
                {
                    for (int i = 0; i < _anims.Length; i++)
                    {
                        _anims[i].SetTrigger(_animTriggers[i]);
                    }
                    _currentTime = _timeBeforeLastCameraChange;
                }
            }
            else
            {
                _disableObjs.SwapObjs();
                _player.SetActive(true);
                Destroy(this.gameObject);
            }
        }
    }
}
