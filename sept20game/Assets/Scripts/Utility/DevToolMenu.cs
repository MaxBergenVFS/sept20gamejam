﻿using UnityEngine;

public class DevToolMenu : MonoBehaviour
{
    [SerializeField]
    private GameObject _devToolsMenu;

    private void Start()
    {
        _devToolsMenu.SetActive(false);
    }

    public void ToggleMenu()
    {
        _devToolsMenu.SetActive(!_devToolsMenu.activeSelf);
    }
}
