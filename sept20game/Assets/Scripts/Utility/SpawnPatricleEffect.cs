﻿using UnityEngine;

public class SpawnPatricleEffect : MonoBehaviour
{
    [SerializeField]
    private GameObject _particleEffect;

    public void InstantiateParticleEffect()
    {
        GameObject particle = Instantiate(_particleEffect, transform.position, Quaternion.identity);
        Destroy(particle, 5f);
    }
}
