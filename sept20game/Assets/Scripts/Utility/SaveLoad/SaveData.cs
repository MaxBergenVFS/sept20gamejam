﻿[System.Serializable]
public class SaveData
{
    public int[] playerInventoryAmts;
    public int playerCurrentHealth;
    public int playerMaxHealth;
    public int playerCurrentLevel;

    public SaveData (SaveGameInteract data)
    {
        playerCurrentLevel = data.playerCurrentLevel;
        playerCurrentHealth = data.playerCurrentHealth;
        playerMaxHealth = data.playerMaxHealth;
        playerInventoryAmts = data.playerInventoryAmts;
    }
}
