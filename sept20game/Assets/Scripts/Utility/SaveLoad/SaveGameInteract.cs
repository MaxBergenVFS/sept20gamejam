﻿using System.Collections.Generic;
using UnityEngine;

public class SaveGameInteract : MonoBehaviour, IInteractable
{
    [SerializeField]
    private GameObject _saveGameUI;

    private PlayerController _playerController;
    private PlayerFootstepSFX _footstepSFX;
    private PlayerHealth _playerHealth;
    private GameManager _gameManager;

    [HideInInspector]
    public int playerCurrentHealth;
    [HideInInspector]
    public int playerMaxHealth;
    [HideInInspector]
    public int playerCurrentLevel;
    [HideInInspector]
    public int[] playerInventoryAmts = new int[64];

    private void Start()
    {
        _playerController = PlayerController.instance;
        _footstepSFX = _playerController.GetComponent<PlayerFootstepSFX>();
        _playerHealth = _playerController.GetComponent<PlayerHealth>();
        _gameManager = GameManager.instance;
    }

    public void Interact()
    {
        _saveGameUI.SetActive(true);
        _footstepSFX.DontPlaySFXOnEnable();
        UpdateSaveData();
    }

    private void UpdateSaveData()
    {
        playerCurrentLevel = _gameManager.GetPreviousSceneIndex();
        //TO DO playerMaxHealth is set to 0 if no stamIncrease has been picked up yet
        playerMaxHealth = _gameManager.currentMaxHealthAmt;
        playerCurrentHealth = _playerHealth.GetCurrentHealth();
        BuildInventoryAmtArray(_gameManager.GetInventoryDatabaseAmtState());
    }

    public void BuildInventoryAmtArray(Dictionary<int, int> inventoryItems)
    {
        playerInventoryAmts = null;
        int[] itemArray = new int[64];
        foreach (var item in inventoryItems)
        {
            itemArray[item.Key] = item.Value;
        }
        playerInventoryAmts = itemArray;
    }

    public void SaveGame()
    {
        SaveSystem.SaveGame(this);
    }

    public void LoadGame()
    {
        SaveData data = SaveSystem.LoadGame();
        GameManager gameManager = GameManager.instance;
        PlayerHealth playerHealth = PlayerController.instance.GetComponent<PlayerHealth>();;
        playerCurrentLevel = data.playerCurrentLevel;
        playerCurrentHealth = data.playerCurrentHealth;
        playerMaxHealth = data.playerMaxHealth;
        GameManager.instance.OverwritePreviousSceneIndex(playerCurrentLevel);
        GameManager.instance.SetCurrentHealthAmt(playerCurrentHealth);
        GameManager.instance.currentMaxHealthAmt = playerMaxHealth;
        GameManager.instance.tempMaxHealthAmt = playerMaxHealth;
        playerHealth.SetCurrentHealthAmt(playerCurrentHealth);
        playerHealth.SetMaxHealthAmt(playerMaxHealth);
    }
}
