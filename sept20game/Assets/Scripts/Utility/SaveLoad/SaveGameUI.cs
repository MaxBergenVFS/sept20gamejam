﻿using UnityEngine.UI;
using UnityEngine;

public class SaveGameUI : MonoBehaviour
{
    [SerializeField]
    private Button _yesButton;
    [SerializeField]
    private Button _noButton;
    [SerializeField]
    private GameObject _levelFadeCanvas;
    [SerializeField]
    private GameObject _gameIsBeingSavedText;
    [SerializeField]
    private InputManager _playerInput;
    [SerializeField]
    private SaveGameInteract _saveGameInteract;

    private Animator _anim;
    private string _SFXPath = "event:/UI/SaveGame";

    private void Start()
    {
        Button yesBtn = _yesButton.GetComponent<Button>();
        yesBtn.onClick.AddListener(SaveGame);
        Button noBtn = _noButton.GetComponent<Button>();
        noBtn.onClick.AddListener(CloseSaveUI);
        _anim = _levelFadeCanvas.GetComponentInChildren<Animator>();
    }

    private void OnEnable()
    {
        if (_levelFadeCanvas) { _levelFadeCanvas.SetActive(false); }
        else { Debug.LogError($"Missing LevelFadeCanvas reference on {this.gameObject.name}"); }
        FreezeTime(true);
    }

    private void OnDisable()
    {
        _playerInput.enabled = true;
    }

    void SaveGame()
    {
        print("you've saved the game");
        FreezeTime(false);
        _levelFadeCanvas.SetActive(true);
        _gameIsBeingSavedText.SetActive(true);
        FMODUnity.RuntimeManager.PlayOneShot(_SFXPath);
        this.gameObject.SetActive(false);
        _saveGameInteract.SaveGame();
    }

    void CloseSaveUI()
    {
        FreezeTime(false);
        this.gameObject.SetActive(false);
    }

    void FreezeTime(bool cond)
    {
        if (cond)
        {
            if (_playerInput) { _playerInput.enabled = false; }
            else { Debug.LogError($"Missing InputManager reference on {this.gameObject.name}"); }
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0;
        }
        else
        {
            if (_playerInput) { _playerInput.enabled = true; }
            else { Debug.LogError($"Missing InputManager reference on {this.gameObject.name}"); }
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            Time.timeScale = 1;
        }
    }
}
