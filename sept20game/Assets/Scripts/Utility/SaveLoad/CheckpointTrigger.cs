﻿using UnityEngine;

public class CheckpointTrigger : TriggerCheck
{
    [SerializeField]
    private Transform _playerSpawnPos;

    private Vector3 _checkpointPos;
    private CheckpointManager _checkpointManager;

    private void Awake()
    {
        if (_playerSpawnPos) { _checkpointPos = _playerSpawnPos.position; }
        else { _checkpointPos = this.transform.position; }
    }

    private void Start()
    {
        _checkpointManager = CheckpointManager.instance;
    }

    protected override void TriggerAction()
    {
        base.TriggerAction();
        _checkpointManager.SetCurrentCheckpoint(this);
    }

    public Vector3 GetCheckpointPos() { return _checkpointPos; }
}
