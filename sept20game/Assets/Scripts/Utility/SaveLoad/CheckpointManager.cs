﻿using System.Collections.Generic;
using UnityEngine;

public class CheckpointManager : MonoBehaviour
{
    [SerializeField]
    private List<CheckpointTrigger> _checkpointsInScene = new List<CheckpointTrigger>();

    public static CheckpointManager instance { get { return _instance; } }

    private static CheckpointManager _instance;
    private int _currentCheckpointNum;
    private PlayerController _playerController;
    private CharacterController _characterController;
    private PlayerHealth _playerHealth;
    private Transform _playerTransform;
    private int _playerCurrentHealth;

    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        _playerController = PlayerController.instance;
        _playerTransform = _playerController.gameObject.transform;
        _characterController = _playerController.GetComponent<CharacterController>();
        _playerHealth = _playerController.GetComponent<PlayerHealth>();
    }

    public void SetCurrentCheckpoint(CheckpointTrigger checkpoint)
    {
        _currentCheckpointNum = _checkpointsInScene.IndexOf(checkpoint);
        _playerCurrentHealth = _playerHealth.GetCurrentHealth();
        print($"current checkpoint is now: {_currentCheckpointNum}");
    }

    public void SpawnPlayerAtRecentCheckpoint()
    {
        Vector3 pos = _checkpointsInScene[_currentCheckpointNum].GetCheckpointPos();
        _characterController.enabled = false;
        _playerTransform.position = pos;
        _playerHealth.SetPlayerInRespawnState(_playerCurrentHealth);
        _characterController.enabled = true;
    }


}
