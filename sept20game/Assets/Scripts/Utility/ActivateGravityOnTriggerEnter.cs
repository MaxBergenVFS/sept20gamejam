﻿using UnityEngine;

public class ActivateGravityOnTriggerEnter : TriggerCheck
{
    [SerializeField]
    private Rigidbody _rb;
    [SerializeField]
    private bool _playSFX = false;
    [SerializeField]
    private string _SFXPath = "Interactables/RockCrumble";

    protected override void TriggerAction()
    {
        base.TriggerAction();
        _rb.useGravity = true;
        if(_playSFX) FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/" + _SFXPath, transform.position);
    }
}
