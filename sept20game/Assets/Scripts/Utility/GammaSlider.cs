﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class GammaSlider : MonoBehaviour
{
    [SerializeField]
    private PostProcessVolume _ppv;
    // [SerializeField]
    // private float _brightnessMin = 0f;
    // [SerializeField]
    // private float _brightnessMax = 350f;

    private ColorGrading _cg;

    private void Start()
    {
        _ppv.profile.TryGetSettings(out _cg);
    }

    public void SetBrightness(float brightness)
    {
        _cg.brightness.value = brightness;
    }

}
