﻿using UnityEngine;

public class HeldWeapon : MonoBehaviour
{
    private PlayerAttack _playerAttack;
    private WeaponSFX _sfx;

    [SerializeField] private GameObject _UIElement;
    [SerializeField] private int _atkLvl = 1, _baseDamage = 20;
    [SerializeField]
    private bool _canChargeAttack = false, _canDeflectProjectiles = false,
        _thisIsBreakable = false;
    [SerializeField] private float _critChance = 0.0f;

    private void Awake()
    {
        _playerAttack = GetComponentInParent<PlayerAttack>();
        _sfx = GetComponentInParent<WeaponSFX>();
    }

    private void Start()
    {
        if (this.gameObject.activeSelf && !GameManager.instance.playerHasSword)
            GameManager.instance.playerHasSword = true;
    }

    private void OnEnable()
    {
        _playerAttack.hasSword = true;
        _playerAttack.EquipSword();
        _UIElement.SetActive(true);
        _sfx.SetSwordSwingSound(_thisIsBreakable);
        _playerAttack.sword.SetCurrentHeldWeapon(this, _UIElement);
        _playerAttack.InitWeaponValues(_baseDamage, _atkLvl, _canChargeAttack, _canDeflectProjectiles);
        if (GameManager.instance == null) return;
        if (!GameManager.instance.playerHasSword) GameManager.instance.playerHasSword = true;
    }

    private void OnDisable()
    {
        _playerAttack.hasSword = false;
        if (_UIElement) _UIElement.SetActive(false);
    }

    public int GetBaseDamage() { return _baseDamage; }
    public int GetAtkLvl() { return _atkLvl; }
    public bool CheckIfCanChargeAttack() { return _canChargeAttack; }
    public bool CheckIfDeflectProjectiles() { return _canDeflectProjectiles; }
    public bool CheckIfIsBreakable() { return _thisIsBreakable; }
}
