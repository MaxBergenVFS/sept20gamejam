﻿using System.Collections;
using UnityEngine;

public class PlayerInteract : MonoBehaviour
{
    private GameObject _interactableItem = null;
    private PlayerSFX _playerSFX;
    private bool _forceInteract = false;
    private float _defaultTemporaryDisableInteractDuration = 0.5f;

    [HideInInspector] public bool interactIsDisabled = false;

    private void Awake() => _playerSFX = GetComponent<PlayerSFX>();

    public void Interact()
    {
        if (_interactableItem == null) { _playerSFX.PlayInteractSoundNeg(); }
        else
        {
            _interactableItem.GetComponent<IInteractable>().Interact();
            _playerSFX.PlayInteractSoundPos();
        }
    }

    public void DisableInteractTemporary()
    {
        StopAllCoroutines();
        StartCoroutine(DisableInteractTimer(_defaultTemporaryDisableInteractDuration));
    }

    public void DisableInteractTemporary(float duration)
    {
        StopAllCoroutines();
        StartCoroutine(DisableInteractTimer(duration));
    }

    IEnumerator DisableInteractTimer(float duration)
    {
        interactIsDisabled = true;
        yield return new WaitForSeconds(duration);
        interactIsDisabled = false;
    }

    public void AllowInteract(GameObject obj)
    {
        if (_forceInteract) return;
        _interactableItem = obj;
    }

    public void ResetInteract()
    {
        if (_forceInteract) return;
        _interactableItem = null;
    }

    public void ForceAllowInteract(GameObject obj)
    {
        _forceInteract = false;
        ResetInteract();
        _forceInteract = true;
        _interactableItem = obj;
    }

    public void ForceResetInteract() => _forceInteract = false;
}
