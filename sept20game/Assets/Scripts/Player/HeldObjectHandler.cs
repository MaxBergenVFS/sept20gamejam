﻿using UnityEngine;

public class HeldObjectHandler : MonoBehaviour
{
    [SerializeField]
    private float _heldObjOffsetAllowance = 0.5f;
    [SerializeField]
    private float _raycastDist = 0.5f;
    [SerializeField]
    private Transform _minHeightRef;
    [SerializeField]
    private Transform _aboveMinHeightRef;

    private RaycastHit hit;
    private float _currentPosOffset;
    private Transform _transform;
    private Transform _heldObjRef;
    private Rigidbody _heldObjRb;
    private Vector3 _startingPos;
    private Vector3 _currentPos;

    private void Awake()
    {
        _transform = this.transform;
        _startingPos = _transform.localPosition;
    }

    public void HandleHeldObject(GameObject obj)
    {
        _heldObjRef = obj.transform;
        _heldObjRb = obj.GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        _transform.localPosition = _startingPos;
        if (_aboveMinHeightRef.position.y < _minHeightRef.position.y)
        {
            _currentPos = new Vector3(_transform.position.x, _minHeightRef.position.y, _transform.position.z);
            _transform.position = _currentPos;
        }
    }

    private void OnDisable()
    {
        _heldObjRef = null;
    }

    private void Update()
    {
        if (_heldObjRef == null) return;
        CheckOffset(_heldObjRef.position.x);
        CheckOffset(_heldObjRef.position.y);
        CheckOffset(_heldObjRef.position.z);
        UpdateHandlerYPos();
    }

    private void UpdateHandlerYPos()
    {
        if (_aboveMinHeightRef.position.y < _minHeightRef.position.y)
        {
            _currentPos = new Vector3(_transform.position.x, _minHeightRef.position.y, _transform.position.z);
        }
        else
        {
            _currentPos = new Vector3(_transform.position.x, _aboveMinHeightRef.position.y, _transform.position.z);
        }
        _transform.position = _currentPos;
    }

    private void ResetObjPos()
    {
        _heldObjRb.isKinematic = true;
        _heldObjRef.localPosition = Vector3.zero;
        _heldObjRb.isKinematic = false;
    }

    private void CheckOffset(float objPos)
    {
        if (objPos > _heldObjOffsetAllowance || objPos < -_heldObjOffsetAllowance) ResetObjPos();
    }
}
