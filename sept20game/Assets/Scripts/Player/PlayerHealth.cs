﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class PlayerHealth : MonoBehaviour, IDamageable
{
    [SerializeField]
    private float _healthUIDisplayTimeout = 1f;
    [SerializeField]
    private float _timeBeforeHidingDamageUI = 3f;
    //[SerializeField]
    private int _defenseLevel = 1;
    [SerializeField]
    private Image _healthBarUI = default;
    [SerializeField]
    private GameObject _flashingHealthBarUI;
    [SerializeField]
    private TextMeshProUGUI _healthNum;
    [SerializeField]
    private GameObject _aboutToDieUI = default;
    [SerializeField]
    private GameObject _takingDamageUI = default;
    [SerializeField]
    private GameObject _gameOverScreen;
    [SerializeField]
    private float _limitedInvincibiltyTime = 0.5f;
    [SerializeField]
    private int _maxHealthAmount = 100;

    private float _timeBeforeOutOfHealthTimeout = 5f;
    private int _healthAmtToReplenishAfterOutOfHealthTimeout = 5;
    private bool _canTakeDamage = true;
    private bool _takingDamage = false;
    private float _currentHealthUIDisplayTimeout;
    private PlayerController _playerController;
    private PlayerSFX _playerSFX;
    private PlayerAttack _playerAttack;
    private GameManager _gameManager;
    private bool _hasEnteredOutOfHealthStatus = false;
    private bool _drainingHealth = false;
    private bool _outOfHealth = false;
    private int _currentHealthAmount;
    private bool _nextHitWillKill = false;
    private bool _beingAttacked = false;
    private float _currentHealthBarFillAmt;
    private bool _healthIsFull = true;

    private void Awake()
    {
        _playerController = GetComponent<PlayerController>();
        _playerSFX = GetComponent<PlayerSFX>();
        _playerAttack = GetComponent<PlayerAttack>();
        _flashingHealthBarUI.SetActive(false);
    }

    private void Start()
    {
        GetCorrectMaxHealthAmt();
        _gameManager = GameManager.instance;
        _currentHealthAmount = _gameManager.GetCurrentHealthAmt();
        _healthNum.SetText(_currentHealthAmount.ToString());
    }

    public bool CheckIfFlesh() { return true; }

    public void ReduceHealthByAmt(int amt)
    {
        if (!_takingDamage) return;
        _currentHealthAmount -= amt;
        UpdateHealthAmtUtility();
    }

    public void SetCurrentHealthAmt(int amt)
    {
        _currentHealthAmount = amt;
        UpdateHealthAmtUtility();
        UpdateHealthBarUI(_currentHealthAmount);
    }

    public void SetMaxHealthAmt(int amt)
    {
        _maxHealthAmount = amt;
        GameManager.instance.currentMaxHealthAmt = amt;
    }

    private void UpdateHealthAmtUtility()
    {
        CheckHealthAmt();
        GameManager.instance.SetCurrentHealthAmt(_currentHealthAmount);
        if (_currentHealthAmount < _maxHealthAmount) { _healthIsFull = false; }
        else { _healthIsFull = true; }
        _takingDamage = false;
    }

    public int GetCurrentHealth() { return _currentHealthAmount; }
    public void IsDrainingHealth(bool cond) { _drainingHealth = cond; }
    public bool CheckIfOutOfHealth() { return _outOfHealth; }

    public void TakeDamage(int amt, bool cond)
    {
        if (!_canTakeDamage) return;

        if (_playerAttack.CheckIfBlocking())
        {
            _playerAttack.PlaySparks();
            _playerAttack.sword.PlayBlockSFX();
        }
        else
        {
            StopCoroutine(StartLimitedInvincibilityTimer());
            StartCoroutine(StartLimitedInvincibilityTimer());
            _beingAttacked = _takingDamage = true;
            StopCoroutine(LerpHealthAmtDisplay(_currentHealthAmount, _currentHealthAmount - amt));
            StartCoroutine(LerpHealthAmtDisplay(_currentHealthAmount, _currentHealthAmount - amt));
            ReduceHealthByAmt(amt);
            DisplayTakingDamageUI();
            if (!_nextHitWillKill) _playerSFX.PlayPainSound();
            if (_outOfHealth)
            {
                if (!_nextHitWillKill)
                {
                    _aboutToDieUI.SetActive(true);
                    _nextHitWillKill = true;
                    StartCoroutine(StartOutOfHealthTimeout());
                }
                else
                {
                    StopCoroutine(StartOutOfHealthTimeout());
                    GameOver(true);
                }
            }
        }
    }

    IEnumerator StartOutOfHealthTimeout()
    {
        yield return new WaitForSeconds(_timeBeforeOutOfHealthTimeout);
        _aboutToDieUI.SetActive(false);
        _outOfHealth = _nextHitWillKill = false;
        StopCoroutine(LerpHealthAmtDisplay(_currentHealthAmount, _healthAmtToReplenishAfterOutOfHealthTimeout));
        StartCoroutine(LerpHealthAmtDisplay(_currentHealthAmount, _healthAmtToReplenishAfterOutOfHealthTimeout));
    }

    private void DisplayTakingDamageUI()
    {
        _takingDamageUI.SetActive(true);
        StopCoroutine(StartDamageUITimer());
        StartCoroutine(StartDamageUITimer());
    }

    IEnumerator StartDamageUITimer()
    {
        yield return new WaitForSeconds(_timeBeforeHidingDamageUI);
        _takingDamageUI.SetActive(false);
    }

    IEnumerator LerpHealthAmtDisplay(int startValue, int endValue)
    {
        float lerpValue = 0f;
        float lerpSpeed = 1f;
        float floatValueToDisplay;
        int intValueToDisplay;

        _healthNum.SetText(startValue.ToString());
        while (lerpValue < 1f)
        {
            lerpValue += Time.deltaTime / lerpSpeed;
            floatValueToDisplay = Mathf.Lerp(startValue, endValue, lerpValue);
            intValueToDisplay = (int)floatValueToDisplay;
            _healthNum.SetText(intValueToDisplay.ToString());
            yield return null;
        }
        _healthNum.SetText(endValue.ToString());
    }

    public bool CompareDefenseLevel(int atkLvl)
    {
        if (atkLvl >= _defenseLevel) return true;
        return false;
    }

    public bool CheckIfDead() { return false; }

    public void ResetHealthRefillTimer()
    {
        if (_takingDamageUI.activeSelf) { _takingDamageUI.SetActive(false); }
        StopCoroutine(LerpHealthAmtDisplay(_currentHealthAmount, _maxHealthAmount));
        StartCoroutine(LerpHealthAmtDisplay(_currentHealthAmount, _maxHealthAmount));
        _currentHealthAmount = _maxHealthAmount;
        _outOfHealth = _beingAttacked = _nextHitWillKill = _hasEnteredOutOfHealthStatus = false;
        _aboutToDieUI.SetActive(false);
        _flashingHealthBarUI.SetActive(false);
        _playerController.LowHealthStatus(false);
        _currentHealthBarFillAmt = _healthBarUI.fillAmount;
        _healthIsFull = true;
    }

    public void ResetHealthRefillTimer(int amtToIncreaseHealthBy)
    {
        if (_takingDamageUI.activeSelf) { _takingDamageUI.SetActive(false); }

        int newHealthAmount;

        if ((_currentHealthAmount + amtToIncreaseHealthBy) > _maxHealthAmount)
        { newHealthAmount = _maxHealthAmount; }
        else { newHealthAmount = _currentHealthAmount + amtToIncreaseHealthBy; }

        StopCoroutine(LerpHealthAmtDisplay(_currentHealthAmount, newHealthAmount));
        StartCoroutine(LerpHealthAmtDisplay(_currentHealthAmount, newHealthAmount));
        _currentHealthAmount = newHealthAmount;

        _outOfHealth = _beingAttacked = _nextHitWillKill = _hasEnteredOutOfHealthStatus = false;
        _aboutToDieUI.SetActive(false);
        _flashingHealthBarUI.SetActive(false);
        _currentHealthBarFillAmt = _healthBarUI.fillAmount;
        _healthIsFull = (_currentHealthAmount == _maxHealthAmount);
    }

    private void CheckHealthAmt()
    {
        if (UtilityHelper.CheckIfDepleted(_currentHealthAmount))
        {
            UpdateHealthBarUI(_currentHealthAmount);
            _playerController.Sprint(false);
            _outOfHealth = true;
        }
    }

    private void UpdateHealthBarUI(int amt)
    {
        string amtToDisplay;
        if (amt < 0) { amtToDisplay = "0"; }
        else { amtToDisplay = amt.ToString(); }
        _healthNum.SetText(amtToDisplay);
    }

    public void IncreaseMaxHealth(int amt)
    {
        _maxHealthAmount += amt;
        _gameManager.tempMaxHealthAmt = _maxHealthAmount;
        //UpdateHealthBarUI(_maxHealthAmount);
        ResetHealthRefillTimer();
    }

    private void GameOver(bool cond)
    {
        if (_beingAttacked) _beingAttacked = false;
        if (_takingDamageUI.activeSelf) _takingDamageUI.SetActive(false);
        if (cond) _playerSFX.PlayDeathSound();
        _gameOverScreen.SetActive(cond);
        _gameManager.GameOver(cond);
    }

    public void SetPlayerInRespawnState(int health)
    {
        SetCurrentHealthAmt(health);
        GameOver(false);
    }

    private void GetCorrectMaxHealthAmt()
    {
        if (GameManager.instance)
        {
            GameManager gameManager = GameManager.instance;
            if (_maxHealthAmount < gameManager.currentMaxHealthAmt)
            {
                _maxHealthAmount = gameManager.currentMaxHealthAmt;
            }
            gameManager.playerIsDead = false;
        }
        else { Debug.LogError($"Missing GameManager in scene"); }
    }

    public bool CheckIfHealthIsFull() { return _healthIsFull; }

    IEnumerator StartLimitedInvincibilityTimer()
    {
        _canTakeDamage = false;
        yield return new WaitForSeconds(_limitedInvincibiltyTime);
        _canTakeDamage = true;
    }
}
