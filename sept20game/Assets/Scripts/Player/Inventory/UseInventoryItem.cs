﻿using System.Collections;
using UnityEngine;

public class UseInventoryItem : MonoBehaviour
{
    private PlayerController _playerController;
    private PlayerHealth _playerHealth;
    private PlayerInventory _playerInventory;
    private InventoryItemDatabase _database;
    private int _soulID, _healthRefillID, _vanishPowderID, _strangeAmuletID;
    private int _healthPickupIncreaseAmt = 25, _potionRefillAmt = 20;
    private bool _onCooldown = false;
    private float _cooldownTime = 0.5f;
    private string _useHealthPotionSFXPath = "SFX/Player/UsePotion";
    private string _amuletNotificationText = "It doesn't seem to have any effect here";
    private string _healthPotionNotificationText = "You already have full health";

    [SerializeField] private DayNightCycle _dayNightCycle;

    private void Awake() => _database = GetComponent<InventoryItemDatabase>();

    private void Start()
    {
        _playerController = PlayerController.instance;
        _playerHealth = _playerController.GetComponent<PlayerHealth>();
        _playerInventory = _playerController.GetComponent<PlayerInventory>();
    }

    public void InitDatabaseIDs(int soulID, int healthRefillID, int strangeAmuletID)
    {
        _soulID = soulID;
        _healthRefillID = healthRefillID;
        _strangeAmuletID = strangeAmuletID;
    }

    public void Use(int id)
    {
        if (_onCooldown) return;

        if (id == _soulID) { UseSoul(); }
        else if (id == _healthRefillID) { UseHealthPotion(); }
        else if (id == _strangeAmuletID) { UseStrangeAmulet(); }
        else { return; }

        StopAllCoroutines();
        StartCoroutine(StartCooldownTimer());
    }

    private void UseSoul()
    {
        _database.AddToInventoryItemAmt(_soulID, -1);
        _playerHealth.IncreaseMaxHealth(_healthPickupIncreaseAmt);
        _playerInventory.RefreshItemAmtDisplay();
    }

    private void UseStrangeAmulet()
    {
        if (_dayNightCycle != null) { _dayNightCycle.ChangeTimeOfDay(); }
        else { NotificationUI.instance.DisplayTxt(_amuletNotificationText); }
    }

    private void UseHealthPotion()
    {
        if (_playerHealth.CheckIfHealthIsFull())
        {
            NotificationUI.instance.DisplayTxt(_healthPotionNotificationText);
        }
        else
        {
            _database.AddToInventoryItemAmt(_healthRefillID, -1);
            _playerInventory.RefreshItemAmtDisplay();
            _playerHealth.ResetHealthRefillTimer(_potionRefillAmt);
            FMODUnity.RuntimeManager.PlayOneShot("event:/" + _useHealthPotionSFXPath);
        }
    }

    IEnumerator StartCooldownTimer()
    {
        _onCooldown = true;
        yield return new WaitForSeconds(_cooldownTime);
        _onCooldown = false;
    }
}
