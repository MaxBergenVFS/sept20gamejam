﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UseInventoryItem))]
public class InventoryItemDatabase : MonoBehaviour
{
    #region Description Text
    [SerializeField]
    [TextArea(3, 10)]
    private string _halflingSwordDescription;
    [SerializeField]
    [TextArea(3, 10)]
    private string _goldDescription;
    [SerializeField]
    [TextArea(3, 10)]
    private string _soulDescription;
    [SerializeField]
    [TextArea(3, 10)]
    private string _keyringDescription;
    [SerializeField]
    [TextArea(3, 10)]
    private string _healthPotionDescription;
    [SerializeField]
    [TextArea(3, 10)]
    private string _turnipDescription;
    [SerializeField]
    [TextArea(3, 10)]
    private string _gibletDescription;
    [SerializeField]
    [TextArea(3, 10)]
    private string _strangeAmuletDescription;
    [SerializeField]
    [TextArea(3, 10)]
    private string _travelPackDescription;
    [SerializeField]
    [TextArea(3, 10)]
    private string _orbDescription;
    [SerializeField]
    [TextArea(3, 10)]
    private string _tabletChunkDescription;
    [SerializeField]
    [TextArea(3, 10)]
    private string _teleportPlantDescription;
    #endregion

    private List<InventoryItem> _inventoryItems;
    private bool _buildDatabaseOnAwake = true;
    private UseInventoryItem _useInventoryItem;

    private void Awake()
    {
        if (_buildDatabaseOnAwake) BuildDatabase();
        _useInventoryItem = GetComponent<UseInventoryItem>();
    }

    private void Start()
    {
        LoadInventoryDatabaseAmtList();
        _useInventoryItem.InitDatabaseIDs(2, 6, 7);
    }

    private void BuildDatabase()
    {
        _inventoryItems = new List<InventoryItem>(){
            new InventoryItem(0, "Halfling Sword", _halflingSwordDescription, false, true, 1),
            new InventoryItem(1, "Gold Pieces", _goldDescription, false, false, 0),
            new InventoryItem(2, "Tortured Soul", _soulDescription, true, false, 0),
            new InventoryItem(3, "Lucky Turnip", _turnipDescription, false, true, 0),
            new InventoryItem(4, "Giblet", _gibletDescription, false, false, 0),
            new InventoryItem(5, "Keyring", _keyringDescription, false, false, 0),
            new InventoryItem(6, "Health Tonic", _healthPotionDescription, true, false, 0),
            new InventoryItem(7, "Strange Amulet", _strangeAmuletDescription, true, true, 0),
            new InventoryItem(8, "Teleport Plant", _teleportPlantDescription, false, true, 0),
            new InventoryItem(9, "", "", false, false, 0),
            new InventoryItem(10, "Travel Pack", _travelPackDescription, false, true, 1),
            new InventoryItem(11, "Red Orb", _orbDescription, false, true, 0),
            new InventoryItem(12, "Green Orb", _orbDescription, false, true, 0),
            new InventoryItem(13, "Blue Orb", _orbDescription, false, true, 0),
            new InventoryItem(14, "", "Smoke Me", false, false, 0),
            new InventoryItem(15, "Crossbow", "Press R to shoot", false, true, 0),
            new InventoryItem(16, "Bolt", "Ammo for the crossbow", false, false, 0),
            new InventoryItem(17, "Tablet Chunk", _tabletChunkDescription, false, true, 0),
        };
    }

    public void SaveInventoryDatabaseAmtList(Dictionary<int, int> databaseAmtState)
    {
        foreach (InventoryItem item in _inventoryItems)
        {
            if (databaseAmtState.ContainsKey(item.id)) { databaseAmtState[item.id] = item.amount; }
            else { databaseAmtState.Add(item.id, item.amount); }
        }
    }

    private void LoadInventoryDatabaseAmtList()
    {
        Dictionary<int, int> databaseAmtDict = GameManager.instance.GetInventoryDatabaseAmtState();
        foreach (int key in databaseAmtDict.Keys)
        {
            SetInventoryItemAmt(key, databaseAmtDict[key]);
        }
    }

    public List<int> GetListOfValidIds()
    {
        List<int> ids = new List<int>();
        foreach (InventoryItem item in _inventoryItems)
        {
            if (item.amount > 0) ids.Add(item.id);
        }
        return ids;
    }

    private InventoryItem GetInventoryItem(int id)
    {
        return _inventoryItems.Find(inventoryItem => inventoryItem.id == id);
    }
    private InventoryItem GetInventoryItem(string title)
    {
        return _inventoryItems.Find(inventoryItem => inventoryItem.title == title);
    }

    public string GetInventoryItemName(int id) { return GetInventoryItem(id).title; }
    public int GetInventoryItemID(string title) { return GetInventoryItem(title).id; }

    public string GetInventoryItemDescription(int id) { return GetInventoryItem(id).description; }

    public int GetInventoryItemAmt(int id) { return GetInventoryItem(id).amount; }
    public void SetInventoryItemAmt(int id, int amt)
    {
        if (amt < 1) amt = 0;
        GetInventoryItem(id).amount = amt;
    }
    public void AddToInventoryItemAmt(int id, int amt)
    {
        int newAmt = GetInventoryItemAmt(id) + amt;
        if (newAmt < 1) newAmt = 0;
        SetInventoryItemAmt(id, newAmt);
    }

    public bool CheckIfCanUseInventoryItem(int id) { return GetInventoryItem(id).canUseInInventory; }

    public Sprite GetInventoryItemImage(int id) { return GetInventoryItem(id).image; }

    public GameObject GetInventoryItemModel(int id) { return GetInventoryItem(id).model; }

    public bool CheckIfInventoryItemIsUnique(int id) { return GetInventoryItem(id).unique; }

    public void UseItemInInventory(int id) { _useInventoryItem.Use(id); }
}
