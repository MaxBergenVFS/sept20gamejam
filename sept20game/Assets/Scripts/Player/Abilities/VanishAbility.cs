﻿using System.Collections;
using UnityEngine;

public class VanishAbility : BaseAbility, ICastable
{
    [SerializeField]
    private Animator _magicArmAnim;
    [SerializeField]
    private float _vanishLength = 2f;

    public override void CastAbilityBtnDown()
    {
        if (_usesReagent && !_playerInventory.CheckIfHasReagent(_databaseID)) return;

        if (_magicArmAnim) _magicArmAnim.SetTrigger("Attack");
        StartCooldown();
        ConsumeResources(_databaseID);

        if (_abilityDelayTime > 0) { StartCoroutine(DelayVanish()); }
        else { EnableVanish(); }
    }

    private void EnableVanish()
    {
        _playerController.SetIsVanished(true);
        StartCoroutine(DisableVanish());
        print("vanish");
    }

    IEnumerator DelayVanish()
    {
        yield return new WaitForSeconds(_abilityDelayTime);
        EnableVanish();
    }

    IEnumerator DisableVanish()
    {
        yield return new WaitForSeconds(_vanishLength);
       _playerController.SetIsVanished(false);
    }
}
