﻿using System.Collections;
using UnityEngine;

public class DistractionAbility : BaseAbility, ICastable
{
    [SerializeField]
    private GameObject _distractionObject;
    [SerializeField]
    private GameObject _distractionPreview;
    [SerializeField]
    private LayerMask _distractableLayers;
    [SerializeField]
    private int _magicStamCost = 20;
    [SerializeField]
    private float _distractRange = 10f;
    // [SerializeField]
    // private Animator _magicArmAnim;
    [SerializeField]
    private LayerMask _attackableLayers;

    private float _distractRayDist;
    private bool _canDistract = false;
    private bool _previewIsActive = false;

    protected override void Start()
    {
        base.Start();
        _distractRayDist = _playerController.GetObjThrowDist();
    }

    public override void CastAbilityBtnDown()
    {
        if (_usesReagent && !_playerInventory.CheckIfHasReagent(_databaseID)) return;
        _canDistract = true;
        _previewIsActive = true;
        _distractionPreview.SetActive(_previewIsActive);
    }

    public override void CastAbilityBtnHold()
    {
        if (!_canDistract) return;

        Ray ray = _camera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, _distractRayDist, _distractableLayers))
        {
            _distractionPreview.transform.position = hit.point;
            if (!_previewIsActive)
            {
                _previewIsActive = true;
                _distractionPreview.SetActive(_previewIsActive);
            }
        }
        else
        {
            _previewIsActive = false;
            _distractionPreview.SetActive(_previewIsActive);
        }
    }

    public override void CastAbilityBtnUp()
    {
        if (!_canDistract) return;

        Ray ray = _camera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, _distractRayDist, _distractableLayers))
        {
            //if (_magicArmAnim) _magicArmAnim.SetTrigger("Attack");
            Distraction(hit.point);
            StartCooldown();
        }
        else
        {
            _distractionPreview.SetActive(false);
            _canDistract = false;
        }
    }

    private void Distraction(Vector3 origin)
    {
        _distractionPreview.SetActive(false);
        _canDistract = false;
        ConsumeResources(_databaseID, _magicStamCost);
        StartCoroutine(Delay(origin));
    }

    IEnumerator Delay(Vector3 origin)
    {
        yield return new WaitForSeconds(_abilityDelayTime);

        Vector3 startingPos = PlayerController.instance.holdObjPos.position;

        GameObject feedbackObj = Instantiate(_distractionObject, startingPos, Quaternion.identity);
        ThrownObjArc thrownArc = feedbackObj.GetComponent<ThrownObjArc>();
        thrownArc.enabled = true;
        thrownArc.SetEndingPos(origin);

        Collider[] hitObj = Physics.OverlapSphere(origin, _distractRange, _attackableLayers);
        foreach (Collider obj in hitObj)
        {
            if (obj.GetComponent<IDistractable>() != null) obj.GetComponent<IDistractable>().Distract(origin);
        }
    }
}
