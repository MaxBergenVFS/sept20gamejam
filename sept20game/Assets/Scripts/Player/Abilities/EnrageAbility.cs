﻿using System.Collections;
using UnityEngine;

public class EnrageAbility : BaseAbility, ICastable
{
    [SerializeField]
    private Animator _magicArmAnim;
    [SerializeField]
    private float _enrageRayDist = 2f;
    [SerializeField]
    private float _enrageRange = 2f;
    [SerializeField]
    private int _magicStamCost = 20;
    [SerializeField]
    private EnrageTarget _enrageTargetObj;

    public override void CastAbilityBtnDown()
    {
        if (_usesReagent && !_playerInventory.CheckIfHasReagent(4)) return;

        Ray ray = _camera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, _enrageRayDist))
        {
            if (hit.transform.GetComponent<IDamageable>() != null)
            {
                StartCooldown();
                _magicArmAnim.SetTrigger("Attack");
                ConsumeResources(4, _magicStamCost);
                EnrageItem(hit.point, hit.transform.gameObject);
            }
        }
    }

    private void EnrageItem(Vector3 origin, GameObject target)
    {
        StartCoroutine(Delay(origin, target));
    }

    IEnumerator Delay(Vector3 origin, GameObject target)
    {
        yield return new WaitForSeconds(_abilityDelayTime);

        EnrageTarget obj = Instantiate(_enrageTargetObj, origin, Quaternion.identity);
        obj.SetTargetException(target);
        obj.transform.parent = target.transform;
    }
}
