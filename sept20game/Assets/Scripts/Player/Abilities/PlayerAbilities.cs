﻿using UnityEngine;

public class PlayerAbilities : MonoBehaviour
{
    [SerializeField]
    private float _globalCoolDown = 1.5f;
    [SerializeField]
    private GameObject[] _abilities;

    private ICastable _currentAbility;
    private int _abilityNum = 0;
    private bool _onCooldown = false;
    private float _currentCooldown;
    private PlayerHealth _playerHealth;
    private PlayerInventory _playerInventory;
    private PlayerAttack _playerAttack;

    private void Awake()
    {
        _playerHealth = GetComponent<PlayerHealth>();
        _playerInventory = GetComponent<PlayerInventory>();
        _playerAttack = GetComponent<PlayerAttack>();
        SetNextAbility(0);
        _currentCooldown = _globalCoolDown;
    }

    void Update()
    {
        if (!_onCooldown) return;

        _currentCooldown -= Time.deltaTime;

        if (_currentCooldown < 0f)
        {
            _onCooldown = false;
            _currentCooldown = _globalCoolDown;
        }
    }

    public void CycleAbilityPos()
    {
        if (_abilityNum > 0) { _abilityNum--; }
        else { _abilityNum = _abilities.Length - 1; }

        SetNextAbility(_abilityNum);
    }

    public void CycleAbilityNeg()
    {
        if (_abilityNum < _abilities.Length - 1) { _abilityNum++; }
        else { _abilityNum = 0; }

        SetNextAbility(_abilityNum);
    }

    public void AbilityDown()
    {
        if (_playerHealth.CheckIfOutOfHealth() || _onCooldown) return;
        _currentAbility.CastAbilityBtnDown();
    }

    public void AbilityHold() { _currentAbility.CastAbilityBtnHold(); }

    public void AbilityUp() { _currentAbility.CastAbilityBtnUp(); }

    public void SetNextAbility(int num)
    {
        _currentAbility = _abilities[num].GetComponent<ICastable>();
    }

    public void SetCooldownStart()
    {
        _onCooldown = true;
    }
}