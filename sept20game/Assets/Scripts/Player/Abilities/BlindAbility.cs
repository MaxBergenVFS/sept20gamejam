﻿using UnityEngine;

public class BlindAbility : BaseAbility, ICastable
{
    [SerializeField]
    private int _magicStamCost = 20;
    [SerializeField]
    private float _blindRayDist = 20f;
    [SerializeField]
    private Animator _magicArmAnim;
    [SerializeField]
    private LayerMask _blindableLayers;
    [SerializeField]
    private GameObject _blindingPowderParticles;

    private Transform _origin;

    protected override void Start()
    {
        base.Start();
        _origin = PlayerController.instance.holdObjPos;
    }

    public override void CastAbilityBtnDown()
    {
        if (_usesReagent && !_playerInventory.CheckIfHasReagent(_databaseID)) return;

        ConsumeResources(_databaseID, _magicStamCost);
        GameObject particle = Instantiate(_blindingPowderParticles, _origin.position, Quaternion.identity);
        Destroy(particle, 3f);

        Ray ray = _camera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, _blindRayDist, _blindableLayers))
        {
            if (hit.collider.GetComponent<IBlindable>() != null)
            {
                hit.collider.GetComponent<IBlindable>().Blind();
            }
        }
    }

}
