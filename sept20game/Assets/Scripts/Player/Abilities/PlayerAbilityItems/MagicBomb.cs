﻿using System.Collections;
using UnityEngine;

public class MagicBomb : BaseAbility, ICastable
{
    [SerializeField]
    private Animator _magicArmAnim;
    [SerializeField]
    private float _bombRayDist = 2f;
    [SerializeField]
    private int _magicStamCost = 20;
    [SerializeField]
    private MagicBombItem _magicBombItem;

    private int _reagentID = 4;

    public override void CastAbilityBtnDown()
    {
        if (_usesReagent && !_playerInventory.CheckIfHasReagent(_reagentID)) return;

        Ray ray = _camera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, _bombRayDist))
        {
            if (hit.transform.GetComponent<IExplodeable>() != null)
            {
                hit.transform.GetComponent<IExplodeable>().SetWillExplode();
                StartCooldown();
                _magicArmAnim.SetTrigger("Attack");
                ConsumeResources(_reagentID, _magicStamCost);
                BombItem(hit.point, hit.transform.gameObject);
            }
        }
    }

    private void BombItem(Vector3 origin, GameObject target)
    {
        StartCoroutine(Delay(origin, target));
    }

    IEnumerator Delay(Vector3 origin, GameObject target)
    {
        yield return new WaitForSeconds(_abilityDelayTime);

        MagicBombItem obj = Instantiate(_magicBombItem, origin, Quaternion.identity);
        obj.transform.parent = target.transform;
    }
}
