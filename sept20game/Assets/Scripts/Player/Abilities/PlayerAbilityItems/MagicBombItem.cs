﻿using System.Collections;
using UnityEngine;

public class MagicBombItem : MonoBehaviour
{
    [SerializeField]
    private float _damageRadius = 10f;
    [SerializeField]
    private int _damageAmount = 100;
    [SerializeField]
    private float _explosionDelayTime = 0.1f;
    [SerializeField]
    private LayerMask _damageableLayers;
    [SerializeField]
    private GameObject _explosionParticles;

    private IExplodeable _parentObj;

    private void OnEnable()
    {
        _parentObj = GetComponentInParent<IExplodeable>();
    }

    public void Explode()
    {
        StartCoroutine(Delay());
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(_explosionDelayTime);

        GameObject particle = Instantiate(_explosionParticles, transform.position, Quaternion.identity);
        Destroy(particle, 2f);
        Collider[] hitObj = Physics.OverlapSphere(transform.position, _damageRadius, _damageableLayers);
        foreach (Collider obj in hitObj)
        {
            if (obj.GetComponent<IDamageable>() != null)
            {
                obj.GetComponent<IDamageable>().TakeDamage(_damageAmount, false);
            }
        }
        Destroy(this.gameObject, 1f);
        //_parentObj.Disable();
    }
}
