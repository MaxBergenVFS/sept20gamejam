﻿using UnityEngine;

public class DistractionRange : MonoBehaviour
{
    private Transform _transform;

    private void Awake()
    {
        _transform = this.transform;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<IDistractable>() != null)
        {
            IDistractable distractable = other.gameObject.GetComponent<IDistractable>();
            distractable.Distract(_transform.position);
            //print($"{other.gameObject.name} is being distracted to {_transform.position}");
        }
    }
}
