﻿using System.Collections;
using UnityEngine;

public class HealthRefillAbility : BaseAbility, ICastable
{
    [SerializeField]
    private Animator _magicArmAnim;

    public override void CastAbilityBtnDown()
    {
        if (_playerHealth.CheckIfHealthIsFull()) return;
        if (_usesReagent && !_playerInventory.CheckIfHasReagent(_databaseID)) return;

        if (_magicArmAnim) _magicArmAnim.SetTrigger("Attack");
        StartCooldown();
        ConsumeResources(_databaseID);

        if (_abilityDelayTime > 0) { StartCoroutine(Delay()); }
        else { _playerHealth.ResetHealthRefillTimer(); }
    }

    IEnumerator Delay()
    {
        yield return new WaitForSeconds(_abilityDelayTime);

        _playerHealth.ResetHealthRefillTimer();
    }

}
