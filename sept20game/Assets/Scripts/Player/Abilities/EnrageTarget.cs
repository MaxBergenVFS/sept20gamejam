﻿using UnityEngine;

public class EnrageTarget : MonoBehaviour
{
    private IEnrageable _targetException;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<IEnrageable>() != null)
        {
            IEnrageable enrageable = other.gameObject.GetComponent<IEnrageable>();
            if (enrageable != _targetException)
            {
                enrageable.Enrage(this.transform.position);
            }
        }
    }

    public void SetTargetException(GameObject targetException)
    {
        if (targetException.GetComponent<IEnrageable>() != null)
        _targetException = targetException.GetComponent<IEnrageable>();
    }
}
