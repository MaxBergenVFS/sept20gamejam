﻿using UnityEngine;

public abstract class BaseAbility : MonoBehaviour
{
    [SerializeField]
    protected float _abilityDelayTime = 0.2f;
    [SerializeField]
    protected bool _usesReagent = true;

    private bool _hasCooldown = true;
    protected PlayerInventory _playerInventory;
    protected PlayerHealth _playerHealth;
    protected PlayerAbilities _playerAbilities;
    protected PlayerController _playerController;
    protected Camera _camera;
    protected int _databaseID;

    protected virtual void Awake()
    {
        _camera = Camera.main;
    }

    protected virtual void Start()
    {
        _playerController = PlayerController.instance;
        _playerInventory = _playerController.GetComponent<PlayerInventory>();
        _playerHealth = _playerController.GetComponent<PlayerHealth>();
        _playerAbilities = _playerController.GetComponent<PlayerAbilities>();
    }

    public void SetDatabaseID(int num)
    {
        _databaseID = num;
        //print($"_databaseID on {this.gameObject.name}: {_databaseID}");
    }

    protected void StartCooldown() { if (_hasCooldown) _playerAbilities.SetCooldownStart(); }

    protected void ConsumeResources(int id)
    {
        if (_usesReagent) _playerInventory.ReduceReagentItemAmt(id);
    }

    protected void ConsumeResources(int id, int healthAmt)
    {
        _playerHealth.ReduceHealthByAmt(healthAmt);
        if (_usesReagent) _playerInventory.ReduceReagentItemAmt(id);
    }

    public virtual void CastAbilityBtnDown() { }
    public virtual void CastAbilityBtnHold() { }
    public virtual void CastAbilityBtnUp() { }
}
