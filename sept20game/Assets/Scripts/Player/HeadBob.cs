﻿using UnityEngine;

public class HeadBob : MonoBehaviour
{
    [SerializeField]
    private float _baseSpeed = 4f;
    [SerializeField]
    private float _baseAmount = 0.01f;

    private float _defaultPosY = 0;
    private float _timer = 0;
    private bool _isMoving = false;
    private Transform _transform;
    private float _speedMultiplier = 1f;

    private void Awake()
    {
        _defaultPosY = transform.localPosition.y;
        _transform = transform;
    }

    private void Update()
    {
        BobHead();
    }

    private void BobHead()
    {
        _timer += Time.deltaTime * _baseSpeed * _speedMultiplier;
        _transform.localPosition = new Vector3(_transform.localPosition.x, _defaultPosY + Mathf.Sin(_timer) * _baseAmount, _transform.localPosition.z);
    }

    public void SetSpeed(bool isMoving, float speed)
    {
        if (isMoving && _speedMultiplier != speed)
        {
            _speedMultiplier = speed;
        }
        else if (!isMoving && _speedMultiplier != 1)
        {
            _speedMultiplier = 1f;
        }
        if (_isMoving != isMoving)
        {
            _isMoving = isMoving;
        }
    }
}
