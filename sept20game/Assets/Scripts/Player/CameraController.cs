﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private float _mouseXSensitivity = 100.0f;
    [SerializeField]
    private float _mouseYSensitivity = 150.0f;
    [SerializeField]
    private float _clampAngle = 85.0f;
    [SerializeField]
    private float _interactDistance = 3f;
    [SerializeField]
    private float _baseVignetteIntensity = 0.2f;
    [SerializeField]
    private float _vignetteIntensityMultiplier = 2f;
    [SerializeField]
    private float _vignetteLerpSpeed = 4f;
    [SerializeField]
    private GameObject _handIcon;
    [SerializeField]
    private HeldObjectHandler _heldObjectHandler;

    [HideInInspector]
    public bool holdingItem = false;

    private PlayerInteract _playerInteract;
    private PlayerController _playerController;
    private PlayerAttack _playerAttack;
    private bool _canInteract = false;
    private bool _startVignetteLerp = false;
    private float _vignetteValue;
    private float _lerpValue = 0f;
    private float _currentPlayerRot = 0.0f;
    private float _currentCamRot = 0.0f;
    private float _mouseX;
    private float _mouseY;
    private bool _vignetteOverride = false;
    private Camera _camera;
    private Vignette _vignette;
    private PostProcessVolume _ppv;

    void Awake()
    {
        _playerInteract = GetComponent<PlayerInteract>();
        _playerController = GetComponent<PlayerController>();
        _playerAttack = GetComponent<PlayerAttack>();
        Cursor.lockState = CursorLockMode.Locked;
        _camera = Camera.main;
        _ppv = _camera.GetComponent<PostProcessVolume>();
        _ppv.profile.TryGetSettings(out _vignette);
        _currentCamRot = _camera.transform.localRotation.eulerAngles.x;
    }

    private void OnEnable() => _currentPlayerRot = transform.localRotation.eulerAngles.y;

    void Update()
    {
        _currentPlayerRot += _mouseX * _mouseXSensitivity * Time.deltaTime;
        transform.rotation = Quaternion.Euler(0f, _currentPlayerRot, 0f);

        _currentCamRot = Mathf.Clamp(_currentCamRot + _mouseY * _mouseYSensitivity * Time.deltaTime, -_clampAngle, _clampAngle);
        _camera.transform.localRotation = Quaternion.identity;
        _camera.transform.Rotate(Vector3.left, _currentCamRot);

        if (_startVignetteLerp) VignetteLerp();
    }

    private void FixedUpdate()
    {
        Ray ray = _camera.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        RaycastHit hit;

        var InRangeToInteract = Physics.Raycast(ray, out hit, _interactDistance)
            && hit.transform.GetComponent<IInteractable>() != null && !_playerInteract.interactIsDisabled;

        if (InRangeToInteract)
        {
            if (_canInteract) return;

            _handIcon.SetActive(true);
            _playerInteract.AllowInteract(hit.transform.gameObject);
            _canInteract = true;
        }
        else
        {
            if (!_canInteract || holdingItem) return;

            _handIcon.SetActive(false);
            _playerInteract.ResetInteract();
            _canInteract = false;
        }
    }

    public void SetHeldObj(GameObject obj, bool cond)
    {
        holdingItem = cond;
        _heldObjectHandler.enabled = cond;
        _playerAttack.sword.HideSword(cond);
        if (cond) _heldObjectHandler.HandleHeldObject(obj);
    }

    public void ControlCamera(float mouseX, float mouseY)
    {
        _mouseX = mouseX;
        _mouseY = mouseY;
    }

    public void EnableVignette(bool isCrouching, bool isInShadows)
    {
        if (_vignetteOverride) return;
        _startVignetteLerp = false;
        _lerpValue = 0f;
        if ((isCrouching && !isInShadows) || (!isCrouching && isInShadows))
        {
            _vignetteValue = _baseVignetteIntensity;
        }
        else if (isCrouching && isInShadows)
        {
            _vignetteValue = _baseVignetteIntensity * _vignetteIntensityMultiplier;
        }
        else { _vignetteValue = 0f; }
        _startVignetteLerp = true;
    }

    public void SetVignetteOverride(bool cond)
    {
        if (!cond) _vignetteOverride = false;
        EnableVignette(cond, cond);
        if (cond) _vignetteOverride = true;
    }

    private void VignetteLerp()
    {
        if (_lerpValue < 1f)
        {
            _lerpValue += _vignetteLerpSpeed * Time.deltaTime;
            _vignette.intensity.value = Mathf.Lerp(_vignette.intensity.value, _vignetteValue, _lerpValue);
        }
        else { _startVignetteLerp = false; _lerpValue = 0f; }
    }
}
