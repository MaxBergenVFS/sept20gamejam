using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region inits
    public static PlayerController instance { get { return _instance; } }
    private static PlayerController _instance;

    public Transform holdObjPos;

    [SerializeField]
    private float _movementSpeed = 5f;
    [SerializeField]
    private float _jumpSpeed = 1f;
    [SerializeField]
    private float _underwaterJumpMultiplier = 1.3f;
    [SerializeField]
    private float _gravity = 20f;
    [SerializeField]
    private float _underwaterGravityMultiplier = 0.5f;
    [SerializeField]
    private float _sprintMultiplier = 2f;
    [SerializeField]
    private float _crouchSpeedMultiplier = 0.75f;
    [SerializeField]
    private float _backwardsMovementSpeedMultiplier = 0.75f;
    [SerializeField]
    private float _outOfStamMultipler = 0.5f;
    [SerializeField]
    private float _crouchCheckDistance = 1f;
    [SerializeField]
    private float _objThrowDist = 20f;
    [SerializeField]
    private float _xOffset = 1f;
    [SerializeField]
    private float _zOffset = 1f;

    private CharacterController _characterController;
    private CameraController _cameraController;
    private PlayerHealth _playerHealth;
    private PlayerAttack _playerAttack;
    private PlayerFootstepSFX _playerFootstepSFX;
    private PlayerSFX _playerSFX;
    private Transform _transform;
    private HeadBob _headBob;
    private bool _isInShadows = false;
    private bool _isVanished = false;
    private float _lerpSpeed = 10f;
    private float _verticalVelocity = 0f;
    private float _defaultSpeed;
    private float _sprintSpeed;
    private float _crouchSpeed;
    private float _underwaterSpeed;
    private float _crouchSprintSpeed;
    private float _outOfStamSpeed;
    private float _charContHeight;
    private float _crouchedHeight;
    private float _currentCoolDownTime = 0f;
    private float _lerpValue = 0f;
    private float _axisH;
    private float _axisV;
    private bool _alreadySprinting = false;
    private bool _alreadyJumping = false;
    private bool _uncrouching = false;
    private bool _uncrouchLerp = false;
    private bool _isCrouching = false;
    private bool _isSprinting = false;
    private bool _isSwimming = false;
    private bool _isUnderwater = false;
    private bool _jumping = false;
    private bool _climbingLadder = false;
    private float _movementCheckerThreshold = 0.05f;
    #endregion

    private void Awake()
    {
        _characterController = GetComponent<CharacterController>();
        _cameraController = GetComponent<CameraController>();
        _playerHealth = GetComponent<PlayerHealth>();
        _playerSFX = GetComponent<PlayerSFX>();
        _playerAttack = GetComponent<PlayerAttack>();
        _playerFootstepSFX = GetComponent<PlayerFootstepSFX>();
        _headBob = GetComponentInChildren<HeadBob>();
        _instance = this;
        _transform = transform;
        _charContHeight = _characterController.height;
        _crouchedHeight = _charContHeight * 0.5f;
        _defaultSpeed = _movementSpeed;
        _sprintSpeed = _movementSpeed * _sprintMultiplier;
        _crouchSpeed = _underwaterSpeed = _movementSpeed * _crouchSpeedMultiplier;
        _crouchSprintSpeed = _movementSpeed * _crouchSpeedMultiplier * _sprintMultiplier;
        _outOfStamSpeed = _movementSpeed * _outOfStamMultipler;
    }

    private void Update()
    {
        SetFootstepSFXState();

        if (_characterController.isGrounded)
        {
            if (CheckIfHasMoved()) { _playerFootstepSFX.SetFootstepSpeed(_movementSpeed); }
            else { _playerFootstepSFX.SetFootstepSpeed(0); }

            var canUncrouchAndHasStam = (!_isCrouching || (_isCrouching && AttemptUncrouch()))
            && !_playerHealth.CheckIfOutOfHealth();

            if (_jumping && canUncrouchAndHasStam)
            {
                if (_isUnderwater) { _verticalVelocity = _jumpSpeed * _underwaterJumpMultiplier; }
                else { _verticalVelocity = _jumpSpeed; }
                _alreadyJumping = true;
                _playerFootstepSFX.PlayJumpSoundUp();
            }
            else
            {
                _verticalVelocity -= 0;
                if (_alreadyJumping) { _alreadyJumping = false; }
            }
        }
        else
        {
            if (!_climbingLadder)
            {
                if (_isUnderwater) { _verticalVelocity -= _gravity * _underwaterGravityMultiplier * Time.deltaTime; }
                else { _verticalVelocity -= _gravity * Time.deltaTime; }
                _verticalVelocity = Mathf.Clamp(_verticalVelocity, -_gravity, _gravity);
            }
            _jumping = false;
        }
        if (_uncrouching)
        {
            if (AttemptUncrouch())
            {
                _uncrouching = false;
                _uncrouchLerp = true;
            }
        }
        else if (_uncrouchLerp) { Uncrouch(); }
        MovePlayer();
    }

    #region Input Manager
    public void ControlPlayer(float axisH, float axisV)
    {
        _axisH = axisH * _movementSpeed;
        if (_axisV > 0) { _axisV = axisV * _movementSpeed; }
        else { _axisV = axisV * _movementSpeed * _backwardsMovementSpeedMultiplier; }
    }

    public void Jump()
    {
        if (_isSwimming) return;
        _jumping = true;
        _playerFootstepSFX.ResetJumpSoundTimer();
    }

    public void CrouchDown()
    {
        if (_isSwimming) return;
        _characterController.height = _crouchedHeight;
        _uncrouching = false;
        _isCrouching = true;
        _cameraController.EnableVignette(_isCrouching, _isInShadows);
        UpdateMovementSpeed(_playerHealth.CheckIfOutOfHealth());
        _playerFootstepSFX.DontPlaySFXOnEnable();
        _playerFootstepSFX.SetStandingParameter(false);
        if (_alreadySprinting || _isSprinting) { SprintUp(); }
    }

    public void CrouchUp()
    {
        if (_isSwimming) return;
        _uncrouching = true;
    }

    public void SprintHold()
    {
        if (_isCrouching) return;

        //if (!_playerAttack.CheckIfSwordIsSheathed()) { SprintUp(); }
        else if (!_isSprinting) { SprintDown(); }

        if (CheckIfHasMoved() && !_alreadySprinting) { Sprint(true); }
        else if (!CheckIfHasMoved() || _alreadySprinting) { Sprint(false); }
    }

    public void SprintDown()
    {
        if (_isCrouching || _isUnderwater) return;
        //if (!_playerAttack.CheckIfSwordIsSheathed()) _playerAttack.SheathSword();
        _isSprinting = true;
        UpdateMovementSpeed(_playerHealth.CheckIfOutOfHealth());
    }

    public void SprintUp()
    {
        _isSprinting = false;
        Sprint(false);
        UpdateMovementSpeed(_playerHealth.CheckIfOutOfHealth());
    }
    #endregion

    public void Sprint(bool sprinting)
    {
        if (_playerHealth.CheckIfOutOfHealth()) return;
        _alreadySprinting = sprinting;
        UpdateMovementSpeed(false);
    }

    private void SetFootstepSFXState()
    {
        if (_characterController.isGrounded == _playerFootstepSFX.enabled) return;
        _playerFootstepSFX.enabled = _characterController.isGrounded;
    }

    private void UpdateMovementSpeed(bool outOfStam)
    {
        if (outOfStam) { _movementSpeed = _outOfStamSpeed; }
        else if (_isUnderwater) { _movementSpeed = _underwaterSpeed; }
        else if (!_isSprinting && !_isCrouching) { _movementSpeed = _defaultSpeed; }
        else if (!_isSprinting && _isCrouching) { _movementSpeed = _crouchSpeed; }
        else if (_isSprinting && _isCrouching) { _movementSpeed = _crouchSprintSpeed; }
        else if (_isSprinting && !_isCrouching) { _movementSpeed = _sprintSpeed; }
    }

    private bool AttemptUncrouch()
    {
        if (AttemptUncrouchRaycast(_xOffset, 0f) && AttemptUncrouchRaycast(-_xOffset, 0f) &&
            AttemptUncrouchRaycast(0f, _zOffset) && AttemptUncrouchRaycast(0f, -_zOffset)) { return true; }
        else { return false; }
    }

    private bool AttemptUncrouchRaycast(float xOffset, float zOffset)
    {
        Vector3 origin = new Vector3(_transform.position.x + xOffset, _transform.position.y, _transform.position.z + zOffset);
        Ray ray = new Ray(origin, _transform.up);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, _crouchCheckDistance)) { return false; }
        else { return true; }
    }

    private void Uncrouch()
    {
        if (_lerpValue < 1f)
        {
            _lerpValue += Time.deltaTime * _lerpSpeed;
            _characterController.height = (Mathf.Lerp(_crouchedHeight, _charContHeight, _lerpValue));
        }
        else if (_lerpValue >= 1f)
        {
            _playerFootstepSFX.SetStandingParameter(true);
            _characterController.height = _charContHeight;
            _uncrouchLerp = false;
            _lerpValue = 0f;
            _isCrouching = false;
            _cameraController.EnableVignette(_isCrouching, _isInShadows);
            UpdateMovementSpeed(_playerHealth.CheckIfOutOfHealth());
        }
    }

    public bool CheckIfCrouching() { return _isCrouching; }

    private bool CheckIfHasMoved()
    {
        bool hasMoved;
        if (_axisH > _movementCheckerThreshold || _axisH < -_movementCheckerThreshold || _axisV > _movementCheckerThreshold || _axisV < -_movementCheckerThreshold)
        {
            hasMoved = true;
        }
        else { hasMoved = false; }
        _headBob.SetSpeed(hasMoved, _movementSpeed);
        return hasMoved;
    }

    public void SetIsInShadows(bool cond)
    {
        _isInShadows = cond;
        _cameraController.EnableVignette(_isCrouching, _isInShadows);
    }

    public void SetIsVanished(bool cond)
    {
        _isVanished = cond;
        _cameraController.SetVignetteOverride(cond);
    }

    public bool CheckIfInShadows()
    {
        if (_isInShadows && _isCrouching) return true;
        return false;
    }

    public bool CheckIfVanished() { return _isVanished; }

    public bool CheckIfHasSword() { return _playerAttack.hasSword; }

    public void LowHealthStatus(bool cond) { UpdateMovementSpeed(cond); }

    public void SetClimbingLadder(bool cond)
    {
        if (_climbingLadder == cond) return;
        _climbingLadder = cond;
        if (_climbingLadder) _playerFootstepSFX.DontPlaySFXOnEnable();
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.GetComponent<Ladder>()) { _climbingLadder = false; }
    }

    private void MovePlayer()
    {
        Vector3 moveDirection;

        if (!_climbingLadder) { moveDirection = new Vector3(_axisH, _verticalVelocity, _axisV); }
        else { moveDirection = new Vector3(_axisH, _axisV, 0f); }

        moveDirection = _transform.TransformDirection(moveDirection);
        _characterController.Move(moveDirection * Time.deltaTime);
    }

    public void ResetSprintAndStam()
    {
        _isSprinting = false;
        UpdateMovementSpeed(false);
    }

    public float GetMovementSpeed()
    {
        if (CheckIfHasMoved()) { return _movementSpeed; }
        else { return 0; }
    }

    public float GetObjThrowDist() { return _objThrowDist; }

    public void SetIsSwimming(bool cond)
    {
        if (_isCrouching) CrouchUp();
        _isSwimming = cond;
    }

    public void SetIsUnderwater(bool cond)
    {
        _isUnderwater = cond;
        _isSwimming = false;
        if (_isSprinting) SprintUp();
        UpdateMovementSpeed(false);
    }

    public bool CheckIfUnderwater() { return _isUnderwater; }
}

