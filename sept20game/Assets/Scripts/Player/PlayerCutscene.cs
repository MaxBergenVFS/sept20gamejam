﻿using UnityEngine;

public class PlayerCutscene : MonoBehaviour
{
    [SerializeField]
    private GameObject _bodyCam;
    [SerializeField]
    private GameObject _crossHairWhite;
    [SerializeField]
    private GameObject _crossHairGreen;
    [SerializeField]
    private GameObject _healthNum;

    private InputManager _inputManager;

    private void Awake()
    {
        _inputManager = GetComponent<InputManager>();
    }

    public void EnterCutsceneState()
    {
        _inputManager.enabled = false;
        _bodyCam.SetActive(false);
        _crossHairWhite.SetActive(false);
        _crossHairGreen.SetActive(false);
        _healthNum.SetActive(false);
    }

    public void ExitCutsceneState()
    {
        _inputManager.enabled = true;
        _bodyCam.SetActive(true);
        _crossHairWhite.SetActive(true);
        _healthNum.SetActive(true);
    }
}
