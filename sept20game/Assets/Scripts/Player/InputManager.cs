﻿using UnityEngine;

public class InputManager : MonoBehaviour
{
    public bool hasAbilities = false;
    public bool hasInventory = false;

    private PauseMenu _pauseMenu;
    private PlayerInteract _playerInteract;
    private PlayerController _playerController;
    private PlayerAttack _playerAttack;
    private PlayerAbilities _playerAbilities;
    private PlayerInventory _playerInventory;
    private CameraController _cameraController;
    private DialogueManager _dialogueManager;
    private DevToolMenu _devToolMenu;
    private bool _inventoryIsOpen = false;
    private bool _isTalkingToNPC = false;

    [HideInInspector]
    public bool isPaused = false;

    private void Awake()
    {
        _pauseMenu = GetComponent<PauseMenu>();
        _playerInteract = GetComponent<PlayerInteract>();
        _playerController = GetComponent<PlayerController>();
        _playerAttack = GetComponent<PlayerAttack>();
        _playerAbilities = GetComponent<PlayerAbilities>();
        _playerInventory = GetComponent<PlayerInventory>();
        _cameraController = GetComponent<CameraController>();
        if (GetComponentInChildren<DevToolMenu>()) _devToolMenu = GetComponentInChildren<DevToolMenu>();
    }

    private void Start() => _dialogueManager = DialogueManager.instance;

    private void OnDisable() => FreezeMovement();

    void Update()
    {

        if (Input.GetButtonDown("DevTools"))
        {
            if (_devToolMenu != null) { _devToolMenu.ToggleMenu(); }
            else { print("Missing DevToolMenu on Player prefab"); }
        }

        if (Input.GetButtonDown("Cancel"))
        {
            _pauseMenu.Pause();
            _playerInventory.InventoryClose(false);
            _inventoryIsOpen = false;
        }

        if (isPaused) return;

        if (hasInventory && !_isTalkingToNPC)
        {
            if (Input.GetButtonDown("Inventory"))
            {
                if (_inventoryIsOpen) { CloseInventory(); }
                else { OpenInventory(); }
            }
        }

        if (_inventoryIsOpen && !_isTalkingToNPC)
        {
            if (Input.GetButtonDown("Right")) _playerInventory.NavigateInventoryRight();
            if (Input.GetButtonDown("Left")) _playerInventory.NavigateInventoryLeft();
            if (Input.GetButtonDown("Interact")) _playerInventory.UseInventoryItem();
        }

        if (_isTalkingToNPC && Input.GetButtonDown("Interact")) _dialogueManager.DisplayNextSentence();

        if (_inventoryIsOpen || _isTalkingToNPC) return;

        _cameraController.ControlCamera(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));
        _playerController.ControlPlayer(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));

        if (Input.GetButtonDown("Jump")) _playerController.Jump();
        if (Input.GetButtonDown("Interact")) _playerInteract.Interact();
        if (Input.GetButtonDown("Crouch")) _playerController.CrouchDown();
        if (Input.GetButtonUp("Crouch")) _playerController.CrouchUp();
        if (Input.GetButtonDown("Sprint")) _playerController.SprintDown();
        if (Input.GetButton("Sprint")) _playerController.SprintHold();
        if (Input.GetButtonUp("Sprint")) _playerController.SprintUp();
        if (Input.GetButtonDown("Attack") && !_cameraController.holdingItem) { _playerAttack.AttackDown(); }
        if (Input.GetButtonUp("Attack") && !_cameraController.holdingItem) { _playerAttack.AttackUp(); }
        if (Input.GetButtonDown("Block") && !_cameraController.holdingItem) { _playerAttack.Block(); }
        if (Input.GetButtonDown("WeaponSwap") && !_cameraController.holdingItem) { _playerAttack.SwapWeapons(); }
        //if (Input.GetButtonUp("Sheath") && !_cameraController.holdingItem) { _playerAttack.SheathSword(); }

        if (!hasAbilities) return; //----------------abilities------------------//

        if (Input.GetAxis("Mouse ScrollWheel") > 0) _playerAbilities.CycleAbilityPos();
        if (Input.GetAxis("Mouse ScrollWheel") < 0) _playerAbilities.CycleAbilityNeg();

        if (_cameraController.holdingItem) return;

        if (Input.GetButtonDown("Fire2")) _playerAbilities.AbilityDown();
        if (Input.GetButton("Fire2")) _playerAbilities.AbilityHold();
        if (Input.GetButtonUp("Fire2")) _playerAbilities.AbilityUp();
    }

    public void StartTalkingToNPC()
    {
        _isTalkingToNPC = true;
        FreezeMovement();
        if (_inventoryIsOpen) CloseInventory();
    }

    public void StopTalkingToNPC()
    {
        _isTalkingToNPC = false;
    }

    private void FreezeMovement()
    {
        _playerController.ControlPlayer(0f, 0f);
        _cameraController.ControlCamera(0f, 0f);
    }

    private void CloseInventory()
    {
        _playerInventory.InventoryClose(true);
        _inventoryIsOpen = false;
    }

    private void OpenInventory()
    {
        _playerInventory.InventoryOpen();
        FreezeMovement();
        _inventoryIsOpen = true;
    }
}

