using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField] private LayerMask _attackableLayers;
    [SerializeField] private float _attackCooldown = 3f;
    [SerializeField] private float _swordDmgDelay = 0.3f;
    [SerializeField] private float _attackRange = 1f;
    [SerializeField] private float _timeBeforeCanBlockAgain = 1f;
    [SerializeField] private float _blockDuration = 0.3f;
    [SerializeField] private Transform _attackOrigin;
    [SerializeField] private float _startChargingThreshold = 1.5f;
    [SerializeField] private float _chargeAtkStamCostDmgMultiplier = 3f;
    [SerializeField] private int _chargeUpStamCost = 15;
    [SerializeField] private int _chargeUpAtkDmgMultiplier = 2;
    [SerializeField] private ParticleSystem _sparkParticles;
    [SerializeField] private ParticleSystem _bloodParticles;
    [SerializeField] private GameObject _playerArms, _swordModel, _crossbowModel;

    [HideInInspector] public Sword sword;
    [HideInInspector] public bool hasSword = false;

    private ProjectileWeapon _projectileWeapon;
    private Transform _armsTransform;
    private PlayerHealth _playerHealth;
    private PlayerInventory _playerInventory;
    private PlayerInteract _playerInteract;
    private int _baseSwordDmg = 1, _atkLvl = 1, _chargeUpAtkDmg;
    private float _currentCoolDownTime = 0f, _currentChargeTime = 0f,
        _chargeUpAtkDmgFloat = 1f, _amtToLowerIdleArmsBy = 1f;
    private bool _justAutoSwung, _swingInit, _canChargeAttack, _canDeflect, _blocking, _chargeAtk,
        _isChargingAttack, _attackIsCharged, _isAttacking, _crossbowEquipped, _swordEquipped,
        _canBlockAgain = true, _hasDoneDmg = true;

    private void Awake()
    {
        _playerHealth = GetComponent<PlayerHealth>();
        _playerInventory = GetComponent<PlayerInventory>();
        _playerInteract = GetComponent<PlayerInteract>();
        sword = _playerArms.GetComponent<Sword>();
        _projectileWeapon = _playerArms.GetComponent<ProjectileWeapon>();
    }

    public void InitWeaponValues(int dmg, int atkLvl, bool canCharge, bool canDeflect)
    {
        _baseSwordDmg = dmg;
        _atkLvl = atkLvl;
        _chargeUpAtkDmg = _baseSwordDmg * _chargeUpAtkDmgMultiplier;
        _canChargeAttack = canCharge;
        _canDeflect = canDeflect;
    }

    void Update()
    {
        if (_isAttacking)
        {
            _currentCoolDownTime += Time.deltaTime;
            if (_currentCoolDownTime >= _swordDmgDelay && !_hasDoneDmg)
            {
                if (_chargeAtk) { DoChargeUpSwordDmg(); }
                else { DoSwordDmg(_baseSwordDmg); }
            }
            else if (_currentCoolDownTime >= _attackCooldown)
            {
                _isAttacking = false;
                _currentCoolDownTime = 0f;
            }
        }

        if (_isChargingAttack)
        {
            _currentChargeTime += Time.deltaTime;
            if (_currentChargeTime >= _startChargingThreshold)
            {
                if (!_attackIsCharged)
                {
                    sword.ChargeSwing();
                    _attackIsCharged = true;
                }
                _playerHealth.ReduceHealthByAmt((int)(_chargeUpStamCost * Time.deltaTime));
                _chargeUpAtkDmgFloat += (_chargeAtkStamCostDmgMultiplier * Time.deltaTime);
            }

            if (_playerHealth.CheckIfOutOfHealth() && _attackIsCharged) { SwingSword(true); }
        }
    }

    private void Shoot()
    {
        if (!_projectileWeapon.isActiveAndEnabled) return;
        _projectileWeapon.SpawnProjectile();
        _playerInteract.DisableInteractTemporary();
    }

    public void SwapWeapons()
    {
        if (!GameManager.instance.playerHasCrossbow || !GameManager.instance.playerHasSword) return;

        if (_crossbowEquipped)
        {
            EquipSword();

        }
        else if (_swordEquipped)
        {
            EquipCrossbow();
        }
    }

    public void EquipCrossbow()
    {
        _crossbowEquipped = true;
        _swordEquipped = false;
        _swordModel.SetActive(false);
        _crossbowModel.SetActive(true);
    }

    public void EquipSword()
    {
        _crossbowEquipped = false;
        _swordEquipped = true;
        _swordModel.SetActive(true);
        _crossbowModel.SetActive(false);
    }

    public void AttackDown()
    {
        if (!_crossbowEquipped && _swordEquipped)
        {
            if (_canChargeAttack)
            {
                _isChargingAttack = true;
                _swingInit = true;
                _justAutoSwung = false;
            }
            else
            {
                SwingSword(false);
            }
        }
        else if (_crossbowEquipped && !_swordEquipped)
        {
            Shoot();
        }
    }

    public void AttackUp()
    {
        if (!_swordEquipped || _crossbowEquipped) return;
        //if (_swordIsSheathed) SheathSword();
        if (_justAutoSwung || !_swingInit || !_canChargeAttack) return;
        SwingSword(false);
    }

    public void Block()
    {
        if (!_canBlockAgain || !_swordEquipped) return;

        _canBlockAgain = false;
        _blocking = true;
        sword?.Block();
        Invoke(nameof(ResetCanBlockAgain), _timeBeforeCanBlockAgain);
        Invoke(nameof(ResetBlock), _blockDuration);
        _playerInteract.DisableInteractTemporary();
    }

    public void PlaySparks() => _sparkParticles.Play();
    public bool CheckIfBlocking() { return _blocking; }
    private void ResetCanBlockAgain() => _canBlockAgain = true;
    private void ResetBlock() => _blocking = false;

    private void SwingSword(bool autoSwingTimeOut)
    {
        if ((!_playerHealth.CheckIfOutOfHealth() || autoSwingTimeOut) && hasSword)
        {
            _chargeAtk = _attackIsCharged;

            if (!_isAttacking && hasSword)
            {
                sword.SwingSword(_attackIsCharged);
                //_playerHealth.ReduceHealthByAmt(_swordStamCost);
                _playerInteract.DisableInteractTemporary();
                _isAttacking = true;
                _hasDoneDmg = false;
            }
        }
        if (_canChargeAttack)
        {
            _isChargingAttack = false;
            _swingInit = false;
            _justAutoSwung = autoSwingTimeOut;
            _currentChargeTime = 0f;
            _currentCoolDownTime = 0f;
        }
    }

    private void DoSwordDmg(int dmg)
    {
        Collider[] hitObj = Physics.OverlapSphere(_attackOrigin.position, _attackRange, _attackableLayers);
        bool hasHitDamageable = false;
        bool hasHitFlesh = false;
        bool hasPassedAtkLvlCheck = false;
        foreach (Collider obj in hitObj)
        {
            if (obj.GetComponent<IDamageable>() != null)
            {
                hasHitDamageable = true;
                IDamageable dmgable = obj.GetComponent<IDamageable>();
                if (dmgable.CompareDefenseLevel(_atkLvl))
                {
                    dmgable.TakeDamage(dmg, true);
                    if (dmgable.CheckIfFlesh()) hasHitFlesh = true;
                    hasPassedAtkLvlCheck = true;
                }

                if (sword.CheckIfIsBreakable())
                {
                    hasSword = false;
                    GameManager.instance.playerHasSword = false;
                    sword.HideSword(true);
                }
            }
            if (_canDeflect)
            {
                if (obj.GetComponent<IDeflectable>() != null) { obj.GetComponent<IDeflectable>().Deflect(); }
            }
        }

        if (hasHitDamageable)
        {
            if (hasHitFlesh && hasPassedAtkLvlCheck)
            {
                sword.PlayWeaponHitSound(true, hasHitFlesh);
                _bloodParticles.Play();
            }
            else if (!hasHitFlesh && hasPassedAtkLvlCheck)
            {
                sword.PlayWeaponHitSound(true, hasHitFlesh);
            }
            else
            {
                _sparkParticles.Play();
                sword.PlayWeaponHitSound(false, false);
            }
        }
        _hasDoneDmg = true;
        _chargeAtk = false;
    }

    private void DoChargeUpSwordDmg()
    {
        if (!_playerHealth.CheckIfOutOfHealth()) DoSwordDmg(_chargeUpAtkDmg + (int)_chargeUpAtkDmgFloat);
        _attackIsCharged = false;
        _chargeUpAtkDmgFloat = 1f;
    }
}
