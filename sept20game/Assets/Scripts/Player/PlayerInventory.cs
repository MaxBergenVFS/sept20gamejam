﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using System.Collections;

public class PlayerInventory : MonoBehaviour
{
    #region init values
    [SerializeField]
    private int _reagentItemLimit = 5;
    [SerializeField]
    private GameObject _soulsAmtNum;
    [SerializeField]
    private GameObject _soulsIcon;
    [SerializeField]
    private GameObject _gibsAmtNum;
    [SerializeField]
    private GameObject _gibsIcon;
    [SerializeField]
    private GameObject _healthPotionAmtNum;
    [SerializeField]
    private GameObject _healthPotionIcon;
    [SerializeField]
    private GameObject _amuletIcon;
    [SerializeField]
    private TextMeshProUGUI _inventoryItemName;
    [SerializeField]
    private TextMeshProUGUI _inventoryItemDescription;
    [SerializeField]
    private TextMeshProUGUI _goldAmtNumUI;
    [SerializeField]
    private TextMeshProUGUI _distractAmtNumUI;
    [SerializeField]
    private TextMeshProUGUI _vitalityRefillAmtNumUI;
    [SerializeField]
    private TextMeshProUGUI _ammoAmtNumUI;
    [SerializeField]
    private Transform _inventoryItemModelPos;
    [SerializeField]
    private GameObject _useInventoryItemPrompt;
    [SerializeField]
    private GameObject _useInventoryItemPromptPressed;
    [SerializeField]
    private GameObject _inventory;
    [SerializeField]
    private GameObject _rightArrowUI;
    [SerializeField]
    private GameObject _leftArrowUI;
    [SerializeField]
    private DistractionAbility _distractionAbility;

    public InventoryItemDatabase inventoryDatabase;

    private string _openInvSFXpath = "event:/SFX/Interactables/BackpackPickup";
    private string _closeInvSFXpath = "event:/SFX/Interactables/BackpackPickup";
    private string _navigateInvSFXpath = "event:/SFX/Player/Interact/Positive";
    private bool[] _keys = new bool[64];
    private List<int> _heldKeys = new List<int>();
    private string _keyTextUIDefault = "";
    private string _keyTextUI = null;
    private TextMeshProUGUI _inventoryUI;
    private TextMeshProUGUI _soulsAmtNumUI;
    private GameObject _currentlyDraggedUIElement = null;
    private GameObject _currentInventoryItemModel;
    private PlayerController _playerController;
    private Vector3 _UIElementStartingPos;
    private bool _snapUIElementToStartingPos = false;
    private int _currentSceneIndex;
    private List<int> _validDatabaseIds;
    private int _currentDatabaseId, _maxDatabaseId;
    private bool _canUseInventoryItem = false;
    private string _keyDatabaseString = "Keyring";
    private string _goldDatabaseString = "Gold Pieces";
    private string _soulsDatabaseString = "Tortured Soul";
    private string _healthPotionDatabaseString = "Health Tonic";
    private string _distractionAbilityDatabaseString = "Giblet";
    private string _ammoDatabaseString = "Bolt";
    private int _keyDatabaseId, _goldDatabaseId, _soulsDatabaseId, _healthPotionDatabaseId,
        _distractionAbilityReagentId, _ammoDatabaseId;
    private float _timeBeforeHidingUIElement = 0.2f;

    #endregion

    private void Awake()
    {
        _inventoryUI = _inventory.GetComponent<TextMeshProUGUI>();
        _playerController = GetComponent<PlayerController>();
        _soulsAmtNumUI = _soulsAmtNum.GetComponent<TextMeshProUGUI>();
        _inventoryUI.text = _keyTextUIDefault;
        _inventory.SetActive(false);
        _currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
    }

    private void Start()
    {
        InitDatabaseIDs();
        UpdateInventoryAmtNums();
    }

    #region Database Utility
    private void InitDatabaseIDs()
    {
        _validDatabaseIds = inventoryDatabase.GetListOfValidIds();
        _maxDatabaseId = _validDatabaseIds.Count;
        _keyDatabaseId = inventoryDatabase.GetInventoryItemID(_keyDatabaseString);
        _goldDatabaseId = inventoryDatabase.GetInventoryItemID(_goldDatabaseString);
        _soulsDatabaseId = inventoryDatabase.GetInventoryItemID(_soulsDatabaseString);
        _healthPotionDatabaseId = inventoryDatabase.GetInventoryItemID(_healthPotionDatabaseString);
        _ammoDatabaseId = inventoryDatabase.GetInventoryItemID(_ammoDatabaseString);
        InitAbilityReagentDatabaseRefs();
        int initID = 0;
        DisplayInventoryItem(initID);
        _currentInventoryItemModel.SetActive(false);
    }

    private void InitAbilityReagentDatabaseRefs()
    {
        _distractionAbilityReagentId = inventoryDatabase.GetInventoryItemID(_distractionAbilityDatabaseString);
        _distractionAbility.SetDatabaseID(_distractionAbilityReagentId);
    }

    private void UpdateInventoryAmtNums()
    {
        _distractAmtNumUI.text = inventoryDatabase.GetInventoryItemAmt(_distractionAbilityReagentId).ToString();
        _soulsAmtNumUI.text = inventoryDatabase.GetInventoryItemAmt(_soulsDatabaseId).ToString();
        _goldAmtNumUI.text = inventoryDatabase.GetInventoryItemAmt(_goldDatabaseId).ToString();
        _vitalityRefillAmtNumUI.text = inventoryDatabase.GetInventoryItemAmt(_healthPotionDatabaseId).ToString();
        _ammoAmtNumUI.text = inventoryDatabase.GetInventoryItemAmt(_ammoDatabaseId).ToString();
    }

    public void UpdateAmmoAmtNum() => _ammoAmtNumUI.text = inventoryDatabase.GetInventoryItemAmt(_ammoDatabaseId).ToString();

    private void SetDatabaseIDs()
    {
        _validDatabaseIds = inventoryDatabase.GetListOfValidIds();
        _maxDatabaseId = _validDatabaseIds.Count;
        if (_currentDatabaseId > _maxDatabaseId - 1) _currentDatabaseId = 0;
    }

    public bool CheckIfInventoryFull(int id)
    {
        if (inventoryDatabase.GetInventoryItemAmt(id) >= _reagentItemLimit) return true;
        return false;
    }
    #endregion

    #region Keys
    public void InitKeys()
    {
        foreach (var key in StateManager.instance.key_hasBeenPickedUp[_currentSceneIndex])
        {
            //print($"key: {key.Key} has been picked up: {key.Value}");
            if (key.Value && !StateManager.instance.key_hasBeenUsed[_currentSceneIndex][key.Key])
                SetKey(true, key.Key);
        }
    }

    public void RemoveKey(int keyNum)
    {
        UpdateRemoveKeyStatus(keyNum);
        inventoryDatabase.AddToInventoryItemAmt(_keyDatabaseId, -1);
    }

    public void RemoveKey(int keyNum, int databaseID)
    {
        UpdateRemoveKeyStatus(keyNum);
        inventoryDatabase.SetInventoryItemAmt(databaseID, 0);
    }

    public void SetKey(bool cond, int keyNum)
    {
        UpdateSetKeyStatus(cond, keyNum);
        inventoryDatabase.SetInventoryItemAmt(_keyDatabaseId, _heldKeys.Count);
    }

    public void SetKey(bool cond, int keyNum, int databaseID)
    {
        UpdateSetKeyStatus(cond, keyNum);
        inventoryDatabase.SetInventoryItemAmt(databaseID, 1);
    }

    private void UpdateRemoveKeyStatus(int keyNum)
    {
        if (_heldKeys.Contains(keyNum)) _heldKeys.Remove(keyNum);
        StateManager.instance.key_hasBeenUsed[_currentSceneIndex][keyNum] = true;
    }

    private void UpdateSetKeyStatus(bool cond, int keyNum)
    {
        _keys[keyNum] = cond;
        if (!_heldKeys.Contains(keyNum)) _heldKeys.Add(keyNum);
    }

    public List<int> GetHeldKeys() { return _heldKeys; }

    #endregion

    #region Reagents
    public void ReduceReagentItemAmt(int id)
    {
        if (inventoryDatabase.GetInventoryItemAmt(id) < 1) return;
        inventoryDatabase.AddToInventoryItemAmt(id, -1);
        if (id == _ammoDatabaseId) UpdateAmmoAmtNum();
    }

    public void IncreaseReagentItemAmt(int id, int amt)
    {
        if (inventoryDatabase.GetInventoryItemAmt(id) + amt > _reagentItemLimit)
        {
            inventoryDatabase.SetInventoryItemAmt(id, _reagentItemLimit);
        }
        else { inventoryDatabase.AddToInventoryItemAmt(id, amt); }

        if (id == _ammoDatabaseId) UpdateAmmoAmtNum();
    }

    public bool CheckIfHasReagent(int id)
    {
        if (inventoryDatabase.GetInventoryItemAmt(id) < 1) return false;
        return true;
    }

    public int GetCurrentReagentAmt(int id) { return inventoryDatabase.GetInventoryItemAmt(id); }

    public void IncreaseMaxReagentAmt(int amt)
    {
        _reagentItemLimit += amt;
        GameManager.instance.tempMaxReagentAmt = _reagentItemLimit;
    }
    #endregion

    #region Navigate Inventory
    public void InventoryOpen()
    {
        _inventory.SetActive(true);
        SetDatabaseIDs();
        DisplayInventoryItem(_validDatabaseIds[_currentDatabaseId]);
        UpdateInventoryAmtNums();
        FMODUnity.RuntimeManager.PlayOneShot(_openInvSFXpath);
    }

    public void InventoryClose(bool playSFX)
    {
        _inventory.SetActive(false);
        _currentInventoryItemModel.SetActive(false);
        if (playSFX) FMODUnity.RuntimeManager.PlayOneShot(_closeInvSFXpath);
    }

    public void NavigateInventoryRight()
    {
        IncrementItemDisplay();
        StopAllCoroutines();
        if (_leftArrowUI.activeSelf) _leftArrowUI.SetActive(false);
        if (_useInventoryItemPromptPressed.activeSelf) _useInventoryItemPromptPressed.SetActive(false);
        StartCoroutine(DisplayPressedUIElement(_rightArrowUI));
        PlayNavigateInvSFX();
    }

    public void NavigateInventoryLeft()
    {
        DecrementItemDisplay();
        StopAllCoroutines();
        if (_rightArrowUI.activeSelf) _rightArrowUI.SetActive(false);
        if (_useInventoryItemPromptPressed.activeSelf) _useInventoryItemPromptPressed.SetActive(false);
        StartCoroutine(DisplayPressedUIElement(_leftArrowUI));
        PlayNavigateInvSFX();
    }

    public void UseInventoryItem()
    {
        if (!_canUseInventoryItem) return;
        StartCoroutine(DisplayPressedUIElement(_useInventoryItemPromptPressed));
        inventoryDatabase.UseItemInInventory(_validDatabaseIds[_currentDatabaseId]);
    }

    private void PlayNavigateInvSFX() { FMODUnity.RuntimeManager.PlayOneShot(_navigateInvSFXpath); }

    IEnumerator DisplayPressedUIElement(GameObject arrowUI)
    {
        arrowUI.SetActive(true);
        yield return new WaitForSeconds(_timeBeforeHidingUIElement);
        arrowUI.SetActive(false);
    }
    #endregion

    #region Display Items
    public void RefreshItemAmtDisplay()
    {
        int amt;
        amt = inventoryDatabase.GetInventoryItemAmt(_validDatabaseIds[_currentDatabaseId]);
        if (amt < 1)
        {
            SetDatabaseIDs();
            IncrementItemDisplay();
        }
        UpdateInventoryAmtNums();
    }

    private void IncrementItemDisplay()
    {
        _currentDatabaseId++;
        if (_currentDatabaseId >= _maxDatabaseId) _currentDatabaseId = 0;
        DisplayInventoryItem(_validDatabaseIds[_currentDatabaseId]);
    }

    private void DecrementItemDisplay()
    {
        _currentDatabaseId--;
        if (_currentDatabaseId < 0) _currentDatabaseId = _maxDatabaseId - 1;
        DisplayInventoryItem(_validDatabaseIds[_currentDatabaseId]);
    }

    private void DisplayInventoryItem(int id)
    {
        _inventoryItemName.text = inventoryDatabase.GetInventoryItemName(id);
        _inventoryItemDescription.text = inventoryDatabase.GetInventoryItemDescription(id);
        string amt;
        if (inventoryDatabase.CheckIfInventoryItemIsUnique(id)) { amt = ""; }
        else { amt = inventoryDatabase.GetInventoryItemAmt(id).ToString(); }

        if (inventoryDatabase.GetInventoryItemModel(id))
        {
            Destroy(_currentInventoryItemModel);
            _currentInventoryItemModel = Instantiate(inventoryDatabase.GetInventoryItemModel(id));
            _currentInventoryItemModel.transform.position = _inventoryItemModelPos.position;
            _currentInventoryItemModel.SetActive(true);
        }
        else { _currentInventoryItemModel.SetActive(false); }

        _canUseInventoryItem = inventoryDatabase.CheckIfCanUseInventoryItem(id);
        _useInventoryItemPrompt.SetActive(_canUseInventoryItem);
    }

    public void DisplayAmuletUI() { if (!_amuletIcon.activeSelf) _amuletIcon.SetActive(true); }

    public void DisplayReagentUI(int ID)
    {
        if (ID == _distractionAbilityReagentId) { DisplayGibsUI(); }
        else if (ID == _healthPotionDatabaseId) { DisplayHealthPotionUI(); }
    }

    public bool CheckIfReagentUIDisplayed(int ID)
    {
        if (ID == _distractionAbilityReagentId)
        {
            return _gibsIcon.activeSelf;
        }
        else if (ID == _healthPotionDatabaseId)
        {
            return _healthPotionIcon.activeSelf;
        }
        return false;
    }

    public void DisplayHealthPotionUI() { DisplayUIElement(_healthPotionAmtNum, _healthPotionIcon); }
    public void DisplaySoulUI() { DisplayUIElement(_soulsAmtNum, _soulsIcon); }
    private void DisplayGibsUI() { DisplayUIElement(_gibsAmtNum, _gibsIcon); }

    private void DisplayUIElement(GameObject amtNumObj, GameObject iconObj)
    {
        if (!amtNumObj.activeSelf) amtNumObj.SetActive(true);
        if (!iconObj.activeSelf) iconObj.SetActive(true);
    }
    #endregion

    #region Get Database IDs
    public int GetGoldDatabaseID() { return _goldDatabaseId; }

    public int GetSoulDatabaseID() { return _soulsDatabaseId; }
    #endregion
}
