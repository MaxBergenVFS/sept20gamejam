﻿using UnityEngine;

public class ThrownObjArc : MonoBehaviour
{
    [SerializeField]
    private string _SFXPath = "Player/Abilities/Distract/Splat";

    private float _maxThrowSpeed = 2f;
    private float _throwSpeed;
    private float _maxVerticalOffsetYAxis = 1.5f;
    private float _verticalOffsetYAxis;
    private Vector3 _verticalOffset;
    private Vector3 _startingPos;
    private Vector3 _controlPoint;
    private Vector3 _endingPos;
    private Transform _transform;
    private Rigidbody _rb;
    private bool _beingThrown = false;
    private float _lerpValue = 1f;
    private float _maxThrowDist;

    private void Awake()
    {
        _transform = this.transform;
        _rb = GetComponent<Rigidbody>();
    }

    private void OnEnable()
    {
        _startingPos = _transform.position;
        _lerpValue = 1f;
        _maxThrowDist = PlayerController.instance.GetObjThrowDist();
    }

    private void OnDisable()
    {
        _beingThrown = false;
        _lerpValue = 0f;
        _rb.useGravity = true;
        _rb.isKinematic = false;
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/" + _SFXPath, _transform.position);
    }

    private void Update()
    {
        if (!_beingThrown) return;

        _lerpValue -= Time.deltaTime / _throwSpeed;
        if (_lerpValue > 0f) { _transform.position = CalculateBezierPoint(_lerpValue, _endingPos, _controlPoint, _startingPos); }
        else { this.enabled = false; }
    }

    public void SetEndingPos(Vector3 endingPos)
    {
        _endingPos = endingPos;
        float distPercentage = CalculateDistancePercentage(_startingPos, endingPos);
        _verticalOffsetYAxis = _maxVerticalOffsetYAxis * distPercentage;
        _throwSpeed = _maxThrowSpeed * distPercentage;
        _verticalOffset = new Vector3(0f, _verticalOffsetYAxis, 0f);
        _controlPoint = ((_startingPos + endingPos) / 2) + _verticalOffset;
        _beingThrown = true;
    }

    private Vector3 CalculateBezierPoint(float t, Vector3 endPos, Vector3 controlPoint, Vector3 startPos)
    {
        float u = 1 - t;
        float tt = t * t;
        float uu = u * u;
        float uuu = uu * u;
        float ttt = tt * t;

        Vector3 p = uuu * endPos;
        p += 3 * uu * t * controlPoint;
        p += 3 * u * tt * controlPoint;
        p += ttt * startPos;

        return p;
    }

    private float CalculateDistancePercentage(Vector3 startingPos, Vector3 endingPos)
    {
        float dist = Vector3.Distance(startingPos, endingPos);
        float distPercentage = dist / _maxThrowDist;
        return distPercentage;
    }
}
