﻿using UnityEngine;

public class EnemyHealth : Health, IDamageable
{
    [SerializeField]
    private GameObject _bloodParticleSpawner;
    [SerializeField]
    private GameObject _itemDrop;
    [SerializeField]
    private GameObject[] _prefabsToDrop;
    [SerializeField]
    private Vector3 _itemDropOffset = new Vector3();
    [SerializeField]
    private float _dropChance = 1f;

    private BloodParticles _bloodParticles;
    private EnemyController _enemyController;
    private SimplifiedEnemyController _simplifiedEnemyController;
    private bool _isDead = false;
    private bool _willExplode = false;

    private void Awake()
    {
        _enemyController = GetComponent<EnemyController>();
        _simplifiedEnemyController = GetComponent<SimplifiedEnemyController>();
        if (_bloodParticleSpawner) _bloodParticles = _bloodParticleSpawner.GetComponent<BloodParticles>();
    }

    public override void TakeDamage(int amt, bool player)
    {
        if (_isDead) return;
        base.TakeDamage(amt, player);
        _enemyController?.Flinch(player);
        if (_bloodParticleSpawner)
        {
            _bloodParticleSpawner.SetActive(true);
            _bloodParticles.SpawnBlood(5f);
        }
        if (UtilityHelper.CheckIfDepleted(_healthAmount))
            SpawnObjOnDeath(_itemDrop, _prefabsToDrop, this.transform.position + _itemDropOffset, _dropChance);
    }

    protected override void SpawnObjOnDeath(GameObject itemDrop, GameObject[] prefabsToDrop, Vector3 spawnPos, float dropChance)
    {
        base.SpawnObjOnDeath(itemDrop, prefabsToDrop, spawnPos, dropChance);
        _enemyController?.Die();
        _simplifiedEnemyController?.Die();
        _isDead = true;
    }

    public bool CheckIfFlesh() { return true; }

}
