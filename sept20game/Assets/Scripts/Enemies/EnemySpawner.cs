﻿using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private GameObject _enemyToSpawn, _particlesToSpawn;
    [SerializeField] private float _sphereRange;
    [SerializeField] private LayerMask _layersToCheck;

    public void SpawnEnemy(Quaternion rot)
    {
        Instantiate(_particlesToSpawn, transform.position, Quaternion.identity);
        Instantiate(_enemyToSpawn, transform.position, rot);
    }

    public bool CheckIfSpawnPointObstructed()
    {
        //print($"CheckSphere: {Physics.CheckSphere(transform.position, _sphereRange, _layersToCheck)}");
        return Physics.CheckSphere(transform.position, _sphereRange, _layersToCheck);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, _sphereRange);
    }
}
