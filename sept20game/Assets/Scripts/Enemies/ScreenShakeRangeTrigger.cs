﻿using UnityEngine;

public class ScreenShakeRangeTrigger : TriggerCheck
{
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            ResetTrigger();
        }
    }
}
