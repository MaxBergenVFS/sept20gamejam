﻿using UnityEngine;

public class EnemySpawnerManager : MonoBehaviour
{
    [SerializeField] EnemySpawner[] _spawnPoints;
    [SerializeField] float _timeBetweenSpawns = 10f;
    [SerializeField] int _maxAmtOfEnemiesActive = 5;
    [SerializeField] float _sphereRange = 10f;
    [SerializeField] private LayerMask _layersToCheck;

    private float _currentTimeBetweenSpawns;
    private int _spawnPointNum;
    private Transform _player;

    private void Start() => _player = PlayerController.instance.transform;

    private void OnEnable() => _currentTimeBetweenSpawns = _timeBetweenSpawns;
    // {
    //     _spawnPoints[Random.Range(0, _spawnPoints.Length)].SpawnEnemy(Quaternion.LookRotation(_player.position));  
    // }

    private void Update()
    {
        _currentTimeBetweenSpawns -= Time.deltaTime;
        if (_currentTimeBetweenSpawns < 0)
        {
            //TO DO make spawned enemies actually face the player
            if (CheckIfCanSpawnMoreEnemies()) _spawnPoints[_spawnPointNum].SpawnEnemy(Quaternion.LookRotation(_player.position));
            _currentTimeBetweenSpawns = _timeBetweenSpawns;
        }
    }

    private void RandomizeSpawnNum() => _spawnPointNum = Random.Range(0, _spawnPoints.Length);

    private bool CheckIfCanSpawnMoreEnemies()
    {
        if (!BelowMaxActiveEnemyLimit()) return false;
        RandomizeSpawnNum();
        return !_spawnPoints[_spawnPointNum].CheckIfSpawnPointObstructed();
    }

    private bool BelowMaxActiveEnemyLimit()
    {
        Collider[] enemiesWithinSphere = Physics.OverlapSphere(transform.position, _sphereRange, _layersToCheck);
        if (enemiesWithinSphere.Length < _maxAmtOfEnemiesActive) return true;
        return false;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, _sphereRange);
    }
}
