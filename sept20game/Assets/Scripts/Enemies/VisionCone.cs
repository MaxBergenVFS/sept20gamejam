﻿using UnityEngine;

public class VisionCone : MonoBehaviour
{
    private EnemyController _enemyController;
    private Pathfinding _pathfinding;
    private Transform _playerTransform;
    private bool _playerIsNear = false;
    private bool _isAttacking = false;
    private bool _isChasing = false;
    private float _currentDelayBeforeSeeingPlayer;
    private float _currentDelayBeforeLosingPlayer;
    private bool _playerRecentlyExitedVision = false;
    private PlayerController _playerController;

    [SerializeField]
    private bool _useRayCast = true;
    [SerializeField]
    private bool _ignoreIfPlayerCrouching = false;
    [SerializeField]
    private bool _ignoreIfPlayerInShadows = false;
    [SerializeField]
    private float _delayBeforeRaycast = 1f;

    public Transform visionOrigin;

    private void Awake()
    {
        _enemyController = GetComponentInParent<EnemyController>();
        _pathfinding = GetComponentInParent<Pathfinding>();
        _currentDelayBeforeSeeingPlayer = _delayBeforeRaycast;
    }

    private void Start()
    {
        _playerController = PlayerController.instance;
        _playerTransform = _playerController.transform;
    }

    void Update()
    {
        if (!_playerIsNear) return;

        if (_useRayCast)
        {
            _currentDelayBeforeSeeingPlayer -= Time.deltaTime;
            if (_currentDelayBeforeSeeingPlayer < 0f)
            {
                RaycastHit ray;
                Vector3 targetPos = _playerTransform.position;
                if (Physics.Linecast(visionOrigin.position, targetPos, out ray))
                {
                    if (ray.transform.gameObject.CompareTag("Player") && _pathfinding.CheckIfCanReachDest(targetPos))
                    {
                        _enemyController.Chase();
                        _enemyController.SetAttackDistance(ray.distance);
                    }
                }
            }
        }
        else
        {
            if (!_isChasing) { _enemyController.Chase(); _isChasing = true; }
        }
        if (_playerRecentlyExitedVision || _playerController.CheckIfVanished())
        {
            _currentDelayBeforeLosingPlayer -= Time.deltaTime;
            if (_currentDelayBeforeLosingPlayer < 0f)
            {
                LoseSightOfPlayer();
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            var checkIfPlayerCrouching = !_ignoreIfPlayerCrouching || (_ignoreIfPlayerCrouching
            && !PlayerController.instance.CheckIfCrouching());
            var checkIfPlayerInShadows = !_ignoreIfPlayerInShadows || (_ignoreIfPlayerInShadows
            && !PlayerController.instance.CheckIfInShadows());
            var canDetectPlayer = (checkIfPlayerCrouching && checkIfPlayerInShadows);

            if (canDetectPlayer)
            {
                _playerIsNear = true;
                _playerRecentlyExitedVision = false;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _playerRecentlyExitedVision = true;
            _currentDelayBeforeLosingPlayer = _delayBeforeRaycast;
        }
    }

    private void LoseSightOfPlayer()
    {
        _playerIsNear = false;
        _isChasing = false;
        _enemyController.SetAttackDistance(100f);
        _enemyController.Patrol();
        _currentDelayBeforeSeeingPlayer = _delayBeforeRaycast;
    }
}
