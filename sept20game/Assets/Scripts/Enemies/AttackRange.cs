using UnityEngine;

public class AttackRange : MonoBehaviour
{
    [SerializeField]
    private int _baseDamage = 20;
    [SerializeField]
    private LayerMask _attackableLayers;
    [SerializeField]
    private Transform _attackOrigin;
    [SerializeField]
    private float _attackRange = 1f;
    [SerializeField]
    private int _atkLvl = 2;

    private IDamageable _selfIDamageable;

    private void Awake()
    {
        _selfIDamageable = GetComponentInParent<IDamageable>();
    }

    public void Attack()
    {
        //print("Attack");
        Collider[] hitObj = Physics.OverlapSphere(_attackOrigin.position, _attackRange, _attackableLayers);
        foreach (Collider obj in hitObj)
        {
            if (obj.GetComponent<IDamageable>() != null)
            {
                IDamageable damageable = obj.GetComponent<IDamageable>();
                //print($"hit: {obj.name}");
                if (damageable != _selfIDamageable && damageable.CompareDefenseLevel(_atkLvl))
                    damageable.TakeDamage(_baseDamage, false);
            }
        }
    }
}
