﻿using UnityEngine;

public class IdleAtDistractDest : StateMachineBehaviour
{
    [SerializeField]
    private float _idleAtDistractDest = 2f;

    private Pathfinding _pathfinding;
    private float _currentIdleTime;
    private bool _exitingState = false;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _pathfinding = animator.GetComponentInParent<Pathfinding>();
        _pathfinding.Stop(true);
        _currentIdleTime = _idleAtDistractDest;
        animator.SetBool("IsIdleAtDistractDest", true);
        _exitingState = false;
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (_exitingState) return;

        _currentIdleTime -= Time.deltaTime;
        if (_currentIdleTime < 0)
        {
            animator.SetBool("IsIdleAtDistractDest", false);
            animator.SetBool("ReachedDistractDest", false);
            animator.SetBool("TurnAround", _pathfinding.CheckIfShouldTurnAround());
            _exitingState = true;
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("IsDistracted", false);
        _pathfinding.SetNextPoint();
    }
}
