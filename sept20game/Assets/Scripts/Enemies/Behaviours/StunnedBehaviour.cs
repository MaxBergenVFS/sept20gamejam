﻿using UnityEngine;

public class StunnedBehaviour : StateMachineBehaviour
{
    [SerializeField]
    private float _stunTime = 1f;

    private float _currentStunTime;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _currentStunTime = _stunTime;
        animator.SetFloat("StunTime", _currentStunTime);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _currentStunTime -= Time.deltaTime;
        animator.SetFloat("StunTime", _currentStunTime);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("IsStunned", false);
    }
}
