﻿using UnityEngine;

public class BlindedBehaviour : StateMachineBehaviour
{
    [SerializeField]
    private float _blindTime = 5f;

    private Pathfinding _pathfinding;
    private float _currentBlindTime;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _pathfinding = animator.GetComponentInParent<Pathfinding>();
        _pathfinding.Stop(true);
        //animator.GetComponentInChildren<EnemyVoiceSFX>().PlayEnemyVoiceAttacking();
        _currentBlindTime = _blindTime;
        animator.SetFloat("BlindTime", _currentBlindTime);
        Debug.Log("I AM BLIND");
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _currentBlindTime -= Time.deltaTime;
        animator.SetFloat("BlindTime", _currentBlindTime);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("IsBlinded", false);
        Debug.Log("I AM NO LONGER BLIND");
    }
}
