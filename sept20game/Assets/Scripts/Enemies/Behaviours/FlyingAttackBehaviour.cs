﻿using UnityEngine;

public class FlyingAttackBehaviour : StateMachineBehaviour
{
    [SerializeField]
    private bool _attackIsTiedToAnimation = true;
    [SerializeField]
    private float _timeBetweenAttacks = 5f;

    private Pathfinding _pathfinding;
    private ProjectileWeapon _shootProjectile;
    private float _currentTimeBetweenAttacks;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!_attackIsTiedToAnimation)
        {
            _currentTimeBetweenAttacks = 0f;
        }
        _pathfinding = animator.GetComponentInParent<Pathfinding>();
        _shootProjectile = animator.GetComponent<ProjectileWeapon>();
        _pathfinding.Stop(true);
        _pathfinding.SetAttackPlayerRotation(true);
        animator.GetComponentInChildren<EnemyVoiceSFX>().PlayEnemyVoiceAttacking();
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (_attackIsTiedToAnimation) return;

        _currentTimeBetweenAttacks -= Time.deltaTime;
        if (_currentTimeBetweenAttacks < 0f)
        {
            _shootProjectile.SpawnProjectile();
            _currentTimeBetweenAttacks = _timeBetweenAttacks;
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _pathfinding.SetAttackPlayerRotation(false);
    }
}
