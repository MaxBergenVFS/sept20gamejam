﻿using UnityEngine;

public class DeathBehaviour : StateMachineBehaviour
{
    [SerializeField]
    private GameObject _deathChunks;
    [SerializeField]
    private int _numOfChunks = 2;

    private Pathfinding _pathfinding;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _pathfinding = animator.GetComponentInParent<Pathfinding>();
        _pathfinding.Stop(true);
        animator.GetComponentInChildren<EnemyVoiceSFX>().StopEnemyVoice();
        animator.SetBool("IsDead", true);
        Debug.Log("I'm dead");
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }
}
