﻿using UnityEngine;

public class EnragedBehaviour : StateMachineBehaviour
{
    [SerializeField]
    private float _enrageTime = 1f;
    [SerializeField]
    private bool _attackIsTiedToAnimation = true;
    [SerializeField]
    private float _timeBetweenAttacks = 2f;

    private Pathfinding _pathfinding;
    private AttackRange _attackRange;
    private float _currentTimeBetweenAttacks;
    private float _currentEnrageTime;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (!_attackIsTiedToAnimation)
        {
            _attackRange = animator.GetComponent<AttackRange>();
            _currentTimeBetweenAttacks = 0f;
        }

        _pathfinding = animator.GetComponentInParent<Pathfinding>();
        _pathfinding.Stop(true);
        animator.GetComponentInChildren<EnemyVoiceSFX>().PlayEnemyVoiceAttacking();
        _currentEnrageTime = _enrageTime;
        animator.SetFloat("EnrageTime", _currentEnrageTime);
        
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _currentEnrageTime -= Time.deltaTime;
        animator.SetFloat("EnrageTime", _currentEnrageTime);

        if (_attackIsTiedToAnimation) return;

        _currentTimeBetweenAttacks -= Time.deltaTime;
        if (_currentTimeBetweenAttacks < 0f)
        {
            _attackRange.Attack();
            _currentTimeBetweenAttacks = _timeBetweenAttacks;
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("IsEnraged", false);
    }
}
