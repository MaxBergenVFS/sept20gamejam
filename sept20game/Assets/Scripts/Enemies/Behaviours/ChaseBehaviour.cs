﻿using UnityEngine;

public class ChaseBehaviour : StateMachineBehaviour
{
    private Pathfinding _pathfinding;
    private MusicBehaviourTrigger _musicBehaviourTrigger;

    [SerializeField]
    private float _speedMultiplier = 2f;

    private float _startingSpeed;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _pathfinding = animator.GetComponentInParent<Pathfinding>();
        _pathfinding.ChasePlayer();
        _musicBehaviourTrigger = animator.GetComponent<MusicBehaviourTrigger>();
        _musicBehaviourTrigger.TriggerAction();
        animator.GetComponentInChildren<EnemyVoiceSFX>().PlayEnemyVoiceChasing();

        IncreaseSpeed(animator);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _pathfinding.ChasePlayer();
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _pathfinding.Stop(true);
        ResetSpeed(animator);
        animator.SetBool("IsChasing", false);
    }

    private void IncreaseSpeed(Animator animator)
    {
        if (_speedMultiplier == 1) return;
        _startingSpeed = animator.speed;
        animator.speed *= _speedMultiplier;
        _pathfinding.NavMeshSpeedIncrease(_speedMultiplier);
    }

    private void ResetSpeed(Animator animator)
    {
        if (_speedMultiplier == 1) return;
        _pathfinding.NavMeshSpeedIncrease(1f);
        animator.speed = _startingSpeed;
    }
}
