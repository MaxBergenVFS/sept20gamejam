﻿using UnityEngine;

public class FlyingChaseBehaviour : StateMachineBehaviour
{
    private Pathfinding _pathfinding;
    private MusicBehaviourTrigger _musicBehaviourTrigger;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _pathfinding = animator.GetComponentInParent<Pathfinding>();
        _pathfinding.TrackPlayer();
        _musicBehaviourTrigger = animator.GetComponent<MusicBehaviourTrigger>();
        _musicBehaviourTrigger.TriggerAction();
        animator.GetComponentInChildren<EnemyVoiceSFX>().PlayEnemyVoiceChasing();
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _pathfinding.TrackPlayer();
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _pathfinding.Stop(true);
        animator.SetBool("IsChasing", false);
    }
}
