﻿using UnityEngine;

public class PatrolBehaviour : StateMachineBehaviour
{
    private Pathfinding _pathfinding;
    private MusicBehaviourTrigger _musicBehaviourTrigger;
    private float _pathfindingDist = 0.5f;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        //Debug.Log("patrolling");
        _pathfinding = animator.GetComponentInParent<Pathfinding>();
        _musicBehaviourTrigger = animator.GetComponent<MusicBehaviourTrigger>();
        _musicBehaviourTrigger.ResetTrigger();
        _pathfinding.GotoNextPoint(_pathfindingDist);
        animator.GetComponentInChildren<EnemyVoiceSFX>()?.PlayEnemyVoiceIdle();
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _pathfinding.GotoNextPoint(_pathfindingDist);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _pathfinding.Stop(true);
        animator.SetBool("IsPatrolling", false);
    }
}
