﻿using UnityEngine;

public class IdleBehaviour : StateMachineBehaviour
{
    private Pathfinding _pathfinding;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _pathfinding = animator.GetComponentInParent<Pathfinding>();
        _pathfinding.Stop(true);
        _pathfinding.SetNextPoint();
    }

    // override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    // {
    
    // }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("IsIdle", false);
    }
}
