﻿using UnityEngine;

public class IdleAtDestBehaviour : StateMachineBehaviour
{
    private Pathfinding _pathfinding;
    private float _idleAtDestTime;
    private bool _isStationary = false;
    private bool _exitingState = false;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _pathfinding = animator.GetComponentInParent<Pathfinding>();
        _pathfinding.Stop(true);
        _idleAtDestTime = animator.GetFloat("IdleAtDestTime");
        _isStationary = animator.GetBool("IsStationary");
        _exitingState = false;
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (_isStationary || _exitingState) return;

        _idleAtDestTime -= Time.deltaTime;
        if (_idleAtDestTime < 0f)
        {
            //_pathfinding.Stop();
            animator.SetBool("IsPatrolling", true);
            animator.SetBool("TurnAround", _pathfinding.CheckIfShouldTurnAround());
            _exitingState = true;
            //Debug.Log("exiting idle update");
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (_isStationary) { _pathfinding.ReturnToStartingPoint(); }
        else
        {
            //Debug.Log("print when OnStateExit of IdleAtDestBehaviour is called");
            _pathfinding.SetNextPoint();
        }
        //Debug.Log("exiting idle exit");
        animator.SetBool("IsIdleAtDest", false);
    }
}
