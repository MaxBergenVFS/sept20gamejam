﻿using UnityEngine;

public class FlinchBehaviour : StateMachineBehaviour
{
    [SerializeField]
    private float _flinchTime = 1f;

    private float _currentFlinchTime;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _currentFlinchTime = _flinchTime;
        animator.SetFloat("FlinchTime", _currentFlinchTime);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _currentFlinchTime -= Time.deltaTime;
        animator.SetFloat("FlinchTime", _currentFlinchTime);
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("IsFlinching", false);
    }
}
