﻿using UnityEngine;

public class DistractedBehaviour : StateMachineBehaviour
{
    [SerializeField]
    private float _pathfindingDist = 2f;

    private Pathfinding _pathfinding;
    private float _currentIdleTime;

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _pathfinding = animator.GetComponentInParent<Pathfinding>();
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _pathfinding.GotoNextPoint(_pathfindingDist);
       // Debug.Log($"{animator.gameObject.name} is distracted and CheckIfHasReachedDest() = {_pathfinding.CheckIfHasReachedDest()}");

        if (_pathfinding.CheckIfHasReachedDest())
        {
            animator.SetBool("ReachedDistractDest", true);
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        _pathfinding.Stop(true);
        animator.SetBool("IsDistracted", false);
    }
}
