﻿using UnityEngine;

public class EnemyWeakSpot : MonoBehaviour, IDamageable
{
    private EnemyController _enemyController;

    private void Awake()
    {
        _enemyController = GetComponentInParent<EnemyController>();
    }

    public void TakeDamage(int amt, bool player) { if (player) { _enemyController.Stun(); } }
    public bool CompareDefenseLevel(int atkLvl) { return true; }
    public bool CheckIfDead() { return false; }
    public bool CheckIfFlesh() { return true; }
}
