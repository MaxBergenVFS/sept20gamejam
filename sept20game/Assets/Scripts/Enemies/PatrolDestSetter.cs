﻿using UnityEngine;

public class PatrolDestSetter : MonoBehaviour
{
    [SerializeField]
    private Transform[] _points;
    [SerializeField]
    private int _pointToChangeTo;
    [SerializeField]
    private int _pointToChangeOnEnemy;
    [SerializeField]
    private Pathfinding _pathfinding;

    public void ChangeDestPointOnPath(int pointToChange, int pointToChangeTo)
    {
        _pathfinding.ChangeDestPoint(pointToChange, _points[pointToChangeTo]);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            ChangeDestPointOnPath(_pointToChangeOnEnemy, _pointToChangeTo);
        }
    }
}
