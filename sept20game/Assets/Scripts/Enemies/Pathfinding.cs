using UnityEngine;
using UnityEngine.AI;

public class Pathfinding : MonoBehaviour
{
    public Transform[] transformPoints;

    [SerializeField]
    private float _turnAroundAnimThreshold = 130f;

    private Vector3[] _points;
    private EnemyController _enemyController;
    private int _destPoint = 0;
    private NavMeshAgent _agent;
    private bool _isAttacking = false;
    private float _startingSpeed;
    private bool _hasReachedDest = false;
    private Vector3 _startingPos;
    private bool _enemyIsStationary = false;
    private Vector3 _lookRotation;
    private Transform _transform;
    private Transform _playerTransform;

    void Awake()
    {
        _agent = GetComponent<NavMeshAgent>();
        _enemyController = GetComponent<EnemyController>();
        _transform = transform;
        _agent.autoBraking = false;
        _agent.updateRotation = false;
        _startingSpeed = _agent.speed;
        _startingPos = new Vector3(_transform.position.x, _transform.position.y, _transform.position.z);
        if (transformPoints.Length == 0)
        {
            _enemyIsStationary = true;
            _points = new Vector3[1];
            _points[0] = _startingPos;
        }
        else
        {
            _points = new Vector3[transformPoints.Length];
            for (int i = 0; i < transformPoints.Length; i++)
            {
                _points[i] = transformPoints[i].position;
            }
        }
    }

    private void Start()
    {
        if (_enemyIsStationary) _enemyController.Stationary();
        _playerTransform = PlayerController.instance.transform;
    }

    private void Update()
    {
        if (_agent.isStopped) return;

        if (_agent.velocity.normalized != Vector3.zero)
        {
            if (!_isAttacking) { _lookRotation = _agent.velocity.normalized; }
            else { _lookRotation = _playerTransform.position - _transform.position; }
            _transform.rotation = Quaternion.LookRotation(_lookRotation);
        }
    }

    public void GotoNextPoint(float dist)
    {
        Stop(false);
        if (!_agent.pathPending && _agent.remainingDistance < dist)
        {
            if (_points.Length == 0) return;

            if (_enemyController.idleAtDest)
            {
                _enemyController.IdleAtDest();
            }
            else
            {
                Stop(true);
                SetNextPoint();
            }
            _hasReachedDest = true;
        }
    }

    public void Stop(bool cond) { if (_agent.isStopped != cond) _agent.isStopped = cond; }

    public void ChasePlayer()
    {
        if (_agent.isStopped) { _agent.isStopped = false; }
        Vector3 targetPos = PlayerController.instance.transform.position;
        if (CheckIfCanReachDest(targetPos)) { _agent.destination = targetPos; }
        else { _enemyController.Patrol(); }
    }

    public void SetNextPoint()
    {
        _agent.destination = _points[_destPoint];
        _destPoint = (_destPoint + 1) % _points.Length;
        _hasReachedDest = false;
    }

    public void SetNextPoint(Vector3 customDest)
    {
        if (CheckIfCanReachDest(customDest))
        {
            _agent.destination = customDest;
            _hasReachedDest = false;
        }
        else { SetNextPoint(); }
    }

    public void SetAttackPlayerRotation(bool cond) { _isAttacking = cond; }

    public bool CheckIfShouldTurnAround()
    {
        Vector3 targetDir = _points[_destPoint] - _transform.position;
        float angle = Vector3.Angle(targetDir, _transform.forward);
        if (angle > _turnAroundAnimThreshold) return true;
        return false;
    }

    public void NavMeshSpeedIncrease(float speedMultiplier)
    {
        _agent.speed = _startingSpeed;
        _agent.speed *= speedMultiplier;
    }

    public bool CheckIfHasReachedDest() { return _hasReachedDest; }

    public void ChangeDestPoint(int pointIndex, Transform pointToChangeTo)
    {
        _points[pointIndex] = pointToChangeTo.position;
    }

    public void ReturnToStartingPoint() { SetNextPoint(_startingPos); }

    public bool CheckIfCanReachDest(Vector3 targetPos)
    {
        NavMeshPath path = new NavMeshPath();
        _agent.CalculatePath(targetPos, path);
        if (path.status == NavMeshPathStatus.PathPartial) { return false; }
        else { return true; }
    }

    public void TrackPlayer()
    {
        Stop(true);
        _transform.rotation = Quaternion.LookRotation(_playerTransform.position - _transform.position);
    }
}
