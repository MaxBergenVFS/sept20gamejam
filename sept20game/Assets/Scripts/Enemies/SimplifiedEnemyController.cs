﻿using UnityEngine;
using UnityEngine.AI;

public class SimplifiedEnemyController : MonoBehaviour
{
    private Transform _player;
    private NavMeshAgent _agent;
    private bool _alreadyAttacked, _walkPointSet, _playerInSightRange, _playerInAttackRange, _enemyIsIdle;
    private AttackRange _attack;
    private Animator _anim;

    public LayerMask _whatIsGround, _whatIsPlayer;

    [SerializeField] private int _baseDamage;
    [SerializeField] private Vector3 _walkPoint;
    [SerializeField] private float _walkPointRange, _timeBetweenAttacks, _sightRange, _attackRange;

    private void Awake()
    {
        _attack = GetComponent<AttackRange>();
        _anim = GetComponent<Animator>();
    }

    private void Start()
    {
        _player = PlayerController.instance.GetComponent<Transform>();
        _agent = GetComponent<NavMeshAgent>();
        Idle();
    }

    private void Update()
    {
        _playerInSightRange = Physics.CheckSphere(transform.position, _sightRange, _whatIsPlayer);
        _playerInAttackRange = Physics.CheckSphere(transform.position, _attackRange, _whatIsPlayer);

        if ((_playerInSightRange || _playerInAttackRange) && _enemyIsIdle) _enemyIsIdle = false;
        if (_enemyIsIdle) return;

        if (!_playerInSightRange && !_playerInAttackRange) Patrolling();
        else if (_playerInSightRange && !_playerInAttackRange) ChasePlayer();
        else if (_playerInSightRange && _playerInAttackRange) AttackPlayer();

        //constrain rotation on X axis
        transform.eulerAngles = new Vector3(0f, transform.eulerAngles.y, transform.eulerAngles.z);
    }

    private void Patrolling()
    {
        if (!_walkPointSet) { SearchWalkPoint(); }
        else { _agent.SetDestination(_walkPoint); }

        Vector3 distanceToWalkPoint = transform.position - _walkPoint;
        if (distanceToWalkPoint.magnitude < 1f) _walkPointSet = false;
    }

    private void SearchWalkPoint()
    {
        float randomZ = Random.Range(-_walkPointRange, _walkPointRange);
        float randomX = Random.Range(-_walkPointRange, _walkPointRange);

        _walkPoint = new Vector3(transform.position.x + randomX, transform.position.y, transform.position.z + randomZ);

        if (Physics.Raycast(_walkPoint, -transform.up, 2f, _whatIsGround)) _walkPointSet = true;
    }

    private void Idle() => _enemyIsIdle = true;

    private void ChasePlayer()
    {
        _anim.SetTrigger("isChasing");
        _agent.SetDestination(_player.position);
    }
    private void AttackPlayer()
    {
        _agent.SetDestination(transform.position);

        transform.LookAt(_player);

        if (!_alreadyAttacked)
        {
            _alreadyAttacked = true;
            _anim.SetTrigger("isAttacking");
            Invoke(nameof(ResetAttack), _timeBetweenAttacks);
        }
    }

    public void Die() => Destroy(this.gameObject);

    public void DealDamage()
    {
        Collider[] hitObj = Physics.OverlapSphere(transform.position, _attackRange, _whatIsPlayer);
        foreach (Collider obj in hitObj)
        {
            if (obj.GetComponent<IDamageable>() != null)
            {
                IDamageable damageable = obj.GetComponent<IDamageable>();
                damageable.TakeDamage(_baseDamage, false);
            }
        }
    }

    private void ResetAttack() => _alreadyAttacked = false;

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, _attackRange);
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, _sightRange);
    }

}
