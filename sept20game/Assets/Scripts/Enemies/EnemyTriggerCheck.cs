﻿using UnityEngine;

public class EnemyTriggerCheck : TriggerCheck
{
    [SerializeField]
    private EnemyController _enemyController;

    protected override void TriggerAction()
    {
        base.TriggerAction();
        _enemyController.Patrol();
    }
}
