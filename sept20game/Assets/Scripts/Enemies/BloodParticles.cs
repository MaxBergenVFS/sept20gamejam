﻿using UnityEngine;

public class BloodParticles : MonoBehaviour
{
    private ParticleSystem[] _bloodParticles;
    private float _duration = 0f;
    private bool _bleeding = false;

    private void Awake()
    {
        _bloodParticles = GetComponentsInChildren<ParticleSystem>();
        for (int i = 0; i < _bloodParticles.Length; i++)
        {
            var em = _bloodParticles[i].emission;
            em.enabled = false;
        }
    }

    void Update()
    {
        if (!_bleeding) return;

        _duration -= Time.fixedDeltaTime;

        if (_duration < 0f)
        {
            PlayBloodParticles(false);
        }
    }

    public void SpawnBlood(float duration)
    {
        PlayBloodParticles(true);
        _duration = duration;
        //Debug.Log("spawning blood");
    }

    private void PlayBloodParticles(bool cond)
    {
        for (int i = 0; i < _bloodParticles.Length; i++)
        {
            var em = _bloodParticles[i].emission;
            em.enabled = cond;
            _bleeding = cond;
            this.gameObject.SetActive(cond);
        }
    }
}
