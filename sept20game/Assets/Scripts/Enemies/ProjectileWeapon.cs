﻿using System.Collections;
using UnityEngine;

public class ProjectileWeapon : MonoBehaviour
{
    [SerializeField] private GameObject _projectile;
    [SerializeField] private float _coolDown = 1.0f;
    [SerializeField] private Transform _origin;
    [SerializeField] private Vector3 _projectileAngleOffset = new Vector3();
    [SerializeField] private Animator _anim;

    private Camera _camera;
    private Transform _playerTransform, _cameraTransform;
    private PlayerInventory _playerInventory;
    private InventoryItemDatabase _inventoryDatabase;
    private WeaponSFX _sfx;
    private bool _onCoolDown, playerHasAmmoRemaining;
    private int _projectileID;

    private void Awake()
    {
        if (_anim == null) _anim = GetComponent<Animator>();
        _sfx = GetComponent<WeaponSFX>();
        _camera = Camera.main;
        _cameraTransform = _camera.transform;
    }

    private void Start()
    {
        _playerTransform = PlayerController.instance.transform;
        _playerInventory = PlayerController.instance.GetComponent<PlayerInventory>();
        _inventoryDatabase = _playerInventory.inventoryDatabase;
        _projectileID = _projectile.GetComponent<Projectile>().GetDatabaseID();
    }

    public void SpawnProjectile()
    {
        if (_onCoolDown) return;

        playerHasAmmoRemaining = _playerInventory.CheckIfHasReagent(_projectileID);
        _anim.SetTrigger("Shoot");
        if (playerHasAmmoRemaining)
        {
            GameObject projectile = Instantiate(_projectile, _origin.position, Quaternion.identity);
            Vector3 targetDir = new Vector3();
            targetDir = _cameraTransform.forward + _projectileAngleOffset;
            projectile.GetComponent<Projectile>().Shoot(_origin.position, targetDir);
            _playerInventory.ReduceReagentItemAmt(_projectileID);
            _sfx.PlayShootCrossbowSound();

            int ammoRemaining = _playerInventory.GetCurrentReagentAmt(_projectileID);
            SetAnimAmmoRemaining(ammoRemaining);
        }
        else
        {
            _sfx.PlayNoAmmoCrossbowSound();
        }
        playerHasAmmoRemaining = _playerInventory.CheckIfHasReagent(_projectileID);
        StopAllCoroutines();
        StartCoroutine(StartCoolDown());
    }

    private void SetAnimAmmoRemaining(int ammo) => _anim.SetInteger("AmmoRemaining", ammo);

    IEnumerator StartCoolDown()
    {
        _onCoolDown = true;
        yield return new WaitForSeconds(_coolDown);
        _onCoolDown = false;

    }
}
