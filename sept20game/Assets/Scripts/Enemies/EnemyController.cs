﻿using UnityEngine;

public class EnemyController : MonoBehaviour, IEnrageable, IDistractable, IBlindable
{
    [SerializeField]
    private float _idleAtDestTime = 0f;
    [SerializeField]
    private bool _startIdle = true;

    [HideInInspector]
    public bool idleAtDest = false;

    private float _currentIdleAtDestTime = 0f;
    private Pathfinding _pathfinding;
    private Animator _anim;

    private void Awake()
    {
        _pathfinding = GetComponent<Pathfinding>();
        _anim = GetComponentInChildren<Animator>();
        if (_idleAtDestTime > 0f) idleAtDest = true;
        if (_startIdle) { Idle(); }
        else { Patrol(); }
    }

    private void Start()
    {
        _pathfinding.SetNextPoint();
    }

    public void Die() { _anim.SetBool("IsDying", true); }

    public void Enrage(Vector3 enrageDest)
    {
        _anim.SetBool("IsEnraged", true);
        _anim.SetBool("IsDistracted", false);
        _pathfinding.SetNextPoint(enrageDest);
    }

    public void Chase()
    {
        if (_anim.GetBool("IsChasing")) return;
        _anim.SetBool("IsChasing", true);
        _anim.SetBool("IsDistracted", false);
    }

    public void Patrol() { _anim.SetBool("IsPatrolling", true); }

    public void Idle() { _anim.SetBool("IsIdle", true); }

    public void SetAttackDistance(float num)
    {
        _anim.SetFloat("AttackDistance", num);
    }

    public void IdleAtDest()
    {
        if (_anim.GetBool("IsDistracted")) return;

        _anim.SetFloat("IdleAtDestTime", _idleAtDestTime);
        _anim.SetBool("IsIdleAtDest", true);
    }

    public void Distract(Vector3 distractionDest)
    {
        _anim.SetBool("IsDistracted", true);
        _pathfinding.SetNextPoint(distractionDest);
    }

    public void DisableDistract()
    {
        _anim.SetBool("IsDistracted", false);
        _pathfinding.SetNextPoint();
    }

    public void Flinch(bool cond)
    {
        _anim.SetBool("IsFlinching", true);
        _anim.SetBool("IsDistracted", false);
        _anim.SetBool("GoFromFlinchingToChasing", cond);
    }

    public void Stun()
    {
        _anim.SetBool("IsStunned", true);
        _anim.SetBool("IsDistracted", false);
    }

    public void Stationary() { _anim.SetBool("IsStationary", true); }

    public void Blind() { _anim.SetBool("IsBlinded", true); }
}
