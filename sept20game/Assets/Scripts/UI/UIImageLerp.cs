﻿using UnityEngine;
using UnityEngine.UI;

public class UIImageLerp : MonoBehaviour
{
    [SerializeField]
    private float _timeBeforeFade = 1.5f;

    private Image _image;
    private float _colorR;
    private float _colorG;
    private float _colorB;
    private bool _displayingImage = false;
    private float _currentFadeTime;
    private float _lerpValue = 1f;
    private float _lerpSpeed = 4f;

    private void Awake()
    {
        _image = GetComponent<Image>();
        _colorR = _image.color.r;
        _colorG = _image.color.g;
        _colorB = _image.color.b;
        SetValues(0f);
    }

    private void Update()
    {
        if (!_displayingImage) return;

        _currentFadeTime -= Time.deltaTime;
        if (_currentFadeTime <= 0f) { FadeOut(); }
    }

    private void SetValues(float alpha)
    {
        _image.color = new Color(_colorR, _colorG, _colorB, alpha);
        _lerpValue = 1f;
        _currentFadeTime = _timeBeforeFade;
        bool alphaIsGreaterThan0;
        if (alpha > 0) { alphaIsGreaterThan0 = true; }
        else { alphaIsGreaterThan0 = false; }
        _displayingImage = alphaIsGreaterThan0;
    }

    public void DisplayImage()
    {
        SetValues(1f);
    }

    private void FadeOut()
    {
        _lerpValue -= Time.deltaTime * _lerpSpeed;
        float alpha = Mathf.Lerp(0f, 1f, _lerpValue);
        _image.color = new Color(_colorR, _colorG, _colorB, alpha);
        if (_lerpValue < 0f)
        {
            SetValues(0f);
        }
    }
}
