﻿using UnityEngine;
using TMPro;

public class DraggableUIElementInfoText : MonoBehaviour
{
    [SerializeField]
    private string _infoText = "";

    private TextMeshProUGUI _text;

    private void Awake()
    {
        _text = GetComponent<TextMeshProUGUI>();
    }

    public void DisplayInfoText(){_text.text = _infoText;}

    public void RemoveInfoText(){_text.text = "";}
}
