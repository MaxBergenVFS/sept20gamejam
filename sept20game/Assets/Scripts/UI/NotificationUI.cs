﻿public class NotificationUI : UITextLerp
{
    public static NotificationUI instance { get { return _instance; } }
    private static NotificationUI _instance;

    protected override void Awake()
    {
        _instance = this;
        base.Awake();
    }
}
