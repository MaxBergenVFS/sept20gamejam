﻿using TMPro;
using UnityEngine;

public class UITextLerp : MonoBehaviour
{
    [SerializeField]
    private float _textDisplayTime = 3f;

    protected float _lerpSpeed = 10f;
    private TextMeshProUGUI _text;
    private float _currentTextFadeTime;
    private bool _displayingText = false;
    private bool _fadingInText = false;
    private float _lerpValue = 0f;
    private float _colorR;
    private float _colorG;
    private float _colorB;

    protected virtual void Awake()
    {
        _currentTextFadeTime = _textDisplayTime;
        _text = GetComponent<TextMeshProUGUI>();
        _colorR = _text.faceColor.r;
        _colorG = _text.faceColor.g;
        _colorB = _text.faceColor.b;
        _text.faceColor = new Color(_colorR, _colorG, _colorB, 0f);
    }

    private void Update()
    {
        if (!_displayingText) return;

        if (_fadingInText) LerpText(0f, 1f);

        _currentTextFadeTime -= Time.deltaTime;

        if (_currentTextFadeTime < 0f)
        {
            LerpText(1f, 0f);
        }
    }

    public virtual void DisplayTxt(string str)
    {
        _displayingText = true;
        _fadingInText = true;
        _lerpValue = 1f;
        _text.text = str;
        _currentTextFadeTime = _textDisplayTime;
    }

    public void DisplayTxt(string str, float displayTime)
    {
        _displayingText = true;
        _fadingInText = true;
        _lerpValue = 1f;
        _text.text = str;
        _currentTextFadeTime = displayTime;
    }

    private void LerpText(float start, float end)
    {
        _lerpValue -= Time.deltaTime * _lerpSpeed;
        float alpha = Mathf.Lerp(end, start, _lerpValue);
        _text.faceColor = new Color(_colorR, _colorG, _colorB, alpha);
        if (_lerpValue < 0f)
        {
            if (_fadingInText)
            {
                _fadingInText = false;
                _lerpValue = 1f;
            }
            else
            {
                _displayingText = false;
                _text.text = "";
            }
        }
    }
}
