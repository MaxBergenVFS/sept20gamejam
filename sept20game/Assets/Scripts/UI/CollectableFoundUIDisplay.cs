﻿using UnityEngine;
using TMPro;

public class CollectableFoundUIDisplay : MonoBehaviour
{
    private TextMeshProUGUI _text;
    private int _collectablesFoundInLevel = 0;
    private int _totalCollectablesInLevel = 0;

    private void Awake()
    {
        _text = GetComponent<TextMeshProUGUI>();
    }

    public void SetTotalCollectablesInLevel(int num)
    {
        _totalCollectablesInLevel = num;
        UpdateUIText();
    }

    public void SetCollectablesFoundInLevel(int num)
    {
        _collectablesFoundInLevel = num;
        UpdateUIText();
    }

    private void UpdateUIText()
    {
        _text.text = $"{_collectablesFoundInLevel}/{_totalCollectablesInLevel}";
    }
}
