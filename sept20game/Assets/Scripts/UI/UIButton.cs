﻿using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIButton : MonoBehaviour
{
    [SerializeField]
    private Button _quitButton;
    [SerializeField]
    private Button _reloadButton;
    [SerializeField]
    private Button _startButton;
    [SerializeField]
    private Button _mainMenuButton;
    [SerializeField]
    private Button _continueButton;
    [SerializeField]
    private Button _loadButton;
    [SerializeField]
    private Button _deleteSaveFileButton;
    [SerializeField]
    private Button _displayControlsButton;
    [SerializeField]
    private Button _closeControlsButton;
    [SerializeField]
    private GameObject _controlsMenu;
    [SerializeField]
    private Animator _levelChangeFade;
    [SerializeField]
    private int _nextScene = 0;

    private int _saveRoomIndex = 6;

    void Start()
    {
        if (_quitButton)
        {
            Button quitBtn = _quitButton.GetComponent<Button>();
            quitBtn.onClick.AddListener(QuitGame);
        }
        if (_reloadButton)
        {
            Button reloadBtn = _reloadButton.GetComponent<Button>();
            reloadBtn.onClick.AddListener(ReloadScene);
        }
        if (_startButton)
        {
            Button startBtn = _startButton.GetComponent<Button>();
            startBtn.onClick.AddListener(NextScene);
        }
        if (_mainMenuButton)
        {
            Button mainMenuBtn = _mainMenuButton.GetComponent<Button>();
            mainMenuBtn.onClick.AddListener(MainMenu);
        }
        if (_continueButton)
        {
            Button continueBtn = _continueButton.GetComponent<Button>();
            continueBtn.onClick.AddListener(NextScene);
        }
        if (_displayControlsButton)
        {
            Button displayControlsBtn = _displayControlsButton.GetComponent<Button>();
            displayControlsBtn.onClick.AddListener(DisplayControls);
        }
        if (_closeControlsButton)
        {
            Button closeControlsBtn = _closeControlsButton.GetComponent<Button>();
            closeControlsBtn.onClick.AddListener(CloseControls);
        }
        if (_loadButton)
        {
            Button loadBtn = _loadButton.GetComponent<Button>();
            loadBtn.onClick.AddListener(LoadGame);
            HideButtonIfNoSaveFileExists(_loadButton.gameObject);
        }
        if (_deleteSaveFileButton)
        {
            Button deleteSaveFileBtm = _deleteSaveFileButton.GetComponent<Button>();
            deleteSaveFileBtm.onClick.AddListener(DeleteSaveFile);
            HideButtonIfNoSaveFileExists(_deleteSaveFileButton.gameObject);
        }
    }

    void DisplayControls()
    {
        _controlsMenu.SetActive(true);
    }

    void CloseControls()
    {
        _controlsMenu.SetActive(false);
    }

    void QuitGame()
    {
        print("You've quit!");
        Application.Quit();
    }

    void ReloadScene()
    {
        HideCursor();
        if (CheckpointManager.instance)
        {
            CheckpointManager.instance.SpawnPlayerAtRecentCheckpoint();
        }
        else
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }
    }

    void LoadGame()
    {
        HideCursor();
        GameManager.instance.SetLoadData();
        _levelChangeFade.gameObject.GetComponent<LevelChangeFade>().OverrideLevelIndex(_saveRoomIndex);
        _levelChangeFade.SetTrigger("FadeOut");
    }

    void DeleteSaveFile()
    {
        SaveSystem.DeleteData();
        HideButtonIfNoSaveFileExists(_loadButton.gameObject);
        HideButtonIfNoSaveFileExists(_deleteSaveFileButton.gameObject);
        print("save file deleted");
    }

    void NextScene()
    {
        HideCursor();
        _levelChangeFade.SetTrigger("FadeOut");
    }

    void MainMenu()
    {
        //Debug.Log("MAIN MENU");
        Cursor.visible = true;
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.None;
        SceneManager.LoadScene(0);
    }

    private void HideCursor()
    {
        Cursor.visible = false;
        Time.timeScale = 1;
        Cursor.lockState = CursorLockMode.Locked;
    }

    private void HideButtonIfNoSaveFileExists(GameObject btn)
    {
        if (!SaveSystem.CheckIfSaveDataExists()) btn.SetActive(false);
    }
}
