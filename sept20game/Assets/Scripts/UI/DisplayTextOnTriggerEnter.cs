﻿using UnityEngine;

public class DisplayTextOnTriggerEnter : TriggerCheck
{
    [SerializeField]
    private string _txtToDisplay;
    [SerializeField]
    private float _txtDisplayTime = 10f;
    [SerializeField]
    private bool _canBeRetriggered = false;

    protected override void TriggerAction()
    {
        base.TriggerAction();
        NotificationUI.instance.DisplayTxt(_txtToDisplay, _txtDisplayTime);
        if (_canBeRetriggered) ResetTrigger();
    }
}
