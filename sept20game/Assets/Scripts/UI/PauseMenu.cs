﻿using TMPro;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public GameObject pauseMenu;

    [SerializeField]
    private GameObject _controlsMenu;
    [SerializeField]
    private GameObject _levelFadeCanvas;
    [SerializeField]
    private TextMeshProUGUI _levelNameRef;

    private InputManager _input;
    private bool _menuIsActive = false;
    private Collectables _collectables;
    private PlayerFootstepSFX _playerFootstepSFX;

    private void Awake()
    {
        _input = GetComponent<InputManager>();
        _collectables = GetComponent<Collectables>();
    }

    private void Start()
    {
        _playerFootstepSFX = GetComponent<PlayerFootstepSFX>();
    }

    public void SetLevelName(string levelName) { _levelNameRef.text = levelName; }

    public void Pause()
    {
        if (GameManager.instance.playerIsDead) return;

        _menuIsActive = !_menuIsActive;
        pauseMenu.SetActive(_menuIsActive);
        _collectables.ActivateCollectableOnPickupUI(!_menuIsActive);

        if (_menuIsActive)
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            Time.timeScale = 0;
            _input.isPaused = true;
            _playerFootstepSFX.DontPlaySFXOnEnable();
            if (_levelFadeCanvas) { _levelFadeCanvas.SetActive(false); }
            else { Debug.LogError($"Missing LevelFadeCanvas reference on {this.gameObject.name}"); }
        }
        else
        {
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            Time.timeScale = 1;
            _input.isPaused = false;
            if (_controlsMenu.activeSelf) _controlsMenu.SetActive(false);
        }
    }
}
