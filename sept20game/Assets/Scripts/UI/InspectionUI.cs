﻿using System.Collections;
using TMPro;
using UnityEngine;

public class InspectionUI : MonoBehaviour
{
    public static InspectionUI instance { get { return _instance; } }
    private static InspectionUI _instance;

    [SerializeField]
    private float _timeBetweenLetters = 0.1f;
    [SerializeField]
    private float _timeBeforeFadeOut = 3f;
    [SerializeField]
    private float _fadeOutSpeed = 1f;

    private TextMeshProUGUI _text;
    private float _colorR;
    private float _colorG;
    private float _colorB;
    private float _lerpValue = 1f;
    private float _lerpSpeed = 4f;
    private bool _currentlyTypingSentence = false;
    private string _currentSentenceRef = "";

    private void Awake()
    {
        _instance = this;
        _text = GetComponent<TextMeshProUGUI>();
        _colorR = _text.faceColor.r;
        _colorG = _text.faceColor.g;
        _colorB = _text.faceColor.b;
        ResetTextDisplayValues(1f);
    }

    public void DisplayTxt(string str)
    {
        _currentSentenceRef = str;
        ResetTextDisplayValues(1f);
        StopAllCoroutines();
        StartCoroutine(TypeSentence(_currentSentenceRef));
    }

    private void ResetTextDisplayValues(float alpha)
    {
        _text.faceColor = new Color(_colorR, _colorG, _colorB, alpha);
        _lerpValue = 1f;
    }

    private void EndSentence()
    {
        _currentlyTypingSentence = false;
        StopAllCoroutines();
        StartCoroutine(FadeOut());
    }

    public bool CheckIfCurrentlyTypingSentence()
    {
        if (_currentlyTypingSentence)
        {
            StopAllCoroutines();
            _text.text = _currentSentenceRef;
            EndSentence();
            ResetTextDisplayValues(1f);
            return true;
        }
        else { return false; }
    }

    IEnumerator TypeSentence(string sentence)
    {
        _text.text = "";
        _currentlyTypingSentence = true;
        foreach (char letter in sentence.ToCharArray())
        {
            _text.text += letter;
            FMODUnity.RuntimeManager.PlayOneShot("event:/UI/dialogueTyping");
            if (_text.text == sentence)
            {
                EndSentence();
            }
            yield return new WaitForSeconds(_timeBetweenLetters);
        }
    }

    IEnumerator FadeOut()
    {
        yield return new WaitForSeconds(_timeBeforeFadeOut);
        float timeElapsed = 0;
        while (timeElapsed < _fadeOutSpeed)
        {
            _lerpValue = Mathf.Lerp(1f, 0f, timeElapsed / _fadeOutSpeed);
            timeElapsed += Time.deltaTime;
            _text.faceColor = new Color(_colorR, _colorG, _colorB, _lerpValue);
            yield return null;
        }
        ResetTextDisplayValues(0f);
    }
}
