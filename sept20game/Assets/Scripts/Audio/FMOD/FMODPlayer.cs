﻿using UnityEngine;

public class FMODPlayer : MonoBehaviour
{
    [SerializeField]
    private string _path;

    private Transform _transform;
    
    private void Awake()
    {
        _transform = this.transform;
    }
    public void PlayOneShotEvent()
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/" + _path, _transform.position);
    }
}
