﻿using FMODUnity;
using UnityEngine;

public class AudioParamaterHandler : MonoBehaviour
{
    [SerializeField] private string[] _audioParameters;

    private MusicManager _musicManager;

    public void SetNextParameter(FMOD.Studio.EventInstance musicInstance, int index)
    {
        //_musicManager = GameObject.Find("MusicManager").GetComponent<MusicManager>();
        if (index >= _audioParameters.Length) index = 0;
        for (int i = 0; i < index + 1; i++) { musicInstance.setParameterByName(_audioParameters[i], 1); }
        //_musicManager.IncrementParamIndex();
    }
}
