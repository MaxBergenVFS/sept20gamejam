﻿using UnityEngine;

public class DynamicMusicManager : MonoBehaviour
{
    private DynamicMusicPlayer[] _dynMusicPlayers;

    private void Awake()
    {
        _dynMusicPlayers = GetComponentsInChildren<DynamicMusicPlayer>();
    }

    public void TriggerMusic(string type, int player, int clip)
    {
        if (type == "StartLoop")
        {
            _dynMusicPlayers[player].StartLoop(clip);
        }
        else if (type == "StopAudio")
        {
            _dynMusicPlayers[player].StopAudio();
        }
        else if (type == "PlayOneShot")
        {
            _dynMusicPlayers[player].PlayOneShot(clip);
        }
    }

    public int GetDynPlayerLength()
    {
        return _dynMusicPlayers.Length;
    }
}
