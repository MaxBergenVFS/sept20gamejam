﻿using UnityEngine;

public class CutsceneDynMusic : MonoBehaviour
{
    private DynamicMusicManager _dynMusicManager;

    [SerializeField]
    private UtilityHelper.musicTriggerEnum _musicTriggerType;
    [SerializeField]
    private int _playerNum;
    [SerializeField]
    private int _clipNum;
    [SerializeField]
    private bool _stopAllDynMusicOnDisable = false;
    [SerializeField]
    private GameObject[] _objsToRemoveOnDisable;

    private void Awake()
    {
        _dynMusicManager = FindObjectOfType<DynamicMusicManager>();
    }

    private void OnEnable()
    {
        TriggerMusic(_playerNum, _clipNum);
    }

    private void OnDisable()
    {
        if (_stopAllDynMusicOnDisable)
        {
            int maxNum =_dynMusicManager.GetDynPlayerLength();
            for (int i = 0; i < maxNum; i++)
            {
                _dynMusicManager.TriggerMusic("StopAudio", i, _clipNum);
            }
            foreach (var obj in _objsToRemoveOnDisable)
            {
                obj.SetActive(false);
            }
        }
    }

    private void TriggerMusic(int player, int clip)
    {
        _dynMusicManager.TriggerMusic(_musicTriggerType.ToString(), player, clip);
    }
}
