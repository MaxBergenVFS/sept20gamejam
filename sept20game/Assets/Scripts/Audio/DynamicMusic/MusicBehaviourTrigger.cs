﻿using System.Collections;
using UnityEngine;

public class MusicBehaviourTrigger : MonoBehaviour
{
    [SerializeField]
    private string _path;
    [SerializeField]
    private bool _changeParameter = false;
    [SerializeField]
    private string _parameterName = "EnemyAlerted";
    [SerializeField]
    private float _startValue = 0f;
    [SerializeField]
    private float _endValue = 1f;
    [SerializeField]
    private float _timeBeforeValueChange = 0.5f;
    [SerializeField]
    private float _timeBeforeEnemyAlertStabCanBeRetriggered = 60f;

    private MusicManager _musicManager;
    private float _timeRemainingBeforeAlertStabCanBeTriggered;
    private FMOD.Studio.EventInstance _currentEventInstance;
    private float _currentValue;
    private string _dynamicMusicPrefix = "event:/Music/Dynamic/";
    private bool _hasBeenTriggered = false;

    private void Awake()
    {
        _currentValue = _startValue;
        _timeRemainingBeforeAlertStabCanBeTriggered = 0f;
    }

    private void Start()
    {
        _musicManager = GameObject.Find("MusicManager").GetComponent<MusicManager>();
    }

    public void TriggerAction()
    {
        if (_hasBeenTriggered) return;
        _hasBeenTriggered = true;
        if (_timeRemainingBeforeAlertStabCanBeTriggered <= 0f)
        {
            FMODUnity.RuntimeManager.PlayOneShot(_dynamicMusicPrefix + _path);
            StopCoroutine(EnemyAlertStabRetriggerTimeout(_timeBeforeEnemyAlertStabCanBeRetriggered));
            StartCoroutine(EnemyAlertStabRetriggerTimeout(_timeBeforeEnemyAlertStabCanBeRetriggered));
        }

        if (_changeParameter) { ChangeParameter(_startValue, _endValue); }
    }

    public void ResetTrigger()
    {
        if (!_hasBeenTriggered) return;
        _hasBeenTriggered = false;
        if (_changeParameter) { ChangeParameter(_endValue, _startValue); }
    }

    private void OnDisable()
    {
        if (_changeParameter)
        {
            _currentEventInstance = _musicManager.GetCurrentEventInstance();
            _currentEventInstance.setParameterByName(_parameterName, _startValue);
        }
    }

    private void ChangeParameter(float startValue, float endValue)
    {
        _currentEventInstance = _musicManager.GetCurrentEventInstance();
        if (_timeBeforeValueChange > 0)
        {
            StopCoroutine(LerpParameter(startValue, endValue));
            StartCoroutine(LerpParameter(startValue, endValue));
        }
        else { _currentEventInstance.setParameterByName(_parameterName, endValue); }
    }

    IEnumerator EnemyAlertStabRetriggerTimeout(float t)
    {
        _timeRemainingBeforeAlertStabCanBeTriggered = t;
        while (_timeRemainingBeforeAlertStabCanBeTriggered > 0f)
        {
            _timeRemainingBeforeAlertStabCanBeTriggered -= Time.deltaTime;
            yield return null;
        }
        _timeRemainingBeforeAlertStabCanBeTriggered = 0f;
    }

    IEnumerator LerpParameter(float startValue, float endValue)
    {
        float timeElapsed = 0;

        while (timeElapsed < _timeBeforeValueChange)
        {
            _currentValue = Mathf.Lerp(startValue, endValue, timeElapsed / _timeBeforeValueChange);
            timeElapsed += Time.deltaTime;
            _currentEventInstance.setParameterByName(_parameterName, _currentValue);
            yield return null;
        }
        _currentValue = endValue;
        _currentEventInstance.setParameterByName(_parameterName, _currentValue);
    }
}
