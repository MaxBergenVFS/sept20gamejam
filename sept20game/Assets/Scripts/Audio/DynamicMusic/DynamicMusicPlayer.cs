﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class DynamicMusicPlayer : MonoBehaviour
{
    [SerializeField]
    protected AudioClip[] _audioClips;

    private AudioSource _audioSource;
    private float _fadeRate = 2f;
    private float _defaultVolume;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _defaultVolume = _audioSource.volume;
    }

    public void StartLoop(int index)
    {
        if (_audioSource.isPlaying)
        {
            StartCoroutine("PlayAfterFadeOut", index);
        }
        else
        {
            PlayAudio(index);
        }
    }

    public void StopAudio()
    {
        StartCoroutine("FadeOut");
    }

    public void PlayOneShot(int index)
    {
        _audioSource.PlayOneShot(_audioClips[index], 1);
    }

    private void PlayAudio(int index)
    {
        _audioSource.volume = _defaultVolume;
        _audioSource.loop = true;
        _audioSource.clip = _audioClips[index];
        _audioSource.Play();
    }

    private IEnumerator FadeOut()
    {
        for (float vol = _audioSource.volume; vol > 0f; vol -= _fadeRate * Time.deltaTime)
        {
            _audioSource.volume = vol;
            yield return null;
        }

        _audioSource.volume = 0f;
        _audioSource.Stop();
    }

    private IEnumerator PlayAfterFadeOut(int index)
    {
        for (float vol = _audioSource.volume; vol > 0f; vol -= _fadeRate * Time.deltaTime)
        {
            _audioSource.volume = vol;
            yield return null;
        }

        PlayAudio(index);
    }
}
