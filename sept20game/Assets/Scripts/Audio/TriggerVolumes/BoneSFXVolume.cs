﻿using UnityEngine;

public class BoneSFXVolume : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerFootstepSFX>().SetBonesParameter(true);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerFootstepSFX>().SetBonesParameter(false);
        }
    }
}
