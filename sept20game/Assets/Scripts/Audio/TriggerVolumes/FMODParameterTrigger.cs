﻿using System.Collections;
using UnityEngine;

public class FMODParameterTrigger : TriggerCheck
{
    [SerializeField]
    private int _musicZoneNum = 0;
    [SerializeField]
    private string _parameterName = "Bass";
    [SerializeField]
    private float _startValue = 0f;
    [SerializeField]
    private float _endValue = 1f;
    [SerializeField]
    private float _timeBeforeValueChange = 1f;
    [SerializeField]
    private bool _canReset = false;

    private MusicManager _musicManager;
    private PlayerZoneManager _zoneManager;
    private FMOD.Studio.EventInstance _currentEventInstance;

    private float _currentValue;

    private void Awake()
    {
        _currentValue = _startValue;
        if (GetComponentInParent<PlayerZoneManager>() != null)
        {
            _zoneManager = GetComponentInParent<PlayerZoneManager>();
        }
        else { Debug.LogError($"Missing PlayerZoneManager component in parent on {gameObject.name}"); }

    }

    private void Start()
    {
        _musicManager = GameObject.Find("MusicManager").GetComponent<MusicManager>();
    }

    protected override void TriggerAction()
    {
        if (!_canReset || _musicZoneNum != _zoneManager.GetZoneNum())
        {
            base.TriggerAction();
            _currentEventInstance = _musicManager.GetCurrentEventInstance();
            if (_timeBeforeValueChange > 0)
            {
                StopAllCoroutines();
                StartCoroutine(LerpParameter());
            }
            else { _currentEventInstance.setParameterByName(_parameterName, _endValue); }
            if (_canReset) ResetTrigger();
        }
        _zoneManager.SetZoneNum(_musicZoneNum);
    }

    IEnumerator LerpParameter()
    {
        float timeElapsed = 0;

        while (timeElapsed < _timeBeforeValueChange)
        {
            _currentValue = Mathf.Lerp(_startValue, _endValue, timeElapsed / _timeBeforeValueChange);
            timeElapsed += Time.deltaTime;
            _currentEventInstance.setParameterByName(_parameterName, _currentValue);
            yield return null;
        }
        _currentValue = _endValue;
        _currentEventInstance.setParameterByName(_parameterName, _currentValue);
    }
}
