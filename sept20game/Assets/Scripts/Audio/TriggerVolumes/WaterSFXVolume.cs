﻿using UnityEngine;

public class WaterSFXVolume : MonoBehaviour
{
    [SerializeField]
    private bool _playFootstepSoundInWater = true;

    public bool CheckIfSwimming() { return !_playFootstepSoundInWater; }

    // private void OnTriggerEnter(Collider other)
    // {
    //     if (other.CompareTag("Player"))
    //     {
    //         other.GetComponent<PlayerFootstepSFX>().SetWaterParameter(true, !_playFootstepSoundInWater);
    //     }
    // }
    // private void OnTriggerExit(Collider other)
    // {
    //     if (other.CompareTag("Player"))
    //     {
    //         other.GetComponent<PlayerFootstepSFX>().SetWaterParameter(false, !_playFootstepSoundInWater);
    //     }
    // }
}
