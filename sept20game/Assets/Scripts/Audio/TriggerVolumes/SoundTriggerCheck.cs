﻿using UnityEngine;
using FMODUnity;

public class SoundTriggerCheck : TriggerCheck
{
    [SerializeField]
    private bool _canBeRetriggered = false;
    [SerializeField]
    private string _path;

    private string _pathPrefix = "event:/";
    private FMOD.Studio.EventInstance _musicTrack;
    private static MusicManager _instance;

    protected override void TriggerAction()
    {
        base.TriggerAction();
        _musicTrack = new FMOD.Studio.EventInstance();
        _musicTrack = RuntimeManager.CreateInstance(_pathPrefix + _path);
        _musicTrack.start();
    }

    private void OnTriggerExit(Collider other)
    {
        if (_canBeRetriggered && other.gameObject.CompareTag("Player"))
        {
            ResetTrigger();
        }
    }

    private void OnDisable()
    {
        _musicTrack.stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
    }
}
