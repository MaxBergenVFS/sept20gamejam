﻿using UnityEngine;

public class PlaySfxBehaviour : StateMachineBehaviour
{
    [SerializeField] private string _sfxPath = "";

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/" + _sfxPath);
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

    }

}
