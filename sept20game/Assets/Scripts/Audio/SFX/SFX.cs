﻿using UnityEngine;

public class SFX : MonoBehaviour
{
    protected string _SFXPrefix = "event:/SFX/";
    protected Transform _transform;

    protected virtual void Awake()
    {
        _transform = this.transform;
    }

    protected void PlaySound2D(string path) => FMODUnity.RuntimeManager.PlayOneShot(_SFXPrefix + path);
    protected void PlaySound3D(string path) => FMODUnity.RuntimeManager.PlayOneShot(_SFXPrefix + path, _transform.position);
    protected void StartInstance(FMOD.Studio.EventInstance instance) => instance.start();
    protected void StopInstance(FMOD.Studio.EventInstance instance) => instance.stop(FMOD.Studio.STOP_MODE.IMMEDIATE);

    protected void StartInstanceAttached(FMOD.Studio.EventInstance instance, Rigidbody rb)
    {
        FMODUnity.RuntimeManager.AttachInstanceToGameObject(instance, _transform, rb);
        instance.start();
    }


}
