﻿using UnityEngine;

public class DoorSFX : SFX
{
    private string _openDoorSFX = "Environment/Door/Open";
    private string _lockedDoorSFX = "Environment/Door/Locked";

    public void PlayDoorSound(bool isOpen)
    {
        if (isOpen) { PlaySound3D(_openDoorSFX); }
        else { PlaySound3D(_lockedDoorSFX); }
    }
}
