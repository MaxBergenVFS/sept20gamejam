﻿using UnityEngine;

public class ObjSFX : SFX
{
    [SerializeField]
    private string _breakSound = "Environment/Crate/Break";
    
    private string _leverSound = "Environment/Lever";
    private string _spikeTrapSound = "Environment/SpikeTrap";

    public void PlayBreakSound() { PlaySound3D(_breakSound); }
    public void PlayLeverSound() { PlaySound3D(_leverSound); }
    public void PlaySpikeTrapSound() { PlaySound3D(_spikeTrapSound); }
}
