﻿public class KeySFX : SFX
{
    private string _keyPickupSound = "Environment/Key/Pickup";

    public void PlayKeyPickupSound()
    {
        PlaySound2D(_keyPickupSound);
    }
}
