﻿using UnityEngine;

public class RandTriggeredSFX : MonoBehaviour
{
    [SerializeField]
    private float _minTimeBetweenSounds = 5f;
    [SerializeField]
    private float _maxTimeBetweenSounds = 20f;
    [SerializeField]
    private string _path;

    private float _timeBetweenSounds;
    private string _pathPrefix = "event:/SFX/";
    private Vector3 _pos;

    private void Awake()
    {
        _pos = transform.position;
    }

    private void OnEnable()
    {
        PlayRandSound();
    }

    private void PlayRandSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(_pathPrefix + _path, _pos);
        _timeBetweenSounds = Random.Range(_minTimeBetweenSounds, _maxTimeBetweenSounds);
    }

    private void Update()
    {
        _timeBetweenSounds -= Time.deltaTime;
        if (_timeBetweenSounds < 0) PlayRandSound();
    }
}
