﻿using UnityEngine;

public class LightningSFX : SFX
{

    // [Range(0.0f, 1.0f)]
    // public float _minVolume = 1f;
    // [Range(0.0f, 1.0f)]
    // public float _maxVolume = 1f;
    // [Range(-3.0f, 3.0f)]
    // public float _minPitch = 1f;
    // [Range(-3.0f, 3.0f)]
    // public float _maxPitch = 1f;

    private string _lightningSFX = "Ambient/Lightning";

    public void PlayLightningSound()
    {
        // RandPitch(_minPitch, _maxPitch);
        // RandVolume(_minVolume, _maxVolume);
        PlaySound2D(_lightningSFX);
    }
}
