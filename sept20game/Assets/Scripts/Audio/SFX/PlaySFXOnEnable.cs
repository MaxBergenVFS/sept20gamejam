﻿using UnityEngine;

public class PlaySFXOnEnable : SFX
{
    [SerializeField]
    private string _path;

    private void OnEnable()
    {
        PlaySound3D(_path);
    }
}
