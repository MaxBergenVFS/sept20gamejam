﻿using UnityEngine;

public class FootstepSFX : SFX
{
   
    [SerializeField]
    private ScreenShakeRangeTrigger _screenShakeRangeTrigger;
    [SerializeField]
    private bool _flyingEnemy = false;

    private string _footstepSound = "Enemy/Troll/Footstep";
    private ScreenShake _screenShakeRef;

    protected override void Awake()
    {
        base.Awake();
        if (_screenShakeRangeTrigger) _screenShakeRef = Camera.main.gameObject.GetComponent<ScreenShake>();
    }

    public void PlayFootstepSound()
    {
        if (_flyingEnemy) return;

        PlaySound3D(_footstepSound);

        if (_screenShakeRangeTrigger == null) return;
        
        if (_screenShakeRangeTrigger.CheckIfTriggered())
        {
            _screenShakeRef.enabled = true;
        }
    }

}
