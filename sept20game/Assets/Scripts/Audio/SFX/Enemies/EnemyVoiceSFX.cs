﻿using UnityEngine;

public class EnemyVoiceSFX : SFX
{
    private string _enemyPrefix = "";
    private string _patrol = "Patrol";
    private string _chase = "Chase";
    private string _attack = "Attack";
    private string _currentPath = "";
    private Rigidbody _rb;

    FMOD.Studio.EventInstance _attackInstance;
    FMOD.Studio.EventInstance _patrolInstance;
    FMOD.Studio.EventInstance _chaseInstance;

    protected override void Awake()
    {
        base.Awake();
        _rb = GetComponentInParent<Rigidbody>();
        _enemyPrefix = _SFXPrefix + "Enemy/Troll/";
        _attackInstance = FMODUnity.RuntimeManager.CreateInstance(_enemyPrefix + _attack);
        _patrolInstance = FMODUnity.RuntimeManager.CreateInstance(_enemyPrefix + _patrol);
        _chaseInstance = FMODUnity.RuntimeManager.CreateInstance(_enemyPrefix + _chase);
    }

    public void PlayEnemyVoiceIdle()
    {
        StopEnemyVoice();
        StartInstanceAttached(_patrolInstance, _rb);
    }

    public void PlayEnemyVoiceChasing()
    {
        StopEnemyVoice();
        StartInstanceAttached(_chaseInstance, _rb);
    }

    public void PlayEnemyVoiceAttacking()
    {
        StopEnemyVoice();
        StartInstanceAttached(_attackInstance, _rb);
    }

    public void StopEnemyVoice()
    {
        StopInstance(_patrolInstance);
        StopInstance(_chaseInstance);
        StopInstance(_attackInstance);
    }
}
