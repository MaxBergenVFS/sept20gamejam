﻿public class EnemySFX : SFX
{
    private string _painSound = "Enemy/Troll/Pain";
    private string _attackSound = "Enemy/Troll/Attack";

    public void PlayPainSound()
    {
        PlaySound3D(_painSound);
    }

    public void PlayAttackSound()
    {
        PlaySound3D(_attackSound);
    }

}
