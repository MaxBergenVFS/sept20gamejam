﻿using UnityEngine;

public class PlayerFootstepSFX : SFX
{
    [SerializeField]
    private float _timeBetweenFootsteps = 1f;
    [SerializeField]
    private LayerMask _loudNoiseLayers;
    [SerializeField]
    private float _loudNoiseRangeStanding = 10f;
    [SerializeField]
    private float _loudNoiseRangeCrouching = 2f;
    [SerializeField]
    private float _minimumTimeBetweenJumpSound = 0.05f;

    private float _speed = 0;
    private float _standingParameterValue = 1f;
    FMOD.Studio.EventInstance FootstepsEvent;
    private string _standingParameterName = "Standing";
    private string _bonesParameterName = "Bones";
    private string _waterParameterName = "Water";
    private string _footstepSound = "event:/SFX/Player/Footstep";
    private string _footstepForReverb = "Player/FootstepForReverb";
    private string _swimmingSound = "Player/Swimming";
    private string _jumpSoundUp = "Player/Jump/Up";
    private string _jumpSoundDown = "Player/Jump/Down";
    private string _jumpSoundBones = "Player/Jump/OnBones";
    private string _jumpSoundWater = "Player/Jump/InWater";
    private string _jumpSoundUnderwater = "Player/Jump/Down";
    private float _timeUntilNextFootstep;
    private float _timeUntilNextJumpSound;
    private bool _onBones = false;
    private bool _inWater = false;
    private bool _isStanding = true;
    private bool _isSwimming = false;
    private bool _dontPlaySFXOnEnable = true;
    private PlayerController _playerController;

    protected override void Awake()
    {
        base.Awake();
        _timeUntilNextFootstep = _timeBetweenFootsteps;
        _timeUntilNextJumpSound = 0f;
        _playerController = GetComponent<PlayerController>();
    }

    private void Start()
    {
        FootstepsEvent = FMODUnity.RuntimeManager.CreateInstance(_footstepSound);
        FootstepsEvent.setParameterByName(_standingParameterName, _standingParameterValue);
        SetBonesParameter(false);
        SetWaterParameter(false, false);
    }

    private void OnEnable()
    {
        if (_dontPlaySFXOnEnable) { _dontPlaySFXOnEnable = false; }
        else if (_timeUntilNextJumpSound <= 0f) { PlayJumpSoundDown(); }
    }

    private void Update()
    {
        _timeUntilNextJumpSound -= Time.deltaTime;
        _timeUntilNextFootstep -= Time.deltaTime * _speed;
        if (_timeUntilNextFootstep < 0f) PlayFootstepSound();
    }

    public void PlayJumpSoundUp() { PlaySound2D(_jumpSoundUp); }

    public void PlayJumpSoundDown()
    {
        if (_playerController.CheckIfUnderwater()) { PlaySound2D(_jumpSoundUnderwater); }
        else
        {
            if (!_isSwimming) PlaySound2D(_jumpSoundDown);
            if (_inWater) { PlaySound2D(_jumpSoundWater); }
            if (_onBones)
            {
                PlaySound2D(_jumpSoundBones);
                MakeLoudNoise();
            }
        }
        _timeUntilNextJumpSound = _minimumTimeBetweenJumpSound;
    }

    public void SetStandingParameter(bool cond)
    {
        float num;
        if (cond) { num = 1f; }
        else { num = 0f; }
        FootstepsEvent.setParameterByName(_standingParameterName, num);
        _isStanding = cond;
    }

    public void SetBonesParameter(bool cond)
    {
        if (cond) { FootstepsEvent.setParameterByName(_bonesParameterName, 1f); }
        else { FootstepsEvent.setParameterByName(_bonesParameterName, 0f); }
        _onBones = cond;
        //print($"_onBones: {_onBones}");
    }

    public void ResetJumpSoundTimer() { _timeUntilNextJumpSound = 0f; }

    private void SetWaterParameter(bool isInWater, bool isSwimming)
    {
        if (isInWater) { FootstepsEvent.setParameterByName(_waterParameterName, 1f); }
        else { FootstepsEvent.setParameterByName(_waterParameterName, 0f); }
        _inWater = isInWater;
        if (isInWater) { _isSwimming = isSwimming; }
        else { _isSwimming = false; }
        _playerController.SetIsSwimming(_isSwimming);
    }

    public void SetFootstepSpeed(float speed)
    {
        if (speed != _speed) _speed = speed;
    }

    private void PlayFootstepSound()
    {
        if (!_isSwimming)
        {
            FootstepsEvent.start();
            PlaySound3D(_footstepForReverb);
            if (_onBones) MakeLoudNoise();
        }
        else { PlaySound2D(_swimmingSound); }
        _timeUntilNextFootstep = _timeBetweenFootsteps;
    }

    private void MakeLoudNoise()
    {
        float range;
        if (_isStanding) { range = _loudNoiseRangeStanding; }
        else { range = _loudNoiseRangeCrouching; }
        Vector3 origin = _transform.position;
        Collider[] hitObj = Physics.OverlapSphere(origin, range, _loudNoiseLayers);
        foreach (Collider obj in hitObj)
        {
            if (obj.GetComponent<IDistractable>() != null) obj.GetComponent<IDistractable>().Distract(origin);
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.GetComponent<WaterSFXVolume>())
        {
            WaterSFXVolume vol = other.GetComponent<WaterSFXVolume>();
            if (!_inWater) SetWaterParameter(true, vol.CheckIfSwimming());
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<WaterSFXVolume>())
        {
            SetWaterParameter(false, false);
        }
    }

    public void DontPlaySFXOnEnable() { _dontPlaySFXOnEnable = true; }

    public float GetTimeBetweenFootsteps() { return _timeBetweenFootsteps; }
}
