﻿using UnityEngine;

public class WeaponSFX : SFX
{
    private string _playerWeaponPrefix = "Player/Weapon/";
    private string _boneSwingSound = "Bone/Swing";
    private string _swordSwingSound = "Sword/Swing";
    private string _weaponHitDmgSound = "Sword/HitAndDmg";
    private string _weaponHitFleshDmgSound = "Sword/HitFleshAndDmg";
    private string _weaponHitNoDmgSound = "Sword/HitAndNoDmg";
    private string _shootCrossbowSound = "Crossbow/Shoot";
    private string _noAmmoCrossbowSound = "Crossbow/NoAmmo";
    private string _weaponSwingSound = "";

    public void SetSwordSwingSound(bool isBreakable)
    {
        if (isBreakable) { _weaponSwingSound = _boneSwingSound; }
        else { _weaponSwingSound = _swordSwingSound; }
    }

    public void PlayWeaponSwingSound()
    {
        if (_weaponSwingSound == "")
        {
            Debug.Log("_weaponSwingSound has not been set");
            return;
        }
        PlaySound3D(_playerWeaponPrefix + _weaponSwingSound);
    }

    public void PlayWeaponHitDmgSound(bool isHittingFlesh)
    {
        if (isHittingFlesh) { PlaySound2D(_playerWeaponPrefix + _weaponHitFleshDmgSound); }
        else { PlaySound2D(_playerWeaponPrefix + _weaponHitDmgSound); }
    }

    public void PlayWeaponHitNoDmgSound() => PlaySound3D(_playerWeaponPrefix + _weaponHitNoDmgSound);
    public void PlayShootCrossbowSound() => PlaySound2D(_playerWeaponPrefix + _shootCrossbowSound);
    public void PlayNoAmmoCrossbowSound() => PlaySound2D(_playerWeaponPrefix + _noAmmoCrossbowSound);
   
}
