﻿public class PlayerSFX : SFX
{
    private string _playerPrefix = "Player/";
    private string _outOfStamBreathingSound = "OutOfBreath";
    private string _deathSound = "Death";
    private string _painSound = "Pain";
    private string _interactSoundPos = "Interact/Positive";
    private string _interactSoundNeg = "Interact/Negative";

    public void PlayOutOfStamBreathingSound()
    {
        PlaySound2D(_playerPrefix + _outOfStamBreathingSound);
    }

    public void PlayDeathSound()
    {
        PlaySound2D(_playerPrefix + _deathSound);
    }

    public void PlayPainSound()
    {
        PlaySound2D(_playerPrefix + _painSound);
    }

    public void PlayInteractSoundPos()
    {
        PlaySound2D(_playerPrefix + _interactSoundPos);
    }

    public void PlayInteractSoundNeg()
    {
        PlaySound2D(_playerPrefix + _interactSoundNeg);
    }

}
