﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager instance { get { return _instance; } }
    private static GameManager _instance;

    [HideInInspector]
    public int currentMaxHealthAmt = 100, tempMaxHealthAmt = 0, currentMaxReagentAmt = 5,
        sceneIndexToTeleportTo = 0, tempMaxReagentAmt = 5;
    [HideInInspector]
    public bool playerHasSword = false, playerHasCrossbow = false,
        hasChangedScene = false, playerHasReadDiary = false, playerIsDead = false;
    
    public bool currentlyInSaveRoom = false;

    private Dictionary<int, int> _inventoryDatabaseAmtState = new Dictionary<int, int>();
    private int _currentSceneIndex, _previousSceneIndex, _cutsceneStep = 0, _currentHealthAmt = 100;
    private bool _loadData = false;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        _currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
    }

    public void UpdateStats()
    {
        if (PlayerController.instance != null)
        {
            PlayerController playerController = PlayerController.instance;
            PlayerHealth playerHealth = playerController.GetComponent<PlayerHealth>();
            PlayerInventory playerInventory = playerController.GetComponent<PlayerInventory>();
            playerInventory.inventoryDatabase.SaveInventoryDatabaseAmtList(_inventoryDatabaseAmtState);
            _currentHealthAmt = playerHealth.GetCurrentHealth();
            currentMaxHealthAmt = tempMaxHealthAmt;
            currentMaxReagentAmt = tempMaxReagentAmt;
        }
    }

    public Dictionary<int, int> GetInventoryDatabaseAmtState() { return _inventoryDatabaseAmtState; }

    public bool CheckIfHasPlayedCutscene(int num)
    {
        if (_cutsceneStep > num) return true;
        return false;
    }

    public void SetLoadData() { _loadData = true; }
    public bool GetLoadData()
    {
        bool cond;
        cond = _loadData;
        if (_loadData) _loadData = false;
        return cond;
    }

    public int GetCurrentHealthAmt() { return _currentHealthAmt; }
    public void SetCurrentHealthAmt(int amt) { _currentHealthAmt = amt; }

    public void CheckIfHasChangedScene()
    {
        _previousSceneIndex = _currentSceneIndex;
        hasChangedScene = (_currentSceneIndex != SceneManager.GetActiveScene().buildIndex);
        _currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
    }

    public void IncrementCutsceneStep() { _cutsceneStep++; }
    public int GetPreviousSceneIndex() { print($"_previousSceneIndex: {_previousSceneIndex}"); return _previousSceneIndex; }
    public void OverwritePreviousSceneIndex(int num) { _previousSceneIndex = num; }

    public void GameOver(bool cond)
    {
        playerIsDead = cond;
        Cursor.visible = cond;
        if (cond)
        {
            Time.timeScale = 0;
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            Time.timeScale = 1;
            Cursor.lockState = CursorLockMode.Locked;
        }
    }
}
