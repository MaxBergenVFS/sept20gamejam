﻿using FMODUnity;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    [SerializeField]
    string[] _musicPath;
    [SerializeField]
    bool _initMusicLoop = false;
    [SerializeField]
    int _loopToInit = 0;

    private int _currentParamIndex = 0;
    private string _pathPrefix = "event:/Music/Loops/";
    private FMOD.Studio.EventInstance[] _musicLoops;
    private static MusicManager _instance;
    private int _currentLoop = 0;

    protected void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        _musicLoops = new FMOD.Studio.EventInstance[_musicPath.Length];

        for (int i = 0; i < _musicPath.Length; i++)
        {
            _musicLoops[i] = RuntimeManager.CreateInstance(_pathPrefix + _musicPath[i]);
        }
    }

    private void Start()
    {
        if (_initMusicLoop) { SetMusicLoop(_loopToInit); }
    }

    public void SetMusicLoop(int address)
    {
        _musicLoops[_currentLoop].stop(FMOD.Studio.STOP_MODE.ALLOWFADEOUT);
        _musicLoops[address] = RuntimeManager.CreateInstance(_pathPrefix + _musicPath[address]);
        _musicLoops[address].start();
        _currentLoop = address;
    }

    public void IncrementParamIndex() => _currentParamIndex++;
    public int GetCurrentParamIndex() { return _currentParamIndex; }
    public FMOD.Studio.EventInstance GetCurrentEventInstance() { return _musicLoops[_currentLoop]; }

}
