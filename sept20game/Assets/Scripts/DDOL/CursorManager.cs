﻿using UnityEngine;

public class CursorManager : MonoBehaviour
{
    public Texture2D cursorTexture;
    public CursorMode cursorMode = CursorMode.Auto;
    public Vector2 hotSpot = Vector2.zero;

    private static CursorManager _instance;

    private void Awake()
    {

        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }

        Cursor.SetCursor(cursorTexture, hotSpot, cursorMode);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void StartSceneWithCursor(bool cond)
    {
        Cursor.visible = cond;
        
        if (cond)
        {
            Cursor.lockState = CursorLockMode.None;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

}
