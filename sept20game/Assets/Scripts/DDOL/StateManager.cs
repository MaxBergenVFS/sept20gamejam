﻿using System.Collections.Generic;
using UnityEngine;

public class StateManager : MonoBehaviour
{
    [HideInInspector]
    public Dictionary<int, Dictionary<int, bool>> door_isUnlocked = new Dictionary<int, Dictionary<int, bool>>();
    [HideInInspector]
    public Dictionary<int, Dictionary<int, bool>> key_hasBeenPickedUp = new Dictionary<int, Dictionary<int, bool>>();
    [HideInInspector]
    public Dictionary<int, Dictionary<int, bool>> key_hasBeenUsed = new Dictionary<int, Dictionary<int, bool>>();
    [HideInInspector]
    public Dictionary<int, Dictionary<int, bool>> secret_hasBeenFound = new Dictionary<int, Dictionary<int, bool>>();
    [HideInInspector]
    public Dictionary<int, Dictionary<int, bool>> soul_hasBeenFreed = new Dictionary<int, Dictionary<int, bool>>();
    [HideInInspector]
    public Dictionary<int, Dictionary<int, bool>> gold_hasBeenPickedUp = new Dictionary<int, Dictionary<int, bool>>();
    [HideInInspector]
    public Dictionary<int, Dictionary<int, bool>> plant_hasBeenPicked = new Dictionary<int, Dictionary<int, bool>>();
    [HideInInspector]
    public Dictionary<int, Dictionary<int, bool>> orbSlot_hasBeenUnlocked = new Dictionary<int, Dictionary<int, bool>>();
    [HideInInspector]
    public Dictionary<int, Dictionary<int, bool>> backpack_hasBeenPickedUp = new Dictionary<int, Dictionary<int, bool>>();

    public static StateManager instance { get { return _instance; } }
    private static StateManager _instance;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
    }

    private int GetNumOfObjFoundInLvl(Dictionary<int, Dictionary<int, bool>> objDict, int sceneIndex)
    {
        int numOfObjFound = 0;
        foreach (var obj in objDict[sceneIndex])
        {
            if (obj.Value == true) numOfObjFound++;
        }
        return numOfObjFound;
    }

    public int GetNumOfSoulsFreedInLevel(int sceneIndex)
    {
        return GetNumOfObjFoundInLvl(soul_hasBeenFreed, sceneIndex);
    }

    public List<int> GetNumOfGoldFoundInLevel(int sceneIndex)
    {
        List<int> indexesOfFoundGold = new List<int>();
        foreach (var obj in gold_hasBeenPickedUp[sceneIndex])
        {
            if (obj.Value == true && !indexesOfFoundGold.Contains(obj.Key))
            {
                indexesOfFoundGold.Add(obj.Key);
            }
        }
        return indexesOfFoundGold;
    }

    public int GetNumOfSecretsFoundInLevel(int sceneIndex)
    {
        return GetNumOfObjFoundInLvl(secret_hasBeenFound, sceneIndex);
    }
}
