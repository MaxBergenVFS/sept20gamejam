﻿using UnityEngine;

public class SkyboxManager : MonoBehaviour
{
    [SerializeField]
    private float _rotationSpeed = 1f;
    [SerializeField]
    private Material _skyboxDefault;
    [SerializeField]
    private Material[] _skyboxVariants;
    [SerializeField]
    private float _minTimeBetweenLightning = 1f;
    [SerializeField]
    private float _maxTimeBetweenLightning = 10f;
    [SerializeField]
    private float _lightningDuration = 0.5f;

    public bool useLightningEffect = true;

    private float _currentLightningDuration;
    private float _timeBetweenLightning = 0f;
    private bool _lightningIsActive = false;
    private int _skyboxVarNum;
    private LightningSFX _lightningSFX;

    private void Awake()
    {
        RenderSettings.skybox = _skyboxDefault;
        _timeBetweenLightning = 0f;
        _skyboxVarNum = Random.Range(0, _skyboxVariants.Length);
        _currentLightningDuration = _lightningDuration;
        _lightningSFX = GetComponent<LightningSFX>();
    }

    private void OnEnable() => RenderSettings.skybox = _skyboxDefault;

    private void Update()
    {
        if (useLightningEffect)
        {
            if (_timeBetweenLightning > 0f)
            {
                _timeBetweenLightning -= Time.deltaTime;
            }
            else
            {
                _skyboxVarNum = Random.Range(0, _skyboxVariants.Length);
                RenderSettings.skybox = _skyboxVariants[_skyboxVarNum];
                _timeBetweenLightning = Random.Range(_minTimeBetweenLightning, _maxTimeBetweenLightning);
                _lightningIsActive = true;
                _currentLightningDuration = _lightningDuration;
                _lightningSFX.PlayLightningSound();
            }

            if (_lightningIsActive)
            {
                _currentLightningDuration -= Time.deltaTime;
            }

            if (_currentLightningDuration <= 0f)
            {
                RenderSettings.skybox = _skyboxDefault;
                _lightningIsActive = false;
            }
        }

        RenderSettings.skybox.SetFloat("_Rotation", Time.time * _rotationSpeed);
    }
}
