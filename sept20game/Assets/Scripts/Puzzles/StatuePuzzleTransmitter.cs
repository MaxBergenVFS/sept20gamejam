﻿using UnityEngine;

public class StatuePuzzleTransmitter : PuzzleTransmitter, IInteractable
{
    [SerializeField]
    private int _unlockedIndex;
    [SerializeField]
    private DialogueTablet _dialogueTablet;
    [SerializeField]
    private string _textIfTabletIncompleteAndPlayerHasFragment = "I should fix the tablet first";
    [SerializeField]
    private string _textIfTabletIncompleteAndPlayerDoesNotHaveFragment = "I shouldn't mess with this until I know what I'm doing";

    private InspectionUI _inspectionUI;
    private int _currentIndex;
    private float _rotationDegrees = 90f;
    private int _numOfStates = 4;
    private Vector3 _currentEulerAngles;

    private void Awake()
    {
        _currentIndex = 0;
        _currentEulerAngles.x = transform.eulerAngles.x;
        _currentEulerAngles.y = transform.eulerAngles.y;
        _currentEulerAngles.z = transform.eulerAngles.z;
    }

    private void Start()
    {
        if (_currentIndex == _unlockedIndex) SetUnlocked(true);
        _inspectionUI = InspectionUI.instance;
    }

    public void Interact()
    {
        if (_stateIsFrozen) return;
        if (_dialogueTablet.CheckIfTabletIsComplete())
        {
            if (_currentIndex < _numOfStates - 1) { _currentIndex++; }
            else { _currentIndex = 0; }
            RotateOnYAxis(_rotationDegrees);
            if (_currentIndex == _unlockedIndex) { SetUnlocked(true); }
            else { SetUnlocked(false); }
        }
        else if (_dialogueTablet.CheckIfPlayerHasRequiredItem())
        {
            //Tablet is not complete but player has fragment in inventory
            _inspectionUI.DisplayTxt(_textIfTabletIncompleteAndPlayerHasFragment);
        }
        else
        {
            //Tablet is not complete NOR does player have fragment in inventory
            _inspectionUI.DisplayTxt(_textIfTabletIncompleteAndPlayerDoesNotHaveFragment);
        }
    }

    private void RotateOnYAxis(float degrees)
    {
        _currentEulerAngles.z += degrees;
        transform.eulerAngles = _currentEulerAngles;
    }

    protected override void SetUnlocked(bool cond)
    {
        base.SetUnlocked(cond);
        _puzzleReceiver.AttemptUnlock();
    }
}
