﻿using System.Collections.Generic;
using UnityEngine;

public class PuzzleReceiver : MonoBehaviour
{
    [SerializeField]
    private PuzzleTransmitter[] _puzzleTransmitters;
    [SerializeField]
    private bool _freezeStateOnceUnlocked = true;
    
    private bool _isUnlocked = false;

    public void AttemptUnlock()
    {
        if (_isUnlocked && _freezeStateOnceUnlocked) return;

        List<bool> unlockedList = new List<bool>();
        foreach (PuzzleTransmitter puzzleTransmitter in _puzzleTransmitters)
        {
            unlockedList.Add(puzzleTransmitter.CheckIfUnlocked());
        }

        if (!unlockedList.Contains(false)) { Unlock(); }
        else if (!_freezeStateOnceUnlocked) { ResetUnlock(); }
    }

    protected virtual void Unlock()
    {
        _isUnlocked = true;
        if (_freezeStateOnceUnlocked)
        {
            foreach (PuzzleTransmitter puzzleTransmitter in _puzzleTransmitters)
            {
                puzzleTransmitter.SetStateFrozen();
            }
        }
    }

    protected virtual void ResetUnlock()
    {
        _isUnlocked = false;
    }
}
