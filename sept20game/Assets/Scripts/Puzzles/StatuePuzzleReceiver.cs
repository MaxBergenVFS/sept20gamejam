﻿using UnityEngine;

public class StatuePuzzleReceiver : PuzzleReceiver
{
    private Animator _anim;
    private EnableOrDisableObjs _objsToEnable;

    private void Awake()
    {
        _anim = GetComponent<Animator>();
        _objsToEnable = GetComponent<EnableOrDisableObjs>();
    }
    
    protected override void Unlock()
    {
        base.Unlock();
        _anim.SetTrigger("Unlock");
        _objsToEnable.SwapObjs();
    }

    protected override void ResetUnlock()
    {
        base.ResetUnlock();
        _anim.SetTrigger("Lock");
    }
}
