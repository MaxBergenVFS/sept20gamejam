﻿using UnityEngine;

public class TotemManager : MonoBehaviour
{
    [SerializeField] private Totem[] _totems;
    [SerializeField] private GameObject _objToActivate;

    public void AttemptToActivate()
    {
        bool canActivate = true;
        foreach (var totem in _totems)
        {
            if(!totem.CheckIfDestroyed()) canActivate = false;
        }
        if (canActivate) Activate();
    }

    private void Activate()
    {
        _objToActivate.SetActive(true);
    }
}
