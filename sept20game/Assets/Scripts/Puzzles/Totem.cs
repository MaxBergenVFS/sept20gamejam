﻿using UnityEngine;

public class Totem : MonoBehaviour
{
    [SerializeField] private GameObject[] _destroyedTotems;
    [SerializeField] private GameObject _intactTotemReflection;
    [SerializeField] private TotemTrigger _totemTrigger;
    [SerializeField] private TotemManager _totemManager;

    private bool _isDestroyed = false;

    public void DestroyTotem()
    {
        if (_isDestroyed) return;

        foreach (var destroyedTotem in _destroyedTotems)
        {
            destroyedTotem.SetActive(true);
        }
        _intactTotemReflection.SetActive(false);
        _totemTrigger.gameObject.SetActive(false);
        _isDestroyed = true;
        _totemManager.AttemptToActivate();
    }

    public bool CheckIfDestroyed() { return _isDestroyed; }
}
