﻿using UnityEngine;

public class PressurePlatePuzzleReceiver : PuzzleReceiver
{
    [SerializeField]
    private Material _unlockedMat;
    [SerializeField]
    private Material _lockedMat;

    private Renderer _rend;

    private void Awake()
    {
        _rend = GetComponent<Renderer>();
        _rend.material = _lockedMat;
    }

    protected override void Unlock()
    {
        base.Unlock();
        _rend.material = _unlockedMat;
    }

    protected override void ResetUnlock()
    {
        base.ResetUnlock();
        _rend.material = _lockedMat;
    }
}
