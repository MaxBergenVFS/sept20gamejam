﻿using UnityEngine;

public class TotemTrigger : MonoBehaviour, IDamageable
{
    private bool _isDestroyed = false;

    [SerializeField] private Totem parentTotem;

    public void TakeDamage(int dmg, bool isPlayer)
    {
        if (dmg <= 0) return;
        parentTotem.DestroyTotem();
    }

    public bool CheckIfDead() { return _isDestroyed; }
    public bool CompareDefenseLevel(int lvl) { return true; }
    public bool CheckIfFlesh() { return false; }
}
