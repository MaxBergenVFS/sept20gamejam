﻿using UnityEngine;

public class PressurePlatePuzzleTransmitter : PuzzleTransmitter
{
    [SerializeField]
    private float _massThreshold = 1f;
    [SerializeField]
    private Material _materialWhenInactive;
    [SerializeField]
    private Material _materialWhenActive;

    private Renderer _rend;
    private float _currentMass;

    private void Awake()
    {
        _rend = GetComponent<Renderer>();
        _rend.material = _materialWhenInactive;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<Rigidbody>())
        {
            Rigidbody rb = other.GetComponent<Rigidbody>();
            _currentMass += rb.mass;
            if (_currentMass >= _massThreshold) SetUnlocked(true);
        }
        // if (other.CompareTag("Player"))
        // {
        //     _currentMass += _massThreshold;
        //     if (_currentMass >= _massThreshold) SetUnlocked(true);
        // }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<Rigidbody>())
        {
            Rigidbody rb = other.GetComponent<Rigidbody>();
            _currentMass -= rb.mass;
            if (_currentMass < _massThreshold) SetUnlocked(false);
        }
        // if (other.CompareTag("Player"))
        // {
        //     _currentMass -= _massThreshold;
        //     if (_currentMass < _massThreshold) SetUnlocked(false);
        // }
    }

    protected override void SetUnlocked(bool cond)
    {
        base.SetUnlocked(cond);
        if (cond) { _rend.material = _materialWhenActive; }
        else { _rend.material = _materialWhenInactive; }
        _puzzleReceiver.AttemptUnlock();
    }
}
