﻿using UnityEngine;

public class PuzzleTransmitter : MonoBehaviour
{
    [SerializeField]
    protected PuzzleReceiver _puzzleReceiver;

    private bool _isUnlocked = false;
    protected bool _stateIsFrozen = false;

    protected virtual void SetUnlocked(bool cond) { _isUnlocked = cond; }

    public bool CheckIfUnlocked() { return _isUnlocked; }

    public void SetStateFrozen() { _stateIsFrozen = true; }
}
