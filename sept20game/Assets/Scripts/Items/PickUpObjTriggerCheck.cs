﻿using UnityEngine;

public class PickUpObjTriggerCheck : MonoBehaviour
{
    private PickUpInteract _pickupInteract;

    private void Awake()
    {
        _pickupInteract = GetComponentInParent<PickUpInteract>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<Rigidbody>() == null && other.gameObject.GetComponent<Renderer>())
        {
            //print($"colliding with {other.name}");
            _pickupInteract.DropItem();
        }
    }
}
