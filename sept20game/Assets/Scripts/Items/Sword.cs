﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(WeaponSFX))]
public class Sword : MonoBehaviour
{
    [SerializeField]
    private float _timeBeforePauseSwing = 0.2f;

    private Animator _anim;
    private PauseAnimation _pauseAnim;
    private WeaponSFX _swordSFX;
    private int _randomAttack;
    private HeldWeapon _currentlyHeldWeapon;
    private GameObject _currentUIElement;

    private void Awake()
    {
        _anim = GetComponent<Animator>();
        _swordSFX = GetComponent<WeaponSFX>();
        _pauseAnim = GetComponent<PauseAnimation>();
    }

    public void SwingSword(bool swingIsCharged)
    {
        if (swingIsCharged) { _pauseAnim.UnPauseAnim(); }
        else { InitSwing(); }
        _swordSFX.PlayWeaponSwingSound();
    }

    public void ChargeSwing()
    {
        InitSwing();
        StartCoroutine(PauseSwingAnim());
    }

    private void InitSwing()
    {
        _randomAttack = Random.Range(1, 3);
        _anim?.SetInteger("RandomAttack", _randomAttack);
        _anim?.SetTrigger("Attack");
    }

    public void Block() => _anim?.SetTrigger("Block");

    public void PlayBlockSFX() => _swordSFX.PlayWeaponHitNoDmgSound();

    public void SetCurrentHeldWeapon(HeldWeapon obj, GameObject UIRef)
    {
        _currentlyHeldWeapon = obj;
        _currentUIElement = UIRef;
    }

    public void EnableSword()
    {
        _currentUIElement.SetActive(true);
        _currentlyHeldWeapon.gameObject.SetActive(true);
    }

    public void HideSword(bool cond) { _currentlyHeldWeapon.gameObject.SetActive(!cond); }

    public bool CheckIfIsBreakable() { return _currentlyHeldWeapon.CheckIfIsBreakable(); }

    public void PlayWeaponHitSound(bool canDmg, bool isHittingFlesh)
    {
        if (canDmg) { _swordSFX.PlayWeaponHitDmgSound(isHittingFlesh); }
        else { _swordSFX.PlayWeaponHitNoDmgSound(); }
    }

    private IEnumerator PauseSwingAnim()
    {
        yield return new WaitForSeconds(_timeBeforePauseSwing);
        _pauseAnim.PauseAnim();
    }

}
