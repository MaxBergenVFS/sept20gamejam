﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OrbDoor : BaseItem, IInteractable
{
    [SerializeField] private OrbSlot[] _orbSlots;
    [SerializeField] private float _timeBeforeForceLoad = 5f;
    [SerializeField] private int _sceneToChangeTo;

    private List<bool> _doorsUnlocked = new List<bool>();
    private string _SFXPath = "event:/SFX/Interactables/RockCrumble";
    private LevelChangeFade _levelChangeFade;
    private Animator _levelChangeFadeAnim;
    private GameObject _levelChangeFadeObj;
    private InputManager _playerInput;

    private void Start()
    {
        RebuildLockedDoorsList();
        _levelChangeFadeObj = PlayerController.instance.GetComponentInChildren<LevelChangeFade>().gameObject;
        _levelChangeFade = _levelChangeFadeObj.GetComponent<LevelChangeFade>();
        _levelChangeFadeAnim = _levelChangeFade.GetComponent<Animator>();
        _playerInput = PlayerController.instance.GetComponent<InputManager>();
    }

    public void Interact()
    {
        foreach (OrbSlot orbSlot in _orbSlots)
        {
            orbSlot.Interact();
        }

        RebuildLockedDoorsList();

        if (_doorsUnlocked.Contains(false))
        {
            DisplayInspectText(false);
        }
        else
        {
            DisplayInspectText(true);
            FMODUnity.RuntimeManager.PlayOneShot(_SFXPath, transform.position);
            _playerInput.enabled = false;
            _levelChangeFade.OverrideLevelIndex(_sceneToChangeTo);
            _levelChangeFadeAnim.SetTrigger("FadeOut");
            StopAllCoroutines();
            StartCoroutine(ForceLevelLoad());
        }
    }

    private void RebuildLockedDoorsList()
    {
        _doorsUnlocked.Clear();
        foreach (OrbSlot orbSlot in _orbSlots)
        {
            _doorsUnlocked.Add(orbSlot.CheckIfUnlocked());
        }
    }

    IEnumerator ForceLevelLoad()
    {
        yield return new WaitForSeconds(_timeBeforeForceLoad);
        SceneManager.LoadScene(_sceneToChangeTo);
    }
}
