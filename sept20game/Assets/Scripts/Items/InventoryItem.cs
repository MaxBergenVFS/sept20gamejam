﻿using UnityEngine;

[CreateAssetMenu(fileName = "InventoryItem", menuName = "ScriptableObjects/InventoryItem", order = 1)]
public class InventoryItem : ScriptableObject
{
    public int id;
    public bool unique;
    public bool canUseInInventory;
    public int amount;
    public string title;
    public string description;
    public Sprite image;
    public GameObject model;

    public InventoryItem(int id, string title, string description, bool canUseInInventory, bool unique, int amount)
    {
        this.id = id;
        this.unique = unique;
        this.title = title;
        if (unique) amount = Mathf.Clamp(amount, 0, 2);
        this.amount = amount;
        this.description = description;
        this.image = Resources.Load<Sprite>("Sprites/InventoryItems/" + title);
        this.model = Resources.Load<GameObject>("Models/InventoryItems/" + title);
        this.canUseInInventory = canUseInInventory;
    }

    public InventoryItem(InventoryItem inventoryItem)
    {
        this.id = inventoryItem.id;
        this.unique = inventoryItem.unique;
        int amount = inventoryItem.amount;
        if (unique) amount = Mathf.Clamp(amount, 0, 2);
        this.amount = amount;
        this.title = inventoryItem.title;
        this.description = inventoryItem.description;
        this.image = Resources.Load<Sprite>("Sprites/InventoryItems/" + inventoryItem.title);
        this.model = Resources.Load<GameObject>("Models/InventoryItems/" + inventoryItem.title);
        this.canUseInInventory = inventoryItem.canUseInInventory;
    }
}