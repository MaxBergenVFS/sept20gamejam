﻿using UnityEngine;

public class PuzzleHintTablet : BaseItem, IInteractable
{
    [SerializeField]
    private int _databaseID = 17;

    private InventoryItemDatabase _inventoryDatabase;

    private void Start()
    {
        _inventoryDatabase = PlayerController.instance.GetComponent<PlayerInventory>().inventoryDatabase;
    }

    public void Interact()
    {
        StartLerp();
        DisplayNotifText();
        _inventoryDatabase.SetInventoryItemAmt(_databaseID, 1);
    }
}
