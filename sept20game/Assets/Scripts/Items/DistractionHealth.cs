﻿using UnityEngine;

public class DistractionHealth : Health, IDamageable, IExplodeable
{
    private bool _willExplode = false;

    public void SetWillExplode()
    {
        _willExplode = true;
    }

    public bool CheckIfFlesh() { return true; }

    public override void TakeDamage(int amt, bool player)
    {
        base.TakeDamage(amt, player);
        if (CheckIfDead())
        {
            if (_willExplode)
            {
                if (GetComponentInChildren<MagicBombItem>())
                {
                    MagicBombItem bomb = GetComponentInChildren<MagicBombItem>();
                    bomb.Explode();
                }
                else { Debug.LogError($"MagicBombItem not found on {this.gameObject.name}"); }
            }
            else { this.gameObject.SetActive(false); }

            Destroy(this.gameObject, 5f);
        }
        
    }
}
