﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Key : BaseItem, IInteractable, ISaveable
{
    [SerializeField]
    protected int _keyNum = 0;

    protected PlayerInventory _playerInventory;
    private KeySFX _keySFX;
    private MusicBehaviourTrigger _musicBehaviourTrigger;
    private EnableOrDisableObjs _objToEnableOrDisable;
    private Collider _col;
    private Renderer _rend;
    protected int _currentSceneIndex;

    protected override void Awake()
    {
        base.Awake();
        _currentSceneIndex = _currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        _keySFX = GetComponent<KeySFX>();
        _col = GetComponent<Collider>();
        _rend = GetComponentInChildren<Renderer>();
        if (GetComponent<MusicBehaviourTrigger>())
            _musicBehaviourTrigger = GetComponent<MusicBehaviourTrigger>();
        if (GetComponent<EnableOrDisableObjs>())
            _objToEnableOrDisable = GetComponent<EnableOrDisableObjs>();
    }

    private void Start()
    {
        _playerInventory = PlayerController.instance.GetComponent<PlayerInventory>();
    }

    public void CheckIfKeyHasBeenPickedUp()
    {
        if (StateManager.instance.key_hasBeenPickedUp[_currentSceneIndex][_keyNum]) this.gameObject.SetActive(false);
    }

    protected override void Destroy()
    {
        _col.enabled = false;
        _rend.enabled = false;
        this.gameObject.SetActive(false);
    }

    public void Interact()
    {
        StartLerp();
        StateManager.instance.key_hasBeenPickedUp[_currentSceneIndex][_keyNum] = true;
        PickUpKey();
    }

    protected virtual void PickUpKey()
    {
        _playerInventory.SetKey(true, _keyNum);
        _keySFX.PlayKeyPickupSound();
        if (_musicBehaviourTrigger) _musicBehaviourTrigger.TriggerAction();
        if (_objToEnableOrDisable) _objToEnableOrDisable.SwapObjs();
    }

    public int GetIDNum() { return _keyNum; }
    public bool IsInDefaultState() { return !this.gameObject.activeSelf; }
    public void UpdateState(bool state) { this.gameObject.SetActive(state); }
}
