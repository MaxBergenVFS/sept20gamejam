﻿using System.Collections;
using UnityEngine;

public class DestructibleFragment : MonoBehaviour
{
    private Rigidbody _rb;
    private Collider _col;
    private Material _mat;
    private Color _defaultColor;
    private float _secondsBeforeDisable = 0.5f;
    private float _secondsRemaining;
    private float _fadeOutTime = 1f;
    private bool _fadingOut = false;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
        _col = GetComponent<Collider>();
        _mat = GetComponent<Renderer>().material;
        _defaultColor = _mat.color;
        _secondsRemaining = _secondsBeforeDisable;
    }

    private void Update()
    {
        if (_fadingOut) return;
        _secondsRemaining -= Time.deltaTime;
        if (_secondsRemaining < 0)
        {
            StartCoroutine(FadeOutMaterial());
            _fadingOut = true;
        }
    }

    IEnumerator FadeOutMaterial()
    {
        float timeElapsed = 0;
        float alpha;
        Color color;

        while (timeElapsed < _fadeOutTime)
        {
            alpha = Mathf.Lerp(1f, 0f, timeElapsed / _fadeOutTime);
            timeElapsed += Time.deltaTime;
            color = new Color(_defaultColor.r, _defaultColor.g, _defaultColor.b, alpha);
            _mat.SetColor("_Color", color);
            yield return null;
        }
        // _rb.isKinematic = true;
        // _rb.useGravity = _col.enabled = false;
        this.gameObject.SetActive(false);
    }
}
