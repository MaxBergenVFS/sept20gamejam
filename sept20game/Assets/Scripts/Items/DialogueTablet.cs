﻿using UnityEngine;

public class DialogueTablet : DialogueTrigger, IInteractable
{
    [SerializeField]
    private string _posInteractTxt;
    [SerializeField]
    private string _negInteractTxt;
    [SerializeField]
    private int _databaseIDToCheck = 17;
    [SerializeField]
    private GameObject _tabletFragment;
    
    private bool _tabletIsComplete = false;
    private bool _displayingDialogue = false;
    private InventoryItemDatabase _inventoryDatabase;
    private InspectionUI _inspectionUI;

    protected override void Start()
    {
        base.Start();
        if (_tabletFragment) { _tabletIsComplete = false; }
        else { _tabletIsComplete = true; }
        _inventoryDatabase = PlayerController.instance.GetComponent<PlayerInventory>().inventoryDatabase;
        _inspectionUI = InspectionUI.instance;
    }

    public bool CheckIfPlayerHasRequiredItem()
    {
        if (_inventoryDatabase.GetInventoryItemAmt(_databaseIDToCheck) > 0) return true;
        return false;
    }

    public bool CheckIfTabletIsComplete() { return _tabletIsComplete; }

    public void Interact()
    {
        if (_tabletIsComplete && !_displayingDialogue)
        {
            TriggerDialogue();
            _displayingDialogue = true;
        }
        else if (_tabletIsComplete && _displayingDialogue)
        {
            _displayingDialogue = false;
        }
        else if (_inventoryDatabase.GetInventoryItemAmt(_databaseIDToCheck) > 0)
        {
            _inventoryDatabase.AddToInventoryItemAmt(_databaseIDToCheck, -1);
            _tabletFragment.SetActive(true);
            _inspectionUI.DisplayTxt(_posInteractTxt);
            _tabletIsComplete = true;
        }
        else
        {
            _inspectionUI.DisplayTxt(_negInteractTxt);
        }
    }
}
