﻿using UnityEngine;

public class ConsumeRange : MonoBehaviour
{
    private DestroyThis _destroyThis;

    private void Awake()
    {
        _destroyThis = GetComponentInParent<DestroyThis>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<IDistractable>() != null)
        {
            _destroyThis.Destroy();
        }
    }
}
