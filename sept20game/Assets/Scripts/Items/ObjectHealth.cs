﻿using UnityEngine;

public class ObjectHealth : Health, IDamageable, IExplodeable
{
    [SerializeField]
    private GameObject _destructionFragments;
    [SerializeField]
    private Transform _itemSpawnPos;
    [SerializeField]
    private GameObject _itemDrop;
    [SerializeField]
    private GameObject[] _prefabsToDrop;
    [SerializeField]
    private float _dropChance = 0.5f;
    [SerializeField]
    private GameObject _objToEnableOn0Health;
    [SerializeField]
    private GameObject _objToDisableInsteadOfSelf;
    [SerializeField]
    private bool _disableColAndRBOn0Health = false;

    private float _explosiveRadius = 10f;
    private float _explosivePower = 100f;
    private ObjSFX _objSFX;
    private SpawnPatricleEffect _particleEffect;
    private Collider _col;
    private Rigidbody _rb;
    private bool _canTakeDmgFromAtkr = true;
    private bool _willExplode = false;
    private Transform _transform;
    private bool _droppedRBObjs = false;

    private void Awake()
    {
        _objSFX = GetComponent<ObjSFX>();
        if (_disableColAndRBOn0Health)
        {
            _col = GetComponent<Collider>();
            _rb = GetComponent<Rigidbody>();
        }
        if (GetComponent<SpawnPatricleEffect>()) _particleEffect = GetComponent<SpawnPatricleEffect>();
        _transform = this.transform;
    }

    public override void TakeDamage(int amt, bool player)
    {
        base.TakeDamage(amt, player);

        if (UtilityHelper.CheckIfDepleted(_healthAmount))
        {
            _objSFX.PlayBreakSound();
            if (_disableColAndRBOn0Health)
            {
                _col.enabled = false;
                if (_rb) { _rb.useGravity = false; _rb.isKinematic = true; }
            }
            if (_particleEffect) { _particleEffect.InstantiateParticleEffect(); }

            if (_itemSpawnPos) SpawnObjOnDeath(_itemDrop, _prefabsToDrop, _itemSpawnPos.position, _dropChance);

            if (_destructionFragments)
            {
                _destructionFragments.SetActive(true);
                Vector3 explosionPos = _transform.position;
                Collider[] colliders = Physics.OverlapSphere(explosionPos, _explosiveRadius);
                foreach (Collider hit in colliders)
                {
                    Rigidbody rb = hit.GetComponent<Rigidbody>();
                    if (rb) rb.AddExplosionForce(_explosivePower, explosionPos, _explosiveRadius, 3.0F);
                }
            }

            if (_willExplode)
            {
                if (GetComponentInChildren<MagicBombItem>())
                {
                    MagicBombItem bomb = GetComponentInChildren<MagicBombItem>();
                    bomb.Explode();
                    _willExplode = false;
                }
                else
                {
                    Debug.LogError($"MagicBombItem not found on {this.gameObject.name}");
                    Disable();
                }
            }
            else { Disable(); }
        }
    }

    public void SetWillExplode() => _willExplode = true;

    public bool CheckIfFlesh() { return false; }

    private void Disable()
    {
        if (_objToEnableOn0Health) _objToEnableOn0Health.SetActive(true);
        if (_objToDisableInsteadOfSelf) { _objToDisableInsteadOfSelf.SetActive(false); }
        else { this.gameObject.SetActive(false); }
    }
}
