﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Door : BaseItem, IInteractable, ISaveable
{
    [SerializeField]
    private bool _canOpen = true;
    [SerializeField]
    private bool _playsSound = true;
    [SerializeField]
    protected bool _isUnlocked = false;
    [SerializeField]
    protected int _lockNum = 0;
    [SerializeField]
    private bool _useTheAnimComponentOnThisObj = true;
    [SerializeField]
    private Animator _anim;

    protected PlayerInventory _playerInventory;
    private int _currentSceneIndex;
    private DoorSFX _doorSFX;
    private bool _isOpen = false;

    protected override void Awake()
    {
        _doorSFX = GetComponent<DoorSFX>();
        _currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        if (_useTheAnimComponentOnThisObj) _anim = GetComponent<Animator>();
    }

    private void Start()
    {
        _playerInventory = PlayerController.instance.GetComponent<PlayerInventory>();
    }

    public void Interact()
    {
        var PlayerCanOpen = _canOpen && ((!_isUnlocked && _playerInventory.GetHeldKeys().Contains(_lockNum)) || _isUnlocked);

        if (PlayerCanOpen)
        {
            _isOpen = _isUnlocked = StateManager.instance.door_isUnlocked[_currentSceneIndex][_lockNum] = true;
            OpenDoor();
        }

        DisplayInspectText(_isOpen);
        PlaySFX();
    }

    protected virtual void OpenDoor()
    {
        _playerInventory.RemoveKey(_lockNum);
        if (_anim) { _anim.SetTrigger("Unlock"); }
        else
        {
            Debug.LogError($"Misssing Animator on {this.gameObject.name}");
            this.gameObject.SetActive(false);
        }
    }

    protected virtual void PlaySFX() { if (_playsSound) _doorSFX.PlayDoorSound(_isOpen); }

    public int GetIDNum() { return _lockNum; }
    public bool IsInDefaultState() { return _isUnlocked; }
    public void UpdateState(bool state) { _isUnlocked = !state; }
}
