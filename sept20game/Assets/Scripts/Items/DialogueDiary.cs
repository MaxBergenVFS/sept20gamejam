﻿using UnityEngine;

public class DialogueDiary : DialogueTrigger, IInteractable
{
    [SerializeField] private GameObject[] _objsToEnableAfterReading;
    [SerializeField] private GameObject[] _objsToDisableAfterReading;

    private bool _displayingDialogue = false;
    private InventoryItemDatabase _inventoryDatabase;
    private InspectionUI _inspectionUI;

    protected override void Start()
    {
        base.Start();
        _inventoryDatabase = PlayerController.instance.GetComponent<PlayerInventory>().inventoryDatabase;
        _inspectionUI = InspectionUI.instance;
    }

    public void Interact()
    {
        if (!_displayingDialogue)
        {
            TriggerDialogue();
            _displayingDialogue = true;
        }
        else if (_displayingDialogue)
        {
            _displayingDialogue = false;
        }
        if (!GameManager.instance.playerHasReadDiary)
        {
            GameManager.instance.playerHasReadDiary = true;
            foreach (GameObject obj in _objsToEnableAfterReading)
            {
                obj.SetActive(true);
            }
            foreach (GameObject obj in _objsToDisableAfterReading)
            {
                obj.SetActive(false);
            }
        }
    }
}

