﻿using UnityEngine;

public class Amulet : BaseItem, IInteractable
{
    private int _databaseID = 7;
    private PlayerInventory _playerInventory;
    private InventoryItemDatabase _inventoryDatabase;

    private void Start()
    {
        _playerInventory = PlayerController.instance.GetComponent<PlayerInventory>();
        _inventoryDatabase = _playerInventory.inventoryDatabase;
    }

    public void Interact()
    {
        StartLerp();
        DisplayNotifText();
        _inventoryDatabase.SetInventoryItemAmt(_databaseID, 1);
        _playerInventory.DisplayAmuletUI();
    }
}
