﻿using UnityEngine;

public class ReagentItem : BaseItem, IInteractable
{
    [SerializeField]
    private int _amtOfReagentItems = 5;
    [SerializeField]
    private int _databaseId = 4;
    [SerializeField]
    private string _pickUpSFXPath = "Player/Abilities/Distract/Bounce";
    [SerializeField]
    private string _tutorialText = "";

    private PlayerController _playerController;
    private PlayerInventory _playerInventory;
    private InputManager _inputManager;
    private Rigidbody _rb;

    protected override void Awake()
    {
        base.Awake();
        if (GetComponent<Rigidbody>()) _rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        _playerController = PlayerController.instance;
        _playerInventory = _playerController.GetComponent<PlayerInventory>();
        _inputManager = _playerController.GetComponent<InputManager>();
    }

    public void Interact()
    {
        if (_playerInventory.CheckIfInventoryFull(_databaseId)) { DisplayNotifText(false); }
        else
        {
            if (_rb) { _rb.useGravity = false; _rb.isKinematic = true; }
            StartLerp();
            if (!_inputManager.hasAbilities) _inputManager.hasAbilities = true;
            _playerInventory.IncreaseReagentItemAmt(_databaseId, _amtOfReagentItems);
            if (!_playerInventory.CheckIfReagentUIDisplayed(_databaseId))
            {
                NotificationUI.instance.DisplayTxt(_tutorialText);
                _playerInventory.DisplayReagentUI(_databaseId);
            }
            else { DisplayNotifText(true); }
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/" + _pickUpSFXPath);
        }

    }

    public void SetReagentNum(int num) { _amtOfReagentItems = num; }
}
