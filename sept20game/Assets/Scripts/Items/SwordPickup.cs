﻿using UnityEngine;

public class SwordPickup : BaseItem, IInteractable
{
    [SerializeField]
    private string _SFXPath;
    [SerializeField]
    private HeldWeapon _swordRef;
    [SerializeField]
    private GameObject[] _objsToEnableOnPickup;

    private int _databaseID = 0;
    private InventoryItemDatabase _inventoryDatabase;

    private void Start()
    {
        _inventoryDatabase = PlayerController.instance.GetComponent<PlayerInventory>().inventoryDatabase;
    }

    public void Interact()
    {
        if (PlayerController.instance.CheckIfHasSword())
        {
            DisplayInspectText(false);
        }
        else
        {
            StartLerp();
            DisplayInspectText(true);
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/" + _SFXPath, transform.position);
            GameManager.instance.playerHasSword = true;
            _inventoryDatabase.SetInventoryItemAmt(_databaseID, 1);
        }
    }

    protected override void Destroy()
    {
        if (_objsToEnableOnPickup.Length > 0)
            foreach (var obj in _objsToEnableOnPickup)
                obj.SetActive(true);
        _swordRef.gameObject.SetActive(true);
        base.Destroy();
    }
}

