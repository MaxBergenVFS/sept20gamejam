﻿using System.Collections;
using UnityEngine;

public class DisableAfterXSeconds : MonoBehaviour
{
    [SerializeField]
    private float _secondsToWait = 1f;
    [SerializeField]
    private bool _disableObj = false;
    [SerializeField]
    private bool _disableRB = false;
    [SerializeField]
    private bool _disableCollider = false;

    private Rigidbody _rb;
    private Collider _col;

    private void Awake()
    {
        if (_disableRB) _rb = GetComponent<Rigidbody>();
        if (_disableCollider) _col = GetComponent<Collider>();
    }

    private void OnEnable()
    {
        StartCoroutine(DisableAfterSeconds());
    }

    IEnumerator DisableAfterSeconds()
    {
        yield return new WaitForSeconds(_secondsToWait);
        if (_disableRB)
        {
            _rb.isKinematic = true;
            _rb.useGravity = false;
        }
        if (_disableCollider) _col.enabled = false;
        if (_disableObj) this.gameObject.SetActive(false);
    }

}
