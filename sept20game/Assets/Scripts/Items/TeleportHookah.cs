﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class TeleportHookah : BaseItem, IInteractable
{
    private int _teleportPlantID, _sceneToChangeTo;
    private InventoryItemDatabase _inventoryDatabase;
    private LevelChangeFade _levelChangeFade;
    private PlayerController _playerController;
    private MusicManager _musicManager;

    [SerializeField] private ParticleSystem _particleSystem;
    [SerializeField] private int[] _sceneIndicesToTeleportTo;
    [SerializeField] private TeleportPlant _teleportPlantRef;
    [SerializeField] private float _timeBeforeForceLoad = 5f;

    private void Start()
    {
        _playerController = PlayerController.instance;
        _levelChangeFade = _playerController.GetComponentInChildren<LevelChangeFade>();
        _inventoryDatabase = _playerController.GetComponent<PlayerInventory>().inventoryDatabase;
        _teleportPlantID = _teleportPlantRef.GetDatabseID();
    }

    public void Interact()
    {
        if (_inventoryDatabase.GetInventoryItemAmt(_teleportPlantID) > 0) { TeleportToNextScene(); }
        else { DisplayNotifText(false); }
    }

    private void TeleportToNextScene()
    {
        DisplayNotifText(true);
        _particleSystem.Play();
        _inventoryDatabase.SetInventoryItemAmt(_teleportPlantID, 0);
        if (GameManager.instance.sceneIndexToTeleportTo > _sceneIndicesToTeleportTo.Length - 1)
            GameManager.instance.sceneIndexToTeleportTo = 0;
        _sceneToChangeTo = _sceneIndicesToTeleportTo[GameManager.instance.sceneIndexToTeleportTo];
        _levelChangeFade.OverrideLevelIndex(_sceneToChangeTo);
        GameManager.instance.sceneIndexToTeleportTo++;
        _levelChangeFade.FadeOut();
        _musicManager = GameObject.Find("MusicManager").GetComponent<MusicManager>();
        _musicManager.IncrementParamIndex();
        StopAllCoroutines();
        StartCoroutine(ForceLevelLoad());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    IEnumerator ForceLevelLoad()
    {
        yield return new WaitForSeconds(_timeBeforeForceLoad);
        SceneManager.LoadScene(_sceneToChangeTo);
    }
}
