﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class OrbSlot : MonoBehaviour, IInteractable, ISaveable
{
    [SerializeField]
    private int _idOfItemToRemove = 0;
    [SerializeField]
    private GameObject _objToSpawn;

    private PlayerInventory _playerInventory;
    private string _SFXPath = "event:/SFX/Interactables/OrbPlacement";
    private bool _SFXHasPlayed, _isUnlocked = false;
    private int _currentSceneIndex;

    private void Awake() => _currentSceneIndex = SceneManager.GetActiveScene().buildIndex;

    private void Start() => _playerInventory = PlayerController.instance.GetComponent<PlayerInventory>();

    public void Interact()
    {
        if (!_playerInventory.CheckIfHasReagent(_idOfItemToRemove)) return;
        _playerInventory.ReduceReagentItemAmt(_idOfItemToRemove);
        StateManager.instance.orbSlot_hasBeenUnlocked[_currentSceneIndex][_idOfItemToRemove] = true;
        UnlockSlot(true);
        PlaySFX();
    }

    private void PlaySFX()
    {
        if (_isUnlocked && !_SFXHasPlayed)
        {
            FMODUnity.RuntimeManager.PlayOneShot(_SFXPath, transform.position);
            _SFXHasPlayed = true;
        }
    }

    private void UnlockSlot(bool cond)
    {
        _objToSpawn.SetActive(cond);
        _isUnlocked = cond;
        //print($"UnlockSlot: {cond}");
    }

    public void UpdateState(bool state) => UnlockSlot(!state);
    public int GetIDNum() { return _idOfItemToRemove; }
    public bool IsInDefaultState() { return _isUnlocked; }
    public bool CheckIfUnlocked() { return _isUnlocked; }

}
