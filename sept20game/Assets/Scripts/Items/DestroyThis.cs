﻿using UnityEngine;

public class DestroyThis : MonoBehaviour
{
    public void Destroy()
    {
        Destroy(this.gameObject, 1f);
    }
}
