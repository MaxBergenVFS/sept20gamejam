﻿using UnityEngine;

public class Crossbow : BaseItem, IInteractable
{
    [SerializeField] private bool _enableInventoryOnPickup = true;
    [SerializeField] private GameObject[] _crossbowUIElementsToEnable;
    [SerializeField] private ProjectileWeapon _youArmsShootProjectile;
    [SerializeField] private int _ammoAmtNum = 5;

    private int _ammoDatabaseId = 16;
    private int _databaseId = 15;
    private InputManager _playerInput;
    private InventoryItemDatabase _inventoryDatabase;
    private PlayerController _playerController;
    private PlayerInventory _playerInventory;
    private PlayerAttack _playerAttack;
    private string _SFXpath = "event:/SFX/Interactables/BackpackPickup";

    private void Start()
    {
        _playerController = PlayerController.instance;
        _playerInput = _playerController.GetComponent<InputManager>();
        _playerAttack = _playerController.GetComponent<PlayerAttack>();
        _playerInventory = _playerController.GetComponent<PlayerInventory>();
        _inventoryDatabase = _playerInventory.inventoryDatabase;
    }

    public void Interact()
    {
        StartLerp();
        DisplayNotifText();
        _youArmsShootProjectile.enabled = true;
        _inventoryDatabase.SetInventoryItemAmt(_databaseId, 1);
        _inventoryDatabase.SetInventoryItemAmt(_ammoDatabaseId, _ammoAmtNum);
        _playerInventory.UpdateAmmoAmtNum();
        _playerAttack.EquipCrossbow();
        if (!GameManager.instance.playerHasCrossbow) GameManager.instance.playerHasCrossbow = true;
        FMODUnity.RuntimeManager.PlayOneShot(_SFXpath, transform.position);
        foreach (var element in _crossbowUIElementsToEnable)
            element.SetActive(true);
    }
}

