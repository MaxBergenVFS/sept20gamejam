﻿using UnityEngine;

public class OrbKey : Key
{
    [SerializeField]
    private int _databaseID;
    [SerializeField]
    private GameObject _shadow;

    private string _SFXPath = "event:/SFX/Interactables/OrbPickup";

    protected override void PickUpKey()
    {
        _shadow.SetActive(false);
        _playerInventory.SetKey(true, _keyNum, _databaseID);
        FMODUnity.RuntimeManager.PlayOneShot(_SFXPath);
    }
}
