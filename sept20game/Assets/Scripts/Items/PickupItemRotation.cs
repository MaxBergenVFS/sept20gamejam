﻿using UnityEngine;

public class PickupItemRotation : MonoBehaviour
{
    [SerializeField]
    private float _rotationSpeed = 50f;
    void Update()
    {
        transform.RotateAround(this.transform.position, Vector3.up, _rotationSpeed * Time.deltaTime);
    }
}
