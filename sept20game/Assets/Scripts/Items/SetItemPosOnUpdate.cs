﻿using UnityEngine;

public class SetItemPosOnUpdate : MonoBehaviour
{
    [SerializeField]
    private Transform _pos;

    private Transform _transform;

    private void Awake()
    {
        _transform = this.transform;
    }

    private void Update()
    {
        if (_pos == null) return;
        _transform.position = _pos.position;
    }

    public Vector3 GetCurrentPos()
    {
        this.enabled = false;
        return _transform.position;
    }
}
