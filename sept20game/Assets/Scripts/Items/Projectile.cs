﻿using UnityEngine;

public class Projectile : BaseItem, IInteractable, IDeflectable
{
    [SerializeField] private int _forceAmt = 500;
    [SerializeField] private float _secsUntilRendererIsEnabled = 0.1f;
    [SerializeField] private int _databaseID = 16;
    [SerializeField] private int _projectileDmgAmt = 5;

    private float _destroyTime = 0.1f;
    private bool _deflectedByPlayer, _frozen = false;
    private string _impactSfxPath = "event:/SFX/Player/Weapon/Crossbow/ArrowImpact";
    private Vector3 _originPos;
    private Rigidbody _rb;
    private MeshRenderer _rend;
    private PlayerInventory _playerInventory;

    protected override void Awake()
    {
        _rb = GetComponent<Rigidbody>();
        _rend = GetComponent<MeshRenderer>();
    }

    private void Start() => _playerInventory = PlayerController.instance.GetComponent<PlayerInventory>();

    public int GetDatabaseID() { return _databaseID; }

    public void Deflect()
    {
        print("DEFLECT");
        _deflectedByPlayer = true;
        _rb.velocity = Vector3.zero;
        Vector3 targetDir = _originPos - transform.position;
        _rb.AddForce(targetDir * _forceAmt);
    }

    public void Interact()
    {
        if (_playerInventory.CheckIfInventoryFull(_databaseID))
        {
            DisplayNotifText(false);
        }
        else
        {
            StartLerp();
            _playerInventory.IncreaseReagentItemAmt(_databaseID, 1);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<IDamageable>() != null && !other.gameObject.CompareTag("Player"))
        {
            IDamageable obj = other.gameObject.GetComponent<IDamageable>();
            obj.TakeDamage(_projectileDmgAmt, _deflectedByPlayer);
            this.transform.SetParent(other.transform);
            Freeze();
            Destroy(this.gameObject, _destroyTime);
        }
        if (other.CompareTag("Environment"))
            Freeze();
    }

    private void Freeze()
    {
        PlayImpactSFX();
        _frozen = true;
        _rb.constraints = RigidbodyConstraints.FreezeAll;
    }

    private void OnEnable() => _rend.enabled = false;

    private void Update()
    {
        if (_frozen) return;
        transform.rotation = Quaternion.LookRotation(_rb.velocity);
        if (_rend.enabled) return;
        _secsUntilRendererIsEnabled -= Time.deltaTime;
        if (_secsUntilRendererIsEnabled < 0) _rend.enabled = true;
    }
    public void Shoot(Vector3 originPos, Vector3 targetDir)
    {
        _originPos = originPos;
        _rb.AddForce(targetDir * _forceAmt);
        //Destroy(this.gameObject, _destroyTime);
    }

    private void PlayImpactSFX() => FMODUnity.RuntimeManager.PlayOneShot(_impactSfxPath, transform.position);

}
