﻿using UnityEngine;

public class SpawnDecalOnColllision : MonoBehaviour
{
    [SerializeField]
    private GameObject _decalToSpawn;
    [SerializeField]
    private string _sfxPath = "Player/Abilities/Distract/Splat";

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Player")) return;

        Vector3 hitNormal = new Vector3
            (
                other.contacts[0].normal.x * Mathf.Rad2Deg,
                other.contacts[0].normal.y * Mathf.Rad2Deg,
                other.contacts[0].normal.z * Mathf.Rad2Deg
            ).normalized;

        var ray = new Ray(transform.position, -hitNormal);
        if (Physics.Raycast(ray, out var hit, 1f))
        {
            var splatter = Instantiate(_decalToSpawn, hit.point + (hitNormal * 0.05f), Quaternion.Euler(hitNormal));
            splatter.transform.up = hitNormal;
            splatter.transform.parent = hit.transform;
            if (_sfxPath != "") { FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/" + _sfxPath, transform.position); }
            //Destroy(splatter, 10f);
        }
    }
}
