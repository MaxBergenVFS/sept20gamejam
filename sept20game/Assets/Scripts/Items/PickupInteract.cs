﻿using UnityEngine;

public class PickUpInteract : MonoBehaviour, IInteractable
{
    private bool _isBeingHeld = false;
    private Transform _transform;
    private PlayerInteract _playerInteract;
    private Rigidbody _rb;
    private CameraController _cameraController;

    [SerializeField]
    private GameObject _triggerChecker;

    private void Awake()
    {
        _transform = this.transform;
        _rb = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        _playerInteract = PlayerController.instance.GetComponent<PlayerInteract>();
        _cameraController = PlayerController.instance.GetComponent<CameraController>();
    }

    public void Interact()
    {
        if (_isBeingHeld) { DropItem(); }
        else { PickUpItem(); }
    }

    public void DropItem()
    {
        _isBeingHeld = false;
        _cameraController.SetHeldObj(this.gameObject, false);
        _rb.useGravity = true;
        _transform.SetParent(null);
        _triggerChecker.SetActive(false);
        _playerInteract.ForceResetInteract();
    }

    private void PickUpItem()
    {
        _isBeingHeld = true;
        _cameraController.SetHeldObj(this.gameObject, true);
        _rb.useGravity = false;
        _transform.SetParent(PlayerController.instance.holdObjPos);
        _transform.localPosition = Vector3.zero;
        _triggerChecker.SetActive(true);
        _playerInteract.ForceAllowInteract(this.gameObject);
    }

}
