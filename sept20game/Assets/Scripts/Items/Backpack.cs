﻿using UnityEngine;

public class Backpack : BaseItem, IInteractable, ISaveable
{
    [SerializeField]
    private bool _enableInventoryOnPickup = true;
    [SerializeField]
    private GameObject _heldSword;

    private InputManager _playerInput;
    private PlayerAttack _playerAttack;
    private string _SFXpath = "event:/SFX/Interactables/BackpackPickup";

    private void Start()
    {
        _playerInput = PlayerController.instance.GetComponent<InputManager>();
        _playerAttack = PlayerController.instance.GetComponent<PlayerAttack>();
    }

    public void Interact()
    {
        StartLerp();
        if (_heldSword != null) _heldSword.SetActive(true);
        DisplayNotifText();
        _playerInput.hasInventory = _enableInventoryOnPickup;
        FMODUnity.RuntimeManager.PlayOneShot(_SFXpath, transform.position);
        if (!GameManager.instance.playerHasSword) GameManager.instance.playerHasSword = true;
        _playerAttack.EquipSword();
    }

    public int GetIDNum() { return 0; }
    public bool IsInDefaultState() { return this.gameObject.activeSelf; }

    public void UpdateState(bool state)
    {
        this.gameObject.SetActive(!state);
        if (_playerInput == null) _playerInput = PlayerController.instance.GetComponent<InputManager>();
        _playerInput.hasInventory = state;
        if (_heldSword != null) _heldSword.SetActive(state);
        if (GameManager.instance.playerHasSword != state) GameManager.instance.playerHasSword = state;
    }
}
