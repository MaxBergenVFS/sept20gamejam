﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class TeleportPlant : BaseItem, IInteractable, ISaveable
{
    private int _databaseID = 8;
    private PlayerController _playerController;
    private InventoryItemDatabase _inventoryDatabase;
    private InputManager _inputManager;
    private bool _hasBeenPicked = false;
    private int _currentSceneIndex;

    [SerializeField] private int _IDNum;
    [SerializeField] private GameObject _nonInteractableModel;

    protected override void Awake()
    {
        base.Awake();
        _currentSceneIndex = _currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
    }

    private void Start()
    {
        _playerController = PlayerController.instance;
        _inputManager = _playerController.GetComponent<InputManager>();
        _inventoryDatabase = _playerController.GetComponent<PlayerInventory>().inventoryDatabase;
    }

    public int GetDatabseID() { return _databaseID; }

    public void Interact()
    {
        if (_inputManager.hasInventory)
        {
            if (_inventoryDatabase.GetInventoryItemAmt(_databaseID) > 0)
            {
                DisplayNotifText(true);
            }
            else
            {
                StartLerp();
                _hasBeenPicked = true;
                StateManager.instance.plant_hasBeenPicked[_currentSceneIndex][_IDNum] = _hasBeenPicked;
                _inventoryDatabase.SetInventoryItemAmt(_databaseID, 1);
            }
        }
        else { DisplayNotifText(false); }
    }

    public int GetIDNum() { return _IDNum; }
    public bool IsInDefaultState() { return !_hasBeenPicked; }
    public void UpdateState(bool state)
    {
        if (!GameManager.instance.playerHasReadDiary) return;

        _hasBeenPicked = state;
        this.gameObject.SetActive(state);
        _nonInteractableModel.SetActive(false);
    }
}
