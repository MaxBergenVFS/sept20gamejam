﻿using System.Collections;
using UnityEngine;

public abstract class BaseItem : MonoBehaviour
{
    [SerializeField]
    private string _posInteractTxt = "";
    [SerializeField]
    private string _negInteractTxt = "";
    [SerializeField]
    private bool _thisObjWillRespawn = false;

    private float _lerpSpeed = 4f;
    private Vector3 _startingPos;
    private Vector3 _destPos;
    private Quaternion _startingRot;
    private float _lerpValue = 0f;
    private SetItemPosOnUpdate _setItemPos;

    protected virtual void Awake()
    {
        _startingRot = transform.rotation;
        if (GetComponent<SetItemPosOnUpdate>()) _setItemPos = GetComponent<SetItemPosOnUpdate>();
    }

    protected void StartLerp()
    {
        _startingPos = transform.position;
        if (_setItemPos) _startingPos = _setItemPos.GetCurrentPos();
        _destPos = PlayerController.instance.transform.position;
        GetComponent<Collider>().enabled = false;
        if (_thisObjWillRespawn) Respawn();
        StopAllCoroutines();
        StartCoroutine(LerpPickUp());
    }

    protected void DisplayNotifText()
    {
        if (_posInteractTxt == "") NotificationUI.instance.DisplayTxt(_negInteractTxt);
        else if (_negInteractTxt == "") NotificationUI.instance.DisplayTxt(_posInteractTxt);
        else Debug.LogError($"text fields are empty on {this.gameObject.name}");
    }

    protected void DisplayNotifText(bool cond)
    {
        string str;
        if (cond) { str = _posInteractTxt; }
        else { str = _negInteractTxt; }
        if (str != "") NotificationUI.instance.DisplayTxt(str);
    }

    protected void DisplayInspectText()
    {
        if (_posInteractTxt == "") InspectionUI.instance.DisplayTxt(_negInteractTxt);
        else if (_negInteractTxt == "") InspectionUI.instance.DisplayTxt(_posInteractTxt);
        else Debug.LogError($"text fields are empty on {this.gameObject.name}");
    }

    protected void DisplayInspectText(bool cond)
    {
        string str;
        if (cond)
        {
            str = _posInteractTxt;
            if (_posInteractTxt == "") Debug.LogError($"_posInteractTxt is empty on {this.gameObject.name}");
        }
        else
        {
            str = _negInteractTxt;
            if (_negInteractTxt == "") Debug.LogError($"_negInteractTxt is empty on {this.gameObject.name}");
        }
        InspectionUI.instance.DisplayTxt(str);
    }

    protected virtual void Destroy() { Destroy(this.gameObject); }
    protected virtual void Destroy(float num) { Destroy(this.gameObject, num); }

    private void Respawn()
    {
        GameObject obj = Instantiate(this.gameObject, _startingPos, _startingRot);
        obj.transform.GetComponent<Collider>().enabled = true;
    }

    IEnumerator LerpPickUp()
    {
        while (_lerpValue < 1f)
        {
            _lerpValue += Time.deltaTime * _lerpSpeed;
            transform.position = Vector3.Lerp(_startingPos, _destPos, _lerpValue);
            yield return null;
        }
        Destroy();
    }
}
