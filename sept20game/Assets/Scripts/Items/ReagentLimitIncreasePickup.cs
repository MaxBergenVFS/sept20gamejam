﻿using UnityEngine;

public class ReagentLimitIncreasePickup : BaseItem, IInteractable
{
    private PlayerInventory _playerInventory;

    [SerializeField]
    private int _maxReagentIncreaseAmt = 5;

    protected override void Awake()
    {
        base.Awake();
        _playerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerInventory>();
    }

    public void Interact()
    {
        StartLerp();
        _playerInventory.IncreaseMaxReagentAmt(_maxReagentIncreaseAmt);
        DisplayInspectText();
    }
}
