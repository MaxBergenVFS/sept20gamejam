﻿using UnityEngine.SceneManagement;
using UnityEngine;
using System.Collections;

public class ChangeSceneOnInteract : MonoBehaviour, IInteractable
{
    [SerializeField]
    private bool _returnToPreviousScene = false;
    [SerializeField]
    protected int _sceneToChangeTo = 0;

    private GameManager _gameManager;
    private GameObject _levelChangeFadeObj;
    private LevelChangeFade _levelChangeFade;
    private Animator _levelChangeFadeAnim;
    private InputManager _playerInput;
    private float _timeBeforeForceLoad = 4f;

    private void Start()
    {
        _gameManager = GameManager.instance;
        _levelChangeFadeObj = PlayerController.instance.GetComponentInChildren<LevelChangeFade>().gameObject;
        _levelChangeFade = _levelChangeFadeObj.GetComponent<LevelChangeFade>();
        _levelChangeFadeAnim = _levelChangeFade.GetComponent<Animator>();
        _playerInput = PlayerController.instance.GetComponent<InputManager>();
    }

    public void Interact()
    {
        _gameManager.UpdateStats();
        if (_returnToPreviousScene) { _levelChangeFade.OverrideLevelIndex(_gameManager.GetPreviousSceneIndex()); }
        else { _levelChangeFade.OverrideLevelIndex(_sceneToChangeTo); }
        _playerInput.enabled = false;
        _levelChangeFadeAnim.SetTrigger("FadeOut");
        StopAllCoroutines();
        StartCoroutine(ForceLevelLoad());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    IEnumerator ForceLevelLoad()
    {
        yield return new WaitForSeconds(_timeBeforeForceLoad);
        SceneManager.LoadScene(_sceneToChangeTo);
    }
}
