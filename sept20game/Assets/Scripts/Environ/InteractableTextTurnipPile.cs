﻿using UnityEngine;

public class InteractableTextTurnipPile : InteractableTextDisplay
{
    [SerializeField]
    private string _interactTxtHasBackPack = "";
    [SerializeField]
    private string[] _additionalTxtHasBackPack;
    [SerializeField]
    private int _lineToPickUpTurnipOn = 2;

    private PlayerController _playerController;
    private InputManager _inputManager;
    private InventoryItemDatabase _inventoryItemDatabase;
    private int _currentIndexHasBackpack = 0;
    private int _turnipDatabaseID = 3;
    private string _SFXpath = "event:/SFX/Environment/SecretFound";

    protected override void Start()
    {
        base.Start();
        _playerController = PlayerController.instance;
        _inputManager = _playerController.GetComponent<InputManager>();
        _inventoryItemDatabase = _playerController.GetComponent<PlayerInventory>().inventoryDatabase;
    }

    protected override string TxtToDisplay()
    {
        if (!_inputManager.hasInventory)
        {
            if (_additionalTxt.Length == 0 || _currentIndex == _additionalTxt.Length)
            {
                return _interactTxt;
            }
            else
            {
                string txt = _additionalTxt[_currentIndex];
                _currentIndex++;
                return txt;
            }
        }
        else
        {
            if (_additionalTxtHasBackPack.Length == 0 || _currentIndexHasBackpack == _additionalTxtHasBackPack.Length)
            {
                return _interactTxtHasBackPack;
            }
            else
            {
                string txt = _additionalTxtHasBackPack[_currentIndexHasBackpack];
                _currentIndexHasBackpack++;
                if (_currentIndexHasBackpack == _lineToPickUpTurnipOn)
                {
                    _inventoryItemDatabase.SetInventoryItemAmt(_turnipDatabaseID, 1);
                    FMODUnity.RuntimeManager.PlayOneShot(_SFXpath);
                }
                return txt;
            }
        }
    }
}
