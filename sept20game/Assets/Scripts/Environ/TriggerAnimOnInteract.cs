﻿using UnityEngine;

public class TriggerAnimOnInteract : MonoBehaviour, IInteractable
{
    private Animator _anim;

    [SerializeField]
    private string _triggerString = "Unlock";

    private void Awake()
    {
        _anim = GetComponent<Animator>();
    }

    public void Interact()
    {
        _anim.SetTrigger(_triggerString);
    }
}
