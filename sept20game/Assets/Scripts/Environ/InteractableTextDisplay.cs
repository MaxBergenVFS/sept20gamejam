﻿using UnityEngine;

public class InteractableTextDisplay : MonoBehaviour, IInteractable
{
    [SerializeField]
    protected string _interactTxt = "";
    [SerializeField]
    protected string[] _additionalTxt;

    private InspectionUI _inspectionUI;
    protected int _currentIndex = 0;

    protected virtual void Start()
    {
        _inspectionUI = InspectionUI.instance;
    }

    public void Interact()
    {
        if (_inspectionUI.CheckIfCurrentlyTypingSentence()) return;
        _inspectionUI.DisplayTxt(TxtToDisplay());
    }

    protected virtual string TxtToDisplay()
    {
        if (_additionalTxt.Length == 0 || _currentIndex == _additionalTxt.Length)
        {
            return _interactTxt;
        }
        else
        {
            string txt = _additionalTxt[_currentIndex];
            _currentIndex++;
            return txt;
        }
    }

    public void ClearText() { _inspectionUI.DisplayTxt(""); }
}
