﻿using UnityEngine;
using System.Collections;

public class Lever : MonoBehaviour, IInteractable
{
    [SerializeField]
    private bool _oneTimeUse = false;
    [SerializeField]
    private bool _hasOnlyOneState = false;
    [SerializeField]
    private float _secondsBeforeActivate = 1f;
    [SerializeField]
    private GameObject[] _objsToActivate;
    [SerializeField]
    private GameObject[] _objsToActivateSecondState;
    [SerializeField]
    private Animator _animToTrigger;
    [SerializeField]
    private string _animString = "Unlock";

    private bool _inFirstState = true;
    private bool _hasBeenUsed = false;

    private ObjSFX _SFX;
    private Animator _anim;

    private void Awake()
    {
        _anim = GetComponentInChildren<Animator>();
        _SFX = GetComponent<ObjSFX>();
    }

    public void Interact()
    {
        if (_hasBeenUsed) return;

        if (_oneTimeUse)
        {
            SetObjsActive(_objsToActivate);
            if (_animToTrigger) _animToTrigger.SetTrigger(_animString);
            _hasBeenUsed = true;
        }
        else
        {
            if (_hasOnlyOneState) { SetObjsActive(_objsToActivate); }
            else
            {
                if (_inFirstState)
                {
                    SetObjsActive(_objsToActivate);
                    _inFirstState = false;
                }
                else
                {
                    SetObjsActive(_objsToActivateSecondState);
                    _inFirstState = true;
                }
            }
        }
        _anim.SetTrigger("Activate");
        _SFX.PlayLeverSound();
    }

    private void SetObjsActive(GameObject[] objs)
    {
        if (_secondsBeforeActivate > 0f)
        {
            StopAllCoroutines();
            StartCoroutine(StartActivationTimer(objs));
        }
        else
        {
            foreach (var obj in objs)
            {
                obj.SetActive(!obj.activeSelf);
            }
        }
    }

    IEnumerator StartActivationTimer(GameObject[] objs)
    {
        yield return new WaitForSeconds(_secondsBeforeActivate);
        foreach (var obj in objs)
        {
            obj.SetActive(!obj.activeSelf);
        }
    }
}
