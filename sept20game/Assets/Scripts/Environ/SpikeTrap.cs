﻿using UnityEngine;

public class SpikeTrap : DamageVolume
{
    [SerializeField]
    private Animator _anim;
    [SerializeField]
    private bool _disableAfterSingleUse = true;

    private ObjSFX _SFX;

    private void Awake()
    {
        _SFX = GetComponent<ObjSFX>();
    }

    protected override void DoDamage()
    {
        base.DoDamage();
        _anim.SetTrigger("TriggerSpikes");
        _SFX.PlaySpikeTrapSound();
        if (_disableAfterSingleUse) this.gameObject.SetActive(false);
    }
}
