﻿using UnityEngine;

[RequireComponent(typeof(LadderSFX))]
public class Ladder : MonoBehaviour
{
    [SerializeField]
    private Collider _ladderTop;

    private LadderSFX _sfx;

    private void Awake()
    {
        _sfx = GetComponent<LadderSFX>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            PlayerController.instance.SetClimbingLadder(true);
            _ladderTop.enabled = false;
            _sfx.enabled = true;
            _sfx.InitSpeedValues();
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            PlayerController.instance.SetClimbingLadder(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            PlayerController.instance.SetClimbingLadder(false);
            _ladderTop.enabled = true;
            _sfx.enabled = false;
        }
    }
}
