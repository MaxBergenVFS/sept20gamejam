﻿using UnityEngine;

public class ChangeSceneAfterXSeconds : ChangeSceneOnInteract
{
    [SerializeField]
    private float _secondsBeforeSceneChange;

    private bool _hasCalledInteract = false;

    void Update()
    {
        _secondsBeforeSceneChange -= Time.deltaTime;
        if (_secondsBeforeSceneChange < 0 && !_hasCalledInteract) { Interact(); _hasCalledInteract = true; }
    }
}
