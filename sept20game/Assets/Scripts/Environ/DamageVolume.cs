﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class DamageVolume : MonoBehaviour
{
    [SerializeField]
    private float _triggerTime = 2f;
    [SerializeField]
    private int _baseDamage = 20;

    private bool _doDamage = false;
    private float _currentTriggerTime = 0f;
    private List<IDamageable> _damageables = new List<IDamageable>();
    private Collider _col;

    private void Awake()
    {
        _col = GetComponent<Collider>();
    }

    private void Update()
    {
        _currentTriggerTime += Time.deltaTime;
        if (_currentTriggerTime >= _triggerTime)
        {
            DoDamage();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<IDamageable>() != null)
        {
            IDamageable damageable = other.gameObject.GetComponent<IDamageable>();
            _damageables.Add(damageable);
        }
    }

    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.GetComponent<IDamageable>() != null)
        {
            IDamageable damageable = other.gameObject.GetComponent<IDamageable>();
            _damageables.Remove(damageable);
        }
    }

    protected virtual void DoDamage()
    {
        _currentTriggerTime = 0f;

        if (_damageables.Count < 1) return;

        foreach (var damageable in _damageables)
        {
            damageable.TakeDamage(_baseDamage, false);
        }
        _damageables.Clear();
    }

    private void OnEnable()
    {
        //_triggerTime - 0.1f allows OnTriggerEnter to add damageables to _damageables before the first dmg tick
        _currentTriggerTime = _triggerTime - 0.1f;
    }

    private void OnDisable()
    {
        _damageables.Clear();
    }

}
