﻿using UnityEngine;

public class UnderwaterMovementVolume : MonoBehaviour
{
    private PlayerController _playerController;

    private void Start()
    {
        _playerController = PlayerController.instance;    
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _playerController.SetIsUnderwater(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _playerController.SetIsUnderwater(false);
        }
    }
}
