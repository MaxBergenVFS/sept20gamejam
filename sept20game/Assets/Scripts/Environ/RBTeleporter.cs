﻿using UnityEngine;

public class RBTeleporter : MonoBehaviour
{
    [SerializeField]
    private Transform _spawnPoint;
    [SerializeField]
    private GameObject _objToSpawn;

    private void Start()
    {
        SpawnObj();
    }

    private void SpawnObj()
    {
        Instantiate(_objToSpawn, _spawnPoint.position, Quaternion.identity);
    }

    private void TeleportObj(GameObject obj)
    {
        obj.transform.position = _spawnPoint.position;
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.GetComponent<Rigidbody>())
        {
            TeleportObj(other.gameObject);
        }
    }
}
