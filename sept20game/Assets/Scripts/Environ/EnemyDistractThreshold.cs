﻿using UnityEngine;

public class EnemyDistractThreshold : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<IDistractable>() != null)
        {
            other.gameObject.GetComponent<IDistractable>().DisableDistract();
        } 
    }
}
