﻿using UnityEngine;

public class ShadowVolume : MonoBehaviour
{

    private PlayerController _playerController;

    private void Start()
    {
        _playerController = PlayerController.instance;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _playerController.SetIsInShadows(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            _playerController.SetIsInShadows(false);
        }
    }
}
