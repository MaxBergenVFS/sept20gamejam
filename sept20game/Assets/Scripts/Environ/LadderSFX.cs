﻿using UnityEngine;

public class LadderSFX : MonoBehaviour
{
    private float _playerMovementSpeed;
    private float _timeBetweenFootsteps;
    private float _timeUntilNextFootstep;
    private float _ladderSFXSpeedMultiplier = 0.5f;
    private string _path = "event:/SFX/Player/LadderClimb";
    private PlayerController _playerController;
    private PlayerFootstepSFX _footstepSFX;

    private void Start()
    {
        _playerController = PlayerController.instance;
        _footstepSFX = _playerController.GetComponent<PlayerFootstepSFX>();
        this.enabled = false;
    }

    private void Update()
    {
        if (_playerController.GetMovementSpeed() == 0) return;
        _timeUntilNextFootstep -= Time.deltaTime * _playerMovementSpeed;
        if (_timeUntilNextFootstep < 0f) PlayLadderClimbingSound();
    }

    public void InitSpeedValues()
    {
        _playerMovementSpeed = _playerController.GetMovementSpeed();
        _timeBetweenFootsteps = _footstepSFX.GetTimeBetweenFootsteps();
    }

    private void PlayLadderClimbingSound()
    {
        FMODUnity.RuntimeManager.PlayOneShot(_path);
        _playerMovementSpeed = _playerController.GetMovementSpeed();
        _timeUntilNextFootstep = _timeBetweenFootsteps * _ladderSFXSpeedMultiplier;
        //print($"_timeUntilNextFootstep: {_timeUntilNextFootstep}");
    }
}
