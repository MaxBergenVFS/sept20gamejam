﻿using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    [SerializeField]
    private Dialogue[] dialogue;

    private DialogueManager _dialogueManager;
    private int _currentIndex = 0;

    protected virtual void Start()
    {
        _dialogueManager = DialogueManager.instance;
    }

    protected void TriggerDialogue()
    {
        _dialogueManager.StartDialogue(dialogue[_currentIndex]);
        if (_currentIndex >= dialogue.Length - 1) { _currentIndex = dialogue.Length - 1; }
        else { _currentIndex++; }

    }
}
