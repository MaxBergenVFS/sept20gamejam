﻿using UnityEngine;

public class NPCDialogue : DialogueTrigger, IInteractable
{
    [SerializeField] private Transform _cameraTarget;

    private bool _canStartDialogue = true;

    public void Interact()
    {
        if (_canStartDialogue)
        {
            TriggerDialogue();
            _canStartDialogue = false;
        }
        else { _canStartDialogue = true; }
    }
}
