﻿using UnityEngine;

public class SoulDialogue : DialogueTrigger
{
    [SerializeField]
    private SoulItem _soulItem;

    public void TriggerDialogueOnDeath()
    {
        TriggerDialogue();
        _soulItem.gameObject.SetActive(true);
    }
}
