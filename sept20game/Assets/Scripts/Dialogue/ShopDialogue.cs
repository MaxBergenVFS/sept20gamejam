﻿using UnityEngine;

public class ShopDialogue : DialogueTrigger, IInteractable
{
    [SerializeField] private int _amtOfGoldRequiredToBuy;
    [SerializeField] private string _negInteractTxt;
    [SerializeField] GameObject _itemToPurchase;

    private InventoryItemDatabase _inventoryDatabase;
    private int _goldDatabaseID = 1;
    private InspectionUI _inspectionUI;
    private bool _displayingDialogue = false;

    protected override void Start()
    {
        base.Start();
        _inventoryDatabase = PlayerController.instance.GetComponent<PlayerInventory>().inventoryDatabase;
        _inspectionUI = InspectionUI.instance;
    }

    public void Interact()
    {
        if (_inventoryDatabase.GetInventoryItemAmt(_goldDatabaseID) >= _amtOfGoldRequiredToBuy)
        {
            if (!_displayingDialogue)
            {
                PurchaseItem();
                _displayingDialogue = true;
            }
            else if (_displayingDialogue)
            {
                _displayingDialogue = false;
            }
        }
        else { _inspectionUI.DisplayTxt(_negInteractTxt); }
    }

    private void PurchaseItem()
    {
        TriggerDialogue();
        _itemToPurchase.SetActive(true);
        _inventoryDatabase.AddToInventoryItemAmt(_goldDatabaseID, -_amtOfGoldRequiredToBuy);
    }
}
