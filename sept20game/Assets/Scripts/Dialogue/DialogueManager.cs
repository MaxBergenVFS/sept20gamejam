﻿using TMPro;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class DialogueManager : MonoBehaviour
{
    private Queue<string> _sentences = new Queue<string>();
    private bool _sentenceIsStillBeingTyped = false;
    private Animator _anim;
    private string _currentSentenceRef;
    private InputManager _playerInput;
    private PauseAnimation _pauseAnim;

    [SerializeField]
    private TextMeshProUGUI _dialogueText;
    [SerializeField]
    private TextMeshProUGUI _characterName;
    [SerializeField]
    private float _timeBetweenLetters = 0.1f;

    public static DialogueManager instance { get { return _instance; } }
    private static DialogueManager _instance;

    private void Awake()
    {
        _instance = this;
        _anim = GetComponent<Animator>();
        _pauseAnim = GetComponent<PauseAnimation>();
    }

    private void Start()
    {
        _playerInput = PlayerController.instance.GetComponent<InputManager>();
    }

    public void StartDialogue(Dialogue dialogue)
    {
        _anim.SetBool("IsOpen", true);
        _playerInput.StartTalkingToNPC();
        _characterName.text = dialogue.characterName;
        _sentences.Clear();
        foreach (string sentence in dialogue.sentences)
        {
            _sentences.Enqueue(sentence);
        }
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (_sentences.Count <= 0 && !_sentenceIsStillBeingTyped)
        {
            EndDialogue();
            return;
        }

        if (_sentenceIsStillBeingTyped)
        {
            StopAllCoroutines();
            _dialogueText.text = _currentSentenceRef;
            _sentenceIsStillBeingTyped = false;
        }
        else
        {
            string sentence = _sentences.Dequeue();
            _currentSentenceRef = sentence;
            StopAllCoroutines();
            StartCoroutine(TypeSentence(sentence));
        }
    }

    IEnumerator TypeSentence(string sentence)
    {
        _sentenceIsStillBeingTyped = true;
        _dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            _dialogueText.text += letter;
            if (_dialogueText.text == sentence) _sentenceIsStillBeingTyped = false;
            FMODUnity.RuntimeManager.PlayOneShot("event:/UI/dialogueTyping");
            yield return new WaitForSeconds(_timeBetweenLetters);
        }
    }

    private void EndDialogue()
    {
        _dialogueText.text = "";
        _characterName.text = "";
        _anim.SetBool("IsOpen", false);
        _pauseAnim.UnPauseAnim();
        _playerInput.StopTalkingToNPC();
    }
}
