using UnityEngine;
using UnityEngine.SceneManagement;

public class Secret : MonoBehaviour, ISaveable
{
    [SerializeField]
    private string _txtToDisplay = "SECRET FOUND";

    private float _txtDisplayTime = 5f;
    private int _IDNum;
    private int _currentSceneIndex;
    private bool _secretHasBeenFound = false;
    private Collectables _collectables;
    private string _path = "event:/SFX/Environment/SecretFound";


    protected virtual void Awake()
    {
        _currentSceneIndex = _currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
    }

    private void Start()
    {
        _collectables = PlayerController.instance.GetComponent<Collectables>();
    }

    protected virtual void SecretFound()
    {
        if (_secretHasBeenFound) return;
        _secretHasBeenFound = true;
        NotificationUI.instance.DisplayTxt(_txtToDisplay, _txtDisplayTime);
        StateManager.instance.secret_hasBeenFound[_currentSceneIndex][_IDNum] = true;
        _collectables.UpdateSecretsFoundInScene(StateManager.instance.GetNumOfSecretsFoundInLevel(_currentSceneIndex));
        _collectables.DisplaySecretFoundOnPickup();
        FMODUnity.RuntimeManager.PlayOneShot(_path);
    }

    public void SetIDNum(int num) { _IDNum = num; }
    public int GetIDNum() { return _IDNum; }
    public bool IsInDefaultState() { return _secretHasBeenFound; }
    public void UpdateState(bool state) { _secretHasBeenFound = !state; }
}
