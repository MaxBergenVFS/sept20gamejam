﻿using UnityEngine;

public class SecretFoundTriggerVolume : Secret
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            SecretFound();
        }
    }
}
