﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class Soul : MonoBehaviour, ISaveable
{
    [SerializeField]
    private SoulHealth _health;
    [SerializeField]
    private SoulItem _item;
    [SerializeField]
    private InteractableTextDisplay _interactTextWhileAlive;
    [SerializeField]
    private InteractableTextDisplay _interactTextWhileDead;
    [SerializeField]
    private PauseAnimation[] _animsToPause;

    private SoulDialogue _dialogue;
    private int _IDNum;
    private bool _soulHasBeenFreed = false;
    private int _currentSceneIndex;

    public void SetIDNum(int num) { _IDNum = num; }
    public int GetIDNum() { return _IDNum; }

    private void Awake()
    {
        _dialogue = GetComponent<SoulDialogue>();
        _currentSceneIndex = _currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
    }

    public void StartDialogue()
    {
        _dialogue.TriggerDialogueOnDeath();
        _health.gameObject.SetActive(false);
        foreach (var anim in _animsToPause) { anim.PauseAnim(); }
        _soulHasBeenFreed = true;
        StateManager.instance.soul_hasBeenFreed[_currentSceneIndex][_IDNum] = true;
    }

    public bool IsInDefaultState()
    {
        return _soulHasBeenFreed;
    }

    public void UpdateState(bool state)
    {
        _interactTextWhileAlive.gameObject.SetActive(state);
        _interactTextWhileDead.gameObject.SetActive(!state);
        _health.gameObject.SetActive(state);
        _dialogue.enabled = state;
        _item.gameObject.SetActive(false);
        _soulHasBeenFreed = state;
    }
}
