﻿using UnityEngine;

public class SoulHealth : MonoBehaviour, IDamageable
{
    [SerializeField]
    private float _healthAmt = 1f;
    [SerializeField]
    private int _defLvl = 1;
    [SerializeField]
    private InteractableTextDisplay _interactTextWhileAlive;
    [SerializeField]
    private Soul _soulManager;

    public void TakeDamage(int amt, bool player)
    {
        if (!player) return;
        _healthAmt -= amt;
        if (_healthAmt <= 0)
        {
            _interactTextWhileAlive.ClearText();
            _interactTextWhileAlive.gameObject.SetActive(false);
            _soulManager.StartDialogue();
        }
    }

    public bool CompareDefenseLevel(int atkLvl)
    {
        if (atkLvl >= _defLvl) return true;
        return false;
    }

    public bool CheckIfDead() { return (_healthAmt <= 0); }

    public bool CheckIfFlesh() { return true; }
}
