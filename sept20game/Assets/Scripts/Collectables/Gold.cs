﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Gold : BaseItem, IInteractable, ISaveable
{
    [SerializeField]
    private int _amtOfCoins;

    private Collectables _collectables;
    private PlayerController _playerController;
    private PlayerInventory _playerInventory;
    private InventoryItemDatabase _inventoryDatabase;
    private InputManager _inputManager;
    private int _IDNum;
    private int _currentSceneIndex;

    protected override void Awake()
    {
        base.Awake();
        _currentSceneIndex = _currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
    }

    private void Start()
    {
        _playerController = PlayerController.instance;
        _collectables = _playerController.GetComponent<Collectables>();
        _inputManager = _playerController.GetComponent<InputManager>();
        _playerInventory = _playerController.GetComponent<PlayerInventory>();
        _inventoryDatabase = _playerInventory.inventoryDatabase;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player")) Interact();
    }

    public void Interact()
    {
        if (_inputManager.hasInventory)
        {
            StartLerp();
            StateManager.instance.gold_hasBeenPickedUp[_currentSceneIndex][_IDNum] = true;
            List<int> goldFoundInScene = StateManager.instance.GetNumOfGoldFoundInLevel(_currentSceneIndex);
            _collectables.UpdateGoldFoundInScene(goldFoundInScene);
            _collectables.DisplayGoldAmtOnPickup(_amtOfCoins);
            _inventoryDatabase.AddToInventoryItemAmt(_playerInventory.GetGoldDatabaseID(), _amtOfCoins);
            FMODUnity.RuntimeManager.PlayOneShot("event:/SFX/Interactables/Gold");
        }
        else { DisplayInspectText(); }
    }

    protected override void Destroy()
    {
        this.gameObject.SetActive(false);
    }

    public int GetAmtOfCoins() { return _amtOfCoins; }
    public void SetIDNum(int num) { _IDNum = num; }
    public int GetIDNum() { return _IDNum; }
    public bool IsInDefaultState() { return !this.gameObject.activeSelf; }
    public void UpdateState(bool state) { this.gameObject.SetActive(state); }
}
