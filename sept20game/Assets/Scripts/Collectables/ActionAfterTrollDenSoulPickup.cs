﻿using System.Collections;
using UnityEngine;

public class ActionAfterTrollDenSoulPickup : ActionToActivate
{
    [SerializeField]
    private float _secondsBeforeCameraChange = 2f;
    [SerializeField]
    private float _secondsBeforeRegainingControl = 5f;
    [SerializeField]
    private GameObject _camToActivate;
    [SerializeField]
    private GameObject[] _objsToDeactivateOnAction;
    [SerializeField]
    private Animator _doorAnim;

    private InputManager _inputManager;

    private void Start()
    {
        _inputManager = PlayerController.instance.GetComponent<InputManager>();
    }

    public override void Activate()
    {
        base.Activate();
        StartCoroutine(ActivateAfterSeconds());
    }

    IEnumerator ActivateAfterSeconds()
    {
        _inputManager.enabled = false;
        yield return new WaitForSeconds(_secondsBeforeCameraChange);
        ObjsToActivate(false);
        _doorAnim.SetTrigger("Unlock");
        yield return new WaitForSeconds(_secondsBeforeRegainingControl);
        _inputManager.enabled = true;
        ObjsToActivate(true);
    }

    private void ObjsToActivate(bool cond)
    {
        _camToActivate.SetActive(!cond);
        foreach (GameObject obj in _objsToDeactivateOnAction)
        {
            obj.SetActive(cond);
        }
    }
}
