﻿using UnityEngine;

public class ActionToActivate : MonoBehaviour
{
    public virtual void Activate() { }
}