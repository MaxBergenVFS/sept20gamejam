﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SoulItem : BaseItem, IInteractable
{
    private PlayerController _playerController;
    private Collectables _collectables;
    private PlayerInventory _playerInventory;
    private PlayerInteract _playerInteract;
    private InventoryItemDatabase _inventoryDatabase;
    private ActionToActivate _actionToActivate;
    private int _currentSceneIndex;
    private bool _soulHasBeenFreed = false;

    [SerializeField]
    private GameObject _soulRenderer;
    [SerializeField]
    private InteractableTextDisplay _interactTextWhileDead;

    protected override void Awake()
    {
        base.Awake();
        _currentSceneIndex = _currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        if (GetComponent<ActionToActivate>()) _actionToActivate = GetComponent<ActionToActivate>();
    }

    private void Start()
    {
        _playerController = PlayerController.instance;
        _collectables = _playerController.GetComponent<Collectables>();
        _playerInventory = _playerController.GetComponent<PlayerInventory>();
        _playerInteract = _playerController.GetComponent<PlayerInteract>();
        _inventoryDatabase = _playerInventory.inventoryDatabase;
    }

    public void Interact()
    {
        FreeSoul();
        if (_actionToActivate) _actionToActivate.Activate();
        _playerInteract.ResetInteract();
    }

    protected override void Destroy() { _soulRenderer.SetActive(false); }

    private void FreeSoul()
    {
        StartLerp();
        _soulHasBeenFreed = true;
        _collectables.UpdateSoulsFreedInScene(StateManager.instance.GetNumOfSoulsFreedInLevel(_currentSceneIndex));
        _collectables.DisplaySoulFreedOnPickup();
        _interactTextWhileDead.gameObject.SetActive(true);
        _inventoryDatabase.AddToInventoryItemAmt(_playerInventory.GetSoulDatabaseID(), 1);
        _playerInventory.DisplaySoulUI();
    }

}
