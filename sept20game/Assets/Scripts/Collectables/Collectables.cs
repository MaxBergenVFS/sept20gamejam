﻿using System.Collections.Generic;
using UnityEngine;

public class Collectables : MonoBehaviour
{
    [SerializeField]
    private CollectableFoundUIDisplay _goldFoundUI;
    [SerializeField]
    private UITextLerp _displayGoldFoundAmt;
    [SerializeField]
    private UIImageLerp _displayGoldFoundImage;
    [SerializeField]
    private CollectableFoundUIDisplay _secretsFoundUI;
    [SerializeField]
    private UIImageLerp _displaySecretsFoundImage;
    [SerializeField]
    private CollectableFoundUIDisplay _soulsFreedUI;
    [SerializeField]
    private UIImageLerp _displaySoulsFreedImage;

    private StateHandler _stateHandler;


    private void Awake()
    {
        _stateHandler = GameObject.FindObjectsOfType<StateHandler>()[0];
    }

    public void InitSecretsInScene(int num) { InitCollectablesInScene(_secretsFoundUI, num); }
    public void UpdateSecretsFoundInScene(int num) { UpdateCollectablesFoundInScene(_secretsFoundUI, num); }
    public void DisplaySecretFoundOnPickup() { _displaySecretsFoundImage.DisplayImage(); }

    public void InitSoulsInScene(int num) { InitCollectablesInScene(_soulsFreedUI, num); }
    public void UpdateSoulsFreedInScene(int num) { UpdateCollectablesFoundInScene(_soulsFreedUI, num); }
    public void DisplaySoulFreedOnPickup() { _displaySoulsFreedImage.DisplayImage(); }

    public void InitGoldInScene(int num) { InitCollectablesInScene(_goldFoundUI, num); }
    public void UpdateGoldFoundInScene(List<int> indexes)
    {
        int amtOfGold = 0;
        foreach (int index in indexes)
        {
            amtOfGold += _stateHandler.GetGoldAmtByIndex(index);
        }
        _goldFoundUI.SetCollectablesFoundInLevel(amtOfGold);
    }
    public void DisplayGoldAmtOnPickup(int num)
    {
        _displayGoldFoundAmt.DisplayTxt($"+{num}");
        _displayGoldFoundImage.DisplayImage();
    }

    public void ActivateCollectableOnPickupUI(bool cond)
    {
        if (_displayGoldFoundAmt) _displayGoldFoundAmt.gameObject.SetActive(cond);
    }

    private void InitCollectablesInScene(CollectableFoundUIDisplay collectable, int num)
    {
        collectable.SetTotalCollectablesInLevel(num);
    }

    private void UpdateCollectablesFoundInScene(CollectableFoundUIDisplay collectable, int num)
    {
        collectable.SetCollectablesFoundInLevel(num);
    }
}
