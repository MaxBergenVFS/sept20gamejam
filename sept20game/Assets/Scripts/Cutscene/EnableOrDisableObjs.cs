﻿using UnityEngine;

public class EnableOrDisableObjs : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _objsToEnable;
    [SerializeField]
    private GameObject[] _objsToDisable;
    [SerializeField]
    private GameObject[] _objsToReverseActiveState;
    [SerializeField]
    private bool _enableInputManager = false;

    private InputManager _inputManager;

    private void Start()
    {
        _inputManager = PlayerController.instance.GetComponent<InputManager>();
    }

    public void SwapObjs()
    {
        if (_enableInputManager) _inputManager.enabled = true;
        foreach (var obj in _objsToEnable)
        {
            obj.SetActive(true);
        }
        foreach (var obj in _objsToDisable)
        {
            obj.SetActive(false);
        }
        foreach (var obj in _objsToReverseActiveState)
        {
            obj.SetActive(!obj.activeSelf);
        }
    }

}
