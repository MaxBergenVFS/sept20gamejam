﻿using UnityEngine;

public class CutsceneCam : MonoBehaviour
{
    private PlayerCutscene _playerCutscene;

    private void OnEnable()
    {
        _playerCutscene = PlayerController.instance.GetComponent<PlayerCutscene>();
        _playerCutscene.EnterCutsceneState();
    }
    private void OnDisable() { _playerCutscene.ExitCutsceneState(); }

}