﻿using UnityEngine;

public class PlayAnimOnEnable : MonoBehaviour
{
    [SerializeField]
    private string _triggerName;

    private Animator _anim;

    private void Awake()
    {
        _anim = GetComponent<Animator>();
    }

    private void OnEnable()
    {
        _anim.SetTrigger(_triggerName);
    }
}
