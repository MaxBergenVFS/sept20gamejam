﻿public interface IDamageable
{
    void TakeDamage(int amt, bool player);
    bool CompareDefenseLevel(int atkLvl);
    bool CheckIfDead();
    bool CheckIfFlesh();
}
