﻿public interface ICastable
{
    void CastAbilityBtnDown();
    void CastAbilityBtnHold();
    void CastAbilityBtnUp();
}
