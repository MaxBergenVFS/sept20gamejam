﻿using UnityEngine;

public interface IDistractable
{
    void Distract(Vector3 distractionDest);
    void DisableDistract();
}