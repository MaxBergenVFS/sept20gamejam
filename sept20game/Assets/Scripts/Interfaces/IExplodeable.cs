﻿public interface IExplodeable
{
    void SetWillExplode();
}
