﻿public interface IDeflectable
{
    void Deflect();
}
