﻿public interface ISaveable
{
    int GetIDNum();
    bool IsInDefaultState();
    void UpdateState(bool state);
}
