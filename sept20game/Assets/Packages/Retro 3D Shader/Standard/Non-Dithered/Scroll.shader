﻿Shader "Particles/WraithTest"
{
	Properties
	{
		[HDR] _Color ("Color", Color) = (1,1,1,1)
		[NoScaleOffset] _MainTex("Texture", 2D) = "white" {}
 
	}
		SubShader
	{
		Tags{ "Queue" = "Transparent" "RenderType" = "Transparent" }
		Lighting Off
		Fog{ Mode Off }
		ZWrite Off
		Cull Off
 
		Pass
	{
 
		Blend One OneMinusSrcAlpha // Premultiplied transparency
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
 
 
#include "UnityCG.cginc"
 
		struct appdata
	{
		float4 vertex : POSITION;
		float4 uv : TEXCOORD0;
		float4 color : COLOR;
	};
 
	struct v2f
	{
		float4 vertex : SV_POSITION;
		float4 uv : TEXCOORD0;
		float4 uv2 : TEXCOORD1;
		float4 color : COLOR;
	};
 
	sampler2D _MainTex;
	float _BlendThreshold;
	float4 _Color;
 
	v2f vert(appdata v)
	{
		v2f o;
		o.vertex = UnityObjectToClipPos(v.vertex);
		o.uv = v.uv;
 
		float2 uv2 = (v.uv.xy + float2(0,v.uv.w)) + frac((_Time.y + v.uv.z * 100) * float2(0, 0.15));
		o.uv2 = float4(uv2,0,0);
		o.color = v.color;
		return o;
	}
 
	fixed4 frag(v2f i) : SV_Target
	{
	fixed4 col1 = tex2D(_MainTex, i.uv) * i.color * _Color;
	fixed4 col2 = tex2D(_MainTex, i.uv2);
 
	fixed4 col;
	col.a = col1.a * col2.a * 2 * i.color.a;
	col.rgb = col1.rgb * col2.rgb * 2 * col2.a * i.color.a;
	return col;
	}
		ENDCG
	}
	}
}
