// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Oasis_Water"
{
	Properties
	{
		_TextureSample2("Texture Sample 2", 2D) = "white" {}
		_WaterScale("Water Scale", Range( 0 , 15)) = 15
		_AlphaScale("Alpha Scale", Range( 0 , 5)) = 5
		_Emission("Emission", Range( 0 , 5)) = 5
		_DistortionScale("Distortion Scale", Range( 0 , 15)) = 5
		_DistortionIntensity("Distortion Intensity", Range( 0 , 1)) = 5
		_NoiseGenScale("Noise Gen Scale", Range( 0 , 1)) = 5
		_DistortionScroll("Distortion Scroll", Vector) = (0,0,0,0)
		_TextureSample1("Texture Sample 1", 2D) = "white" {}
		_WaterTint("Water Tint", Color) = (0.4357423,0.6037736,0.479816,1)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "AlphaTest+0" "IsEmissive" = "true"  }
		Cull Back
		Blend SrcAlpha OneMinusSrcAlpha
		
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha noshadow 
		struct Input
		{
			float4 screenPos;
			float2 uv_texcoord;
		};

		uniform float4 _WaterTint;
		uniform sampler2D _TextureSample2;
		uniform float2 _DistortionScroll;
		uniform float _DistortionScale;
		uniform float _NoiseGenScale;
		uniform float _DistortionIntensity;
		uniform float _WaterScale;
		uniform float _Emission;
		uniform sampler2D _TextureSample1;
		uniform float _AlphaScale;


		float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }

		float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }

		float snoise( float2 v )
		{
			const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
			float2 i = floor( v + dot( v, C.yy ) );
			float2 x0 = v - i + dot( i, C.xx );
			float2 i1;
			i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
			float4 x12 = x0.xyxy + C.xxzz;
			x12.xy -= i1;
			i = mod2D289( i );
			float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
			float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
			m = m * m;
			m = m * m;
			float3 x = 2.0 * frac( p * C.www ) - 1.0;
			float3 h = abs( x ) - 0.5;
			float3 ox = floor( x + 0.5 );
			float3 a0 = x - ox;
			m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
			float3 g;
			g.x = a0.x * x0.x + h.x * x0.y;
			g.yz = a0.yz * x12.xz + h.yz * x12.yw;
			return 130.0 * dot( m, g );
		}


		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float4 ase_screenPos = float4( i.screenPos.xyz , i.screenPos.w + 0.00000000001 );
			float4 ase_screenPosNorm = ase_screenPos / ase_screenPos.w;
			ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
			float2 temp_cast_0 = (_DistortionScale).xx;
			float2 uv_TexCoord59 = i.uv_texcoord * temp_cast_0;
			float2 panner57 = ( 1.0 * _Time.y * _DistortionScroll + uv_TexCoord59);
			float simplePerlin2D101 = snoise( panner57*_NoiseGenScale );
			simplePerlin2D101 = simplePerlin2D101*0.5 + 0.5;
			float2 temp_cast_1 = (_WaterScale).xx;
			float2 uv_TexCoord9 = i.uv_texcoord * temp_cast_1;
			float4 tex2DNode94 = tex2D( _TextureSample2, ( ( ase_screenPosNorm.w + ( simplePerlin2D101 * _DistortionIntensity ) ) + uv_TexCoord9 ) );
			float4 lerpResult48 = lerp( _WaterTint , tex2DNode94 , float4( 0,0,0,0 ));
			o.Albedo = lerpResult48.rgb;
			o.Emission = ( tex2DNode94 * _Emission ).rgb;
			o.Alpha = ( ( tex2DNode94 * tex2D( _TextureSample1, i.uv_texcoord ) ) * _AlphaScale ).r;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=18900
-1920;23;1920;1036;1070.563;689.6869;1.473979;True;False
Node;AmplifyShaderEditor.RangedFloatNode;72;-1613.575,-435.9302;Inherit;False;Property;_DistortionScale;Distortion Scale;5;0;Create;True;0;0;0;False;0;False;5;15;0;15;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;58;-1206.231,-282.312;Inherit;False;Property;_DistortionScroll;Distortion Scroll;8;0;Create;True;0;0;0;False;0;False;0,0;0,0.2;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;59;-1272.846,-452.0202;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;102;-1038.177,-120.6689;Inherit;False;Property;_NoiseGenScale;Noise Gen Scale;7;0;Create;True;0;0;0;False;0;False;5;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;57;-959.1612,-280.4212;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;53;-753.0923,15.53063;Inherit;False;Property;_DistortionIntensity;Distortion Intensity;6;0;Create;True;0;0;0;False;0;False;5;0.05;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;101;-707.3755,-236.4288;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;54;-421.1634,-106.4796;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;52;-719.9128,-497.5712;Float;True;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;1;-608.5281,209.3908;Inherit;False;Property;_WaterScale;Water Scale;2;0;Create;True;0;0;0;False;0;False;15;2;0;15;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;67;-264.8204,-366.4718;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;9;-241.0411,96.49213;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;112;29.81012,-27.0719;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;15;-53.72607,322.0708;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;94;233.0711,-107.181;Inherit;True;Property;_TextureSample2;Texture Sample 2;1;0;Create;True;0;0;0;False;0;False;-1;None;84c97261bdfd742b29d807100a332a0e;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;14;240.2491,206.8613;Inherit;True;Property;_TextureSample1;Texture Sample 1;9;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;49;202.5645,-464.8484;Inherit;False;Property;_WaterTint;Water Tint;10;0;Create;True;0;0;0;False;0;False;0.4357423,0.6037736,0.479816,1;0.6988252,0.9811321,0.7603183,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;117;620.5391,-218.5368;Inherit;False;Property;_Emission;Emission;4;0;Create;True;0;0;0;False;0;False;5;0.37;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;35;652.1292,62.95803;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;115;554.2249,430.8058;Inherit;False;Property;_AlphaScale;Alpha Scale;3;0;Create;True;0;0;0;False;0;False;5;0.6;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;116;868.7568,105.7573;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;118;841.8936,-72.87105;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;48;662.011,-377.325;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;119;1127.417,-41.4692;Float;False;True;-1;2;ASEMaterialInspector;0;0;Standard;Oasis_Water;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;False;-1;0;False;-1;False;0;False;-1;0;False;-1;False;0;Custom;0.5;True;False;0;True;Transparent;;AlphaTest;All;14;all;True;True;True;True;0;False;-1;False;0;False;-1;255;False;-1;255;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;-1;False;2;15;10;25;False;0.5;False;2;5;False;-1;10;False;-1;0;0;False;-1;0;False;-1;0;False;-1;0;False;-1;0;False;0;0,0,0,0;VertexOffset;True;False;Cylindrical;False;Relative;0;;0;-1;-1;-1;0;False;0;0;False;-1;-1;0;False;-1;0;0;0;False;0.1;False;-1;0;False;-1;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0;False;9;FLOAT;0;False;10;FLOAT;0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;59;0;72;0
WireConnection;57;0;59;0
WireConnection;57;2;58;0
WireConnection;101;0;57;0
WireConnection;101;1;102;0
WireConnection;54;0;101;0
WireConnection;54;1;53;0
WireConnection;67;0;52;4
WireConnection;67;1;54;0
WireConnection;9;0;1;0
WireConnection;112;0;67;0
WireConnection;112;1;9;0
WireConnection;94;1;112;0
WireConnection;14;1;15;0
WireConnection;35;0;94;0
WireConnection;35;1;14;0
WireConnection;116;0;35;0
WireConnection;116;1;115;0
WireConnection;118;0;94;0
WireConnection;118;1;117;0
WireConnection;48;0;49;0
WireConnection;48;1;94;0
WireConnection;119;0;48;0
WireConnection;119;2;118;0
WireConnection;119;9;116;0
ASEEND*/
//CHKSM=70007BBAC06B1C75B3EED9A48D75B1541ABF43FC